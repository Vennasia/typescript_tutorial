class A {
  private readonly x: any;
  constructor(arg1: any) {
    this.x = arg1;
  }

  info() {
    console.log(this.x);
  }
}

class B extends A{

}


const b = new B('xxxx')
b.info(); //xxxx
console.log(b.hasOwnProperty('x')); // true
