ts中模块分为两种

一种叫内部模块 ---> 命名空间

一种叫外部模块 ---> import/export (es6模块语法)

---

>命名空间
>
> 为了划分作用域 防止变量冲突
>
比如js里↓
```js
let obj = {
    a(){}
}

let obj1 = {
    a(){}
}
```
ts中为了解决同一个模块中可能命名冲突
```ts
namespace School1 {
    export interface PERSON {
        name: string
        age: number
    }

    export class Teacher{}

    export const a = 1

    //↓如果只是在内部使用就无需导出
    export namespace Room {
        export const class1 = 'class1'
    }

    Room.class1
}
School1.Teacher
School1.a
School1.Room.class1//TS2741: Property 'age' is missing in type '{ name: string; }' but required in type 'PERSON'.

let p1:School1.PERSON = {name:'ahhh'}

namespace School2 {
    export interface PERSON {
        name: string
        age?: number
    }
    export class Teacher{}
}
School2.Teacher

let p2:School2.PERSON = {name:'ahhh'}
```
一个namespace其实就是一个闭包

命名空间也可以嵌套

## 命名空间最大的功能 --- 合并类型
命名空间最大的功能 并不是写代码的时候用, 而是用它来合并类型 扩展

> 1) 命名空间可以来进行合并
>

