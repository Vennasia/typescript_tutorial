/** ambient namespace
 流行的程序库D3在全局对象d3里定义它的功能。
 因为这个库通过一个 <script>标签加载（不是通过模块加载器），它的声明文件使用内部模块来定义它的类型。
 为了让TypeScript编译器识别它的类型，我们使用外部命名空间声明。 比如，我们可以像下面这样写：
*/
/*→外部命名空间(declare)*/declare namespace D3 {
  export interface Selectors {
    select: {
      (selector: string): Selection;
      (element: EventTarget): Selection;
    };
  }

  export interface Event {
    x: number;
    y: number;
  }

  export interface Base extends Selectors {
    event: Event;
  }
}

declare var d3: D3.Base;

// 这样和没有declare相比 好处在于 我们可以省略掉命名空间内部的export关键字 即可直接 `D3.x` 访问内部成员
