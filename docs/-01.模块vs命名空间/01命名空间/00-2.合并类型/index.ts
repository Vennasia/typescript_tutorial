export = {};

//命名空间最大的功能 并不是写代码的时候用, 而是用它来合并类型 扩展
//注意：也可以扩展功能（实现）, 但一般都是扩展类型, 让类型提示更好

/** 1) 命名空间可以和类来进行合并*/
class Person {}
//类无法声明多个同名的 但命名空间可以
namespace Person {
  export let type = '人';
}
console.log(Person.type); //人


/** 2) 合并函数*/
function $(){}
//但需要注意的是命名空间必须放在函数声明后面
namespace $ {
  export namespace fn {
    //可以放实现
    export let a = 1;

    //也可以只是个类型（一般都是放类型,给出更多的提示）
    export let b:string;
  }
}
console.log($.fn.a); //1
console.log($.fn.b); //undefined


/** 3) 命名空间可以和枚举合并*/
enum AUTH {
  user = 1
}
namespace AUTH {
  export let admin: string
}
// AUTH.



/** 4) 命名空间同名也可以合并*/
namespace AUTH {
  export let manager: string
}
// AUTH.


/** 5) 接口和命名空间不能合并*/


