export = {}
namespace School1 {
  export interface PERSON {
    name: string
    age: number
  }

  export class Teacher{}

  export const a = 1

  //↓如果只是在内部使用就无需导出
  export namespace Room {
    export const class1 = 'class1'
  }

  Room.class1
}
School1.Teacher
School1.a
School1.Room.class1

let p1:School1.PERSON = {name:'ahhh'} //TS2741: Property 'age' is missing in type '{ name: string; }' but required in type 'PERSON'.

namespace School2 {
  export interface PERSON {
    name: string
    age?: number
  }
  export class Teacher{}
}
School2.Teacher

let p2:School2.PERSON = {name:'ahhh'}
