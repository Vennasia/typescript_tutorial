/// <reference path="Validation.ts" />
/// <reference path="LettersOnlyValidator.ts" />
/// <reference path="ZipCodeValidator.ts" />

// Some samples to try
let strings = ["Hello", "98052", "101"];

// Validators to use
let validators: { [s: string]: Validation.StringValidator; } = {};
validators["ZIP code"] = new Validation.ZipCodeValidator();
validators["Letters only"] = new Validation.LettersOnlyValidator();

// Show whether each string passed each validator
for (let s of strings) {
  for (let name in validators) {
    console.log(`"${ s }" - ${ validators[name].isAcceptable(s) ? "matches" : "does not match" } ${ name }`);
  }
}


/**
 命名空间分割成多个文件。 尽管是不同的文件，它们仍是同一个命名空间，并且在使用的时候就如同它们在一个文件中定义的一样。

 因为不同文件之间存在依赖关系，所以我们加入了引用标签来告诉编译器文件之间的关联。

 注意我们之所以使用`/// <reference path="Validation.ts" />`,是因为我们此时不是外部模块 而是内部模块的分离(命名空间)
 内部模块是作用于全局的

 参看 Validation.ts LettersOnlyValidator.ts ZipCodeValidator.ts
*/

/**
当涉及到多文件时，我们必须确保所有编译后的代码都被加载了。 我们有两种方式。

第一种方式，把所有的输入文件编译为一个输出文件，需要使用--outFile标记：
 tsc --outFile sample.js index.ts
 参看case0

 编译器会根据源码里的引用标签自动地对输出进行排序。你也可以单独地指定每个文件。
 tsc --outFile sample.js Validation.ts LettersOnlyValidator.ts ZipCodeValidator.ts index.ts
*/
/**
 第二种方式，我们可以编译每一个文件（默认方式），那么每个源文件都会对应生成一个JavaScript文件。 然后，在页面上通过 <script>标签把所有生成的JavaScript文件按正确的顺序引进来，比如：

 <script src="Validation.js" type="text/javascript" />
 <script src="LettersOnlyValidator.js" type="text/javascript" />
 <script src="ZipCodeValidator.js" type="text/javascript" />
 <script src="Test.js" type="text/javascript" />
*/

