export {};
let person = {
  name: 'ahhh',
  age: 123,
  address: {
    where: {},
    code: {}
  }
}
//5. 类型反推 typeof (取一个值(变量)的类型
type MySchool = typeof/*←注意这个是ts里的typeof 而不是js里的typeof*/ person
/*
type MySchool = {
  name: string;
  age: number;
  address: {
    where: {};
    code: {};
  };
}
*/

/** 需要注意的是js里也有typeof, 但不会冲突, 因为我们这个typeof是放在类型里使用的*/
