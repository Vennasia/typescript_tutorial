import ts from 'rollup-plugin-typescript2'; //解析ts
import {nodeResolve} from '@rollup/plugin-node-resolve'; //告诉rollup如何查找外部模块
import serve from 'rollup-plugin-serve'; // 启动本地服务
import path from 'path'

export default {
  input: 'src/index.ts',
  output: {
    format: 'iife',
    file: path.resolve(__dirname, 'dist/bundle.js'),
    sourcemap: true //tsconfig.json里也需要开启sourceMap
  },
  plugins: [
    nodeResolve({ //第三方文件解析
      extensions: ['.js', '.ts']
    }),
    ts({
      tsconfig: path.resolve(__dirname, 'tsconfig.json')
    }),
    serve({
      // open: true,
      openPage: '/public/index.html',
      port: 3000,
      contentBase: ''
    })
  ]
}

//打包.d.ts的时候再单独说tsc用法
