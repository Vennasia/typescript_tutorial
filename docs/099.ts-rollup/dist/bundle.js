(function () {
  'use strict';

  var X = /** @class */ (function () {
      function X() {
          this._n = '';
          //↑定义在原型上的,不同实例间共享这个属性
          //↓定义在实例自身身上的,每个实例独有的
          this.name = 'ahhh';
      }
      Object.defineProperty(X.prototype, "n", {
          get: function () {
              return this._n;
          },
          set: function (val) {
              this._n = val;
          },
          enumerable: false,
          configurable: true
      });
      //↓ 类里声明的方法 不带static的话 是原型上的 （带static 就是类自身身上的
      X.prototype.say = function () { };
      return X;
  }());
  console.log(new X().n === new X().n); //true
  var Role;
  (function (Role) {
      // Reporter = 1 //可以进行自定义赋值，后面的值会以此为基础递增
      Role[Role["Reporter"] = 0] = "Reporter";
      Role[Role["Developer"] = 1] = "Developer";
      Role[Role["Maintainer"] = 2] = "Maintainer";
      Role[Role["Owner"] = 3] = "Owner";
      Role[Role["Guest"] = 4] = "Guest";
  })(Role || (Role = {}));
  console.log(Role);

})();
//# sourceMappingURL=bundle.js.map
