class X {
  private _n = ''
  get n() {
    return this._n
  }
  set n(val){
    this._n = val
  }

  //↑定义在原型上的,不同实例间共享这个属性
  //↓定义在实例自身身上的,每个实例独有的
  name: string = 'ahhh'

  //↓ 类里声明的方法 不带static的话 是原型上的 （带static 就是类自身身上的
  say() {}
}

console.log(new X().n === new X().n) //true

enum Role{
  // Reporter = 1 //可以进行自定义赋值，后面的值会以此为基础递增
  Reporter
  ,Developer
  ,Maintainer
  ,Owner
  ,Guest
}
console.log(Role);
