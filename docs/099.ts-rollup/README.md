```shell script
npm install rollup typescript rollup-plugin-typescript2 @rollup/plugin-node-resolve rollup-plugin-serve -D
```

---

要生成声明文件,需要设置`"declaration": true`

---

声明文件的查找:
1. 默认先查找node_modules ---> 对应包的package.json ---> types字段
2. 如果没有types字段, 则会找对应包下有没有index.d.ts
3. 如果没有说明此包没有用ts来写
4. 找 @types/对应的包 下的 types字段

---带修正---

5. 如果上面还是没找到, 会找当前工程下所有.d.ts文件
6. 如果这样都没找到,可以手动指定查找路径,tsconfig.json里的:
    + `paths`字段
    + `include`字段
