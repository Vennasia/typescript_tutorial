export = {}

const getVal = <T extends Object, K extends keyof T>(obj: T, key: K) => {};
// 或者也可以↓
// const getVal = <T extends Object>(obj:T,key:keyof T)=>{}
getVal({a:1,b:2},'a1'/*← 必须是a或则b*/)


/** 约束属性 keyof 表示取对象中的所有key属性名合并到一起来作为一个联合类型*/
//取的是key
type IKeys = keyof {a:1, b: 'afwf';}; // ---> "a" | "b"

/** case 0*/
type t3 = keyof string; //type t3 = .... 所有string上的属性名
let o:t3;
o = 'codePointAt'

/** case 1 特殊的 keyof any*/
// 特殊的 keyof any
type t1 = keyof any; //type t1 = string | number | symbol

/** case 2
 * keyof (xx|yy) 的结果是 xx和yy 两个类型的公有属性的key组成的类型*/
// case 2.1
type t2 = keyof (string | number); //type t2 = "toString" | "valueOf"
//keyof (string | number)结果是
//string和number公有的属性的key组成的联合类型
let x = /*123*/'123'
console.log(x.valueOf()); //123
console.log(x.toString()); //123

// case 2.2
let o1 = {
  a:1,
  b:2,
  c:3
}
let o2 = {
  b:'b',
  c:'c',
  d:'d'
}
type x = keyof (typeof o1 | typeof o2) //type x = "b" | "c"



