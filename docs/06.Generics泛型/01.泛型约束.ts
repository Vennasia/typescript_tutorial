export = {}

// 因为 T 可以是任何类型,sth并不一定是具有length的对象,So下面会报错
// function returnIt<T>(sth: T): T {
//   sth.length; // TS2339: Property 'length' does not exist on type 'T'.
//   return sth;
// }

interface hashLength {
  length: number;
}

//So 我们需要对泛型进行约束
// 只要是能放在extends后面的东西 都可以约束T
function returnIt<T extends/*表示[满足]后面的约束*/ hashLength>(sth: T): T {
  sth.length; // TS2339: Property 'length' does not exist on type 'T'. //但一旦extends hashLength后 就能确保传进来的都是带有.length属性的了
  return sth;
}

/** 我们很少直接用约束来 extends string 这种, 这种情况推荐使用函数重载*/

returnIt({a: 1, length: 123});
