export = {}

class MyArray<T>{
  public arr: T[] = [];

  add(v: T) {
    this.arr.push(v)
  }

  getMaxNum():T{
    let arr = this.arr;
    let max = arr[0];
    for (let i = 0; i < arr.length; i++) {
      let current = arr[i];
      current > max ? max = current : void 0;
    }
    return max
  }
}

let arr = new MyArray<number>();
arr.add(1)
arr.add(3)
arr.add(2)
console.log(arr.getMaxNum()); //3
