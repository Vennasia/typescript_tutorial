export {};

/** 泛型与函数*/
//returnSth也可以这样定义
//既可以在function时定义 (函数声明)
//也可以写个let像下面这样定义↓ (函数表达式)
let returnSth2: <T>(sth: T) => T = (sth) => {
  return sth;
};
//再或则以调用签名对象字面量(同接口)来定义泛型函数
let returnStr3: { <T>(sth: T): T; } = (sth) => {
  return sth
};

function returnArray<T>(array:T[]):Array<T>{ // T[] 和 Array<T> 是等价的
  return array;
}

