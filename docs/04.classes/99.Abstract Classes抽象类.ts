export = {}
/** 抽象类
  也称之为 爸爸类: 专门当别的类的爸爸的类(一定有儿子,但也有可能有爸爸),它要求它的子类必须实现自己抽象的方法
  也称之为 没有写完的类: 只描述有什么方法, 并没有完全实现这些方法
*/
abstract class Animal {
  // 这个方法实现了↓
  movie(): void {
    console.log('walk walk...')
  }

  /** 抽象类必须有抽象方法 或则说 抽象方法必须声明在抽象类中*/
  // 这个方法没有具体实现
  // 抽象方法的语法与接口方法相似。 两者都是定义方法签名但不包含方法体。 然而，抽象方法必须包含 abstract关键字并且可以包含访问修饰符(public、protected、private)
  abstract makeSound(): void;

  /*
    emmm 如果你记得interface
    这不是打脸吗？
    interface就是可以直接不写实现 只写方法签名
    你现在用了类之后呢 你可以写实现
    然后你又搞一个抽象类说 我类也可以不写实现
    有没有感觉打脸 (⊙o⊙)??
    what the fuck??
  */
}

// new Animal() // TS2511: Cannot create an instance of an abstract class.

class Human extends Animal{
  makeSound(): void {

  }
  xxx():void{
    console.log('xxx');
  }
}

const h = new Human()

h.movie(); // 'walk walk...'
h.xxx(); // xxx
