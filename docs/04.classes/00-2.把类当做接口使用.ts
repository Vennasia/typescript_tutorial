export = {}
/** 如上一节里所讲的，类定义会创建两个东西：类的实例类型(类型) 和 一个构造函数(值)。
    + 因为类可以创建出类型，所以你能够在允许使用接口的地方使用类。*/


class Point {
  x!: number;
  y!: number;
}

interface Point3d extends Point {
  z: number;
}

let point3d: Point3d = {x: 1, y: 2, z: 3};

/** 注：
 * 1. 一个接口可以extends多个接口或则类(或abstract class)
 * 2. 一个类可以implements多个接口
 * 2. 一个类只能extends一个类(或abstract class)
 */
