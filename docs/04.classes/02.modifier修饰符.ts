export = {}
/** public、protected、private、readonly
 * js里的静态属性/方法比较奇葩 可以被继承*/
class Animal {
  protected kind: string; /** 只能在当前类及其子类中使用*/
  /*protected*//** ← 默认是public,如果不是的话,我们new Animal的时候就会报错*/ constructor(kind:string) {
    this.kind = kind;
  }
  move(): void{}

  static bbbb = 222
}

new Animal('bird')

class Human extends Animal {
  readonly name: string; /** 你可以使用 readonly关键字将属性设置为只读的。 只读属性必须在声明时或构造函数里被初始化。*/
  public age: string; /** public为属性修饰符的默认值*/
  private secret: string; /** 出了Human这个class范围就不可见*/

  constructor(name:string,age:string) {
    super('哺乳动物'); // 这个super 等同于 Animal的constructor
    this.name = name;
    this.age = age;
    this.secret = '这是我的秘密';
  }

  say(){
    console.log(`my kind is ${this.kind}`);
    console.log(`my secret is ${this.secret}`);
  }

  private static xxx1 = 'xxx1'; //Human自己的属性 而不是其实例的属性

  static yyy(){+
    console.log(this.xxx1)
  }
}

const p = new Human('ahhh', '123');
console.log(Human.bbbb);

// console.log(p.secret); // TS2341: Property 'secret' is private and only accessible within class 'Human'.

p.say();

// console.log(p.kind); // TS2445: Property 'kind' is protected and only accessible within class 'Animal' and its subclasses.

// p.name = 'ahhh2' //TS2540: Cannot assign to 'name' because it is a read-only property.

/** 静态属性与修饰符*/
// Human.xxx1; // TS2341: Property 'xxx1' is private and only accessible within class 'Human'.
Human.yyy(); // xxx1

