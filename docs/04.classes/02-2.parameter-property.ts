/** 参数属性 Parameter properties*/
class Octopus {
  readonly name: string;
  readonly numberOfLegs: number = 8;
  constructor (theName: string) {
    this.name = theName;
  }
}
let dad = new Octopus("Man with the 8 strong legs");
/*↑
在上面的例子中，我们必须在Octopus类里定义一个只读成员 name和一个参数为 theName的构造函数，并且立刻将 theName的值赋给 name，这种情况经常会遇到。
参数属性可以方便地让我们在一个地方定义并初始化一个成员。 下面的例子是对之前 Octopus类的修改版，使用了参数属性：*/

class Octopus2 {
  // readonly name: string; // 可以省掉了 因为我们在参数前面使用了修饰符
  readonly numberOfLegs: number = 8;
  constructor(readonly name: string) {
    // this.name = name; // 可以省掉了 因为我们在参数前面使用了修饰符
  }
}
let dad2 = new Octopus("Man with the 8 strong legs");
console.log(dad2.name); // Man with the 8 strong legs

export {}
