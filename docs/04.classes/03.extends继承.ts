export = {}
//js里的静态属性/方法比较奇葩 可以被继承 (因为编译里有这么一句 Child.__proto__ = Father)

class Animal {
  kind: string;
  constructor(kind:string) {
    this.kind = kind;
  }
  move(): void{}

  static bbbb = 222

  static getName(){
    return '动物类'
  }

  say(){
    console.log('animal say')
  }
}

class Human extends Animal {
  name: string;
  age: string;
  constructor(name:string,age:string) {
    /** 在构造函数里访问 this的属性之前，我们 一定要调用 super()。 这个是TypeScript强制执行的一条重要规则*/
    super('哺乳动物'); // 这个super 等同于 Animal的constructor
    this.name = name;
    this.age = age;
  }

  static getName(){
    console.log('super.getName():',super.getName()); //这里的super指代的是父类本身
    return '人类'
  }

  // 如果重载了父类的方法, 需要注意的是,
  // 父类的返回值需要为void (默认) , 表示父类不关心子类的返回值,
  // 你这里才能改变自己的返回值类型
  say(){
    super.say(); //这里的super指代的是父类的原形
    console.log('human say');
  }
}

console.log(Human.bbbb); //222

console.log(Human.getName());
