class Animal {}

class Cat extends Animal {

}

//1. super在构造函数中指定的是父类
//2. 在原型方法中指代的是父类的原形
//3. 在静态方法中指定的是父类本身
//查看 04.classes/03.extends继承.ts

export {};
