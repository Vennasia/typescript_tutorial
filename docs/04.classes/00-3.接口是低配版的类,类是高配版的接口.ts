export = {};
/** 类就是用来告诉你一个对象必须有什么属性的东西(同接口)*/
// 类就是用来创造对象的东西
// 有一些语言创建对象必须先声明一个类(比如java),而有的语言(比如js)则不需要

/** 接口是低配版的类,类是高配版的接口*/
// 有些类的功能接口实现不了
// 理论上我们可以全用类而不用接口,但可能会多写很多代码,很烦

/*
对于没有使用过TS的JS程序员来说, 类看起来还挺无聊
>我需要什么属性随时加不就好了吗？

对于使用过TS的JS程序员来说, 类可以让你的系统更加可预测
>这个对象不会出现一些我不知道的属性,一切都尽在我的掌握
*/

/** 用interface去创建一个对象*/
interface Human0 {
  name: string;
  age: number;

  movie():void
}

// console.log(Human0); // TS2693: 'Human0' only refers to a type, but is being used as a value here.

function movie(): void {
  console.log('我在动');
}

const ahhh0: Human0 = {
  name: 'ahhh',
  age: 123,
  movie
};

const ahhh1: Human0 = {
  name: 'ahhh',
  age: 123,
  movie
};

/** 用类取创建一个对象*/
// class相较于interface,它可以直接把"实现"写在里面
class Human {
  // 声明你constructor里会用到的属性
  name: string; //必须在constructor调用阶段初始值, 否则ts会报错(严格模式下) //如果你是在不想赋初始值,可以在这里断言 name！: string
  age: number;

  /** interface无法写具体实现↓*/
  move(): void {
    console.log('我在动');
  }

  /** interface木有↓ 它使得类变得很灵活*/
  constructor(name:string = 'ahhh', age:number = 123) {
    this/*this表示我们访问的是该类的成员*/.name = name;
    this.age = age;
  }

  /** 静态属性*/
  static xxx = 1; //Human自己的属性 而不是其实例的属性

}

// const ahhh = new Human('ahhh',123);
const ahhh = new Human();
console.log(ahhh);

/** ↓↓这是js在整个编程语言界里比较特殊的一点,会将constructor也放进去*/
console.log(ahhh.constructor/*等同于Human.prototype.constructor*/); // [Function: Human]
console.log(Human); // 和interface不同它能够被打印,其实它就是构造函数 constructor
console.log(Human === ahhh.constructor); // true

// 静态属性
console.log(Human.xxx); // 1

