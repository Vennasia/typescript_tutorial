export = {}

//实例方法和属性就是放在 this. 上的
//原型方法和属性是放在 X.prototype 上的

class X {
  //this._na
  private _n = ''

  //Object.defineProperty(X.prototype,'n',{get,set})
  get n() {
    return this._n
  }
  set n(val){
    this._n = val
  }
  //↑定义在原型上的,不同实例间共享这个属性(n),但这里是getter,So获取的其实是n的getter里返回的值
  //
  //↓定义在实例自身身上的,每个实例独有的
  //this.name
  name: string = 'ahhh'

  //↓ 类里声明的方法 不带static的话 是原型上的 （带static 就是类自身身上的
  say() {}
}

console.log(new X().n === new X().n); //true

// (function () {
//   'use strict';
//
//   var X = /** @class */ (function () {
//     function X() {
//       this._n = '';
//       //↑定义在原型上的,不同实例间共享这个属性
//       //↓定义在实例自身身上的,每个实例独有的
//       this.name = 'ahhh';
//     }
//     Object.defineProperty(X.prototype, "n", {
//       get: function () {
//         return this._n;
//       },
//       set: function (val) {
//         this._n = val;
//       },
//       enumerable: false,
//       configurable: true
//     });
//     //↓ 类里声明的方法 不带static的话 是原型上的 （带static 就是类自身身上的
//     X.prototype.say = function () { };
//     return X;
//   }());
//   console.log(new X().n === new X().n); //true
//
// }());


/** 但ts没法区分得到的属性是原型上还是实例上的*/
class Y {
  public a;

  constructor() {
    this.a = 1;
  }

  //同名会报错, 但你在下面实际使用的时候, 点的时候, 并不能区分它是从实例上来的 还是 原型上来的
  // get a(){}
}

new Y().a
