/*class Circle {
  constructor(x: number, y: number, r: number) {
    this.x = x; //TS2339: Property 'x' does not exist on type 'Circle'.
    this.y = y;
    this.r = r;
  }
} */


class Circle {
  x/*: number*/ //类型可省略 下面声明了
  public y //修饰符默认就为public, 可省略

  // 但声明一个属性 就必须要给它指定一个类型并赋予初始值, 或者指定为any
  // l //TS7008: Member 'l' implicitly has an 'any' type.

  // S2564: Property '_r' has no initializer and is not definitely assigned in the constructor.
  // 声明的一个非any类型的成员, 必须要在constructor中或者这里声明时赋予初始值
  // _r:string

  // 这里声明的属性 其实就是帮助我们在this上拿到提示
  // 只要声明了 就能在this上点出来
  // 查看 /** case 1*/
  public _r: string = '';

  constructor(x: number, y: number, public r: number) {
    /** ts要求 要给this赋值的话 必须要提前声明*/
    // this.zzz = 'nnn'; //TS2339: Property 'zzz' does not exist on type 'Circle'.

    this.x = x;
    this.y = y;

    /** 若给构造函数接受的参数任意一个修饰符（这被称之为 【参数属性 Parameter properties】） 即可省略↓*/
    // this.r = r;
  }
}

/** case 1*/
new Circle(1,1,2)._r

export = {}
