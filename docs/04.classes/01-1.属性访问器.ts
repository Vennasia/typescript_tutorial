export = {}
class X {
  get eat(){
    return '吃'
  }
  //↑是定义在原型上的
}

console.log(new X().eat)

// (function () {
//   'use strict';
//
//   var X = /** @class */ (function () {
//     function X() {
//     }
//     Object.defineProperty(X.prototype, "eat", {
//       get: function () {
//         return '吃';
//       },
//       enumerable: false,
//       configurable: true
//     });
//     return X;
//   }());
//   console.log(new X().eat);
//
// }());
