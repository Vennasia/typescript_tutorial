export = {}
/*
When you declare a class in TypeScript, you are actually creating multiple declarations at the same time.

+ The first is the [type] of the instance of the class.

+ The second is, we creating a [value] that we call the constructor function.
*/
class Greeter {
  greeting: string;
  constructor(message: string) {
    this.greeting = message;
  }
  greet() {
    return "Hello, " + this.greeting;
  }
}

let greeter: Greeter; /** 意思是 Greeter类的【实例】的类型是 Greeter*/
greeter = new Greeter("world");
console.log(greeter.greet());

/** ↑编译为javascript后↓*/
// let Greeter = (function () {
//     function Greeter(message) {
//         this.greeting = message;
//     }
//     Greeter.prototype.greet = function () {
//         return "Hello, " + this.greeting;
//     };
//     return Greeter;
// })();
//
// let greeter;
// greeter = new Greeter("world");
// console.log(greeter.greet());
/*↑
上面的代码里， let Greeter将被赋值为构造函数。
当我们调用 new并执行了这个函数后，便会得到一个类的实例。
这个构造函数也包含了类的所有静态属性。
换个角度说，我们可以认为类具有 实例部分与 静态部分这两个部分。*/



/** 我们使用 typeof Greeter，意思是取Greeter类的类型，而不是实例的类型。 或者更确切的说，"告诉我 Greeter标识符的类型"，也就是构造函数的类型*/
//Here we use typeof Greeter, that is “give me the type of the Greeter class itself” rather than the instance type.
//Or, more precisely, “give me the type of the symbol called Greeter,” which is the type of the constructor function.
//precisely -> 精确地;恰好地;严谨地，严格地;一丝不苟地;

//
let greeterMaker: typeof Greeter = Greeter;
// greeterMaker.prototype // 能取到.prototype 说明是构造函数类型

//
class Animal {

}
function createInstance(
  // clazz:{new ():Animal}
  //↕ 等价
  clazz:new ()=>Animal
){

}
createInstance(Animal)

let A = Animal //鼠标划上去, 你可以看到类型为 ---> let A: typeof Animal
//↕ 等价 即 typeof Animal 等于 new ()=>Animal
let B: new () => Animal = Animal //let A:new ()=>Animal = Animal
//除了用↑这种 `()=>` type的形式, 你也可以用接口去描述
// let C: { new(): Animal; };
// ↕ 等价
interface IClazz {
  new(): Animal;
}
let C: IClazz;


//查看 05.interface(描述任意对象形状)/05.接口描述类类型(构造函数).ts
