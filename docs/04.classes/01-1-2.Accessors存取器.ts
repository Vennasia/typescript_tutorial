/** getter/setter模式 */
class Human{
  constructor(public name:string = 'ahhh',private _age:number = 123) {

  }

  get age(){
    return this._age
  }

  set age(value:number){
    value < 0 ? this._age = 0 :
      this._age = value;
  }

  /*
   Object.definedProperty(Human.prototype,'age',{
     get:function(){}
     ,set:function(){}
   })
 */
}

const ahhh = new Human();
ahhh.age = -1;
console.log(ahhh.age); // 0

/** 注意:
 + 首先，存取器要求你将编译器设置为输出ECMAScript 5或更高。 不支持降级到ECMAScript 3。

 + 其次，只带有 get不带有 set的存取器自动被推断为 readonly。 这在从代码生成 .d.ts文件时是有帮助的，因为利用这个属性的用户会看到不允许够改变它的值。
 */
