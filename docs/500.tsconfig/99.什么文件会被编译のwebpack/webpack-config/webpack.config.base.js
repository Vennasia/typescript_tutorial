const path = require('path');

module.exports = {
  // entry: './src/index.js'
  entry: './index.ts'

  , output: {
    filename: 'boundle-[hash:7].js'

    , path: path.resolve(__dirname, '../dist')
  }

  , resolve: {
    //省略加载文件的后缀名会按照以下后缀进行补全后依次查找
    extensions: ['.tsx','.ts','.jsx', '.js', '.json', '.css']

    //自定义路径解析
    , alias: require('./alias')
  }

  , module: {
    rules: [
      /*{
        test: /\.jsx?$/

        , use: [{
          loader: 'babel-loader'
          , options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
            , plugins: [
              ["@babel/plugin-proposal-decorators",{"legacy":true}]
              , ['@babel/plugin-proposal-class-properties',{"loose":true}]
            ]
          }
        }]

        , include: path.resolve(__dirname,'../src')

        , exclude: /node_modules/
      }*/

      //使用ts作为编译器
      {
        test: /\.[jt]sx?$/

        , use: [{
          // 也可以使用babel-loader,然后使用Babel和ts合作出的@babel/preset-typescript
          // 但不支持类型检查 需要在脚本中自己开启类型检查 "type-check": "tsc --watch"(在开发时另起一个终端执行该npm脚本)
          // 记得将tsconfig.json的noEmit置为true
          // 但babel无法编译命名空间、类型断言(let s = {} as A)、常量枚举(const enum E {A})、默认导出(export = s)
          loader: 'ts-loader'
          ,options:{
            // 默认为false
            // transpileOnly: true // 只做语言转化而不做类型检查(意思是说即使写的代码不符合ts规范,也能编译成功) 类型检查交给 ForkTsCheckerWebpackPlugin 插件来做 这样做是为了提高构建速度(build的速度)
          }
        },'eslint-loader']

        , include: path.resolve(__dirname,'../src')

        , exclude: /node_modules/
      }

      //图片
      ,{
        test: /\.(gif|jpg|jpeg|png|bmp|eot|woff|woff2|ttf|svg)$/
        , use: [
          {
            loader: 'url-loader'
            , options: {
              limit: 4096
              , outputPath: 'images' //将图片拷贝到哪里 基于配置的打包的output
              , publicPath: '/images' //重写webpack output里的publicPath配置
            }
          }
        ]
      }

      //支持html中的img引用图片
      , {
        test: /\.html/
        , use: 'html-withimg-loader'
      }
    ]
  }

  , plugins: [
    //copy
    // new CopyWebpackPlugin([
    //   {
    //     from: path.resolve(__dirname, 'src/assets')
    //     , to: path.resolve(__dirname, 'dist/')
    //   }
    // ])
  ]

};
