const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const DllReferencePlugin = require('webpack/lib/DllReferencePlugin');
const path = require('path');

module.exports = {

  devServer: require('./devServer')

  , devtool: 'eval'

  ,optimization: {
    minimize:false
  }

  , module: {
    rules: [
      {
        test: /\.css$/
        , use: [{
          loader:'style-loader'
          , options: {
            //在<header></header>底部插入还是顶部插入,默认是底部
            // insertAt: 'top'
          }
        }, 'css-loader']

      }

      //sass
      ,{
        test:/\.s[ac]ss$/
        , use: [{
          loader:'style-loader'
          , options: {
            //在<header></header>底部插入还是顶部插入,默认是底部
            // insertAt: 'top'
          }
        }, 'css-loader', 'sass-loader']
      }
    ]
  }

  , plugins: [
    new HtmlWebpackPlugin({
      template: './public/index_dev.html'
      , filename: 'index.html'
      //为了避免缓存,插入script引入产出的文件时,会在url中带上hash作为query
      // (浏览器是以url为缓存依据的,如果url不一样缓存就失效了)
      , hash: true
    })

    ,new DllReferencePlugin({
      manifest:path.resolve(__dirname,'../dist/react_family.manifest.json')
    })

    // Hot Module Replacement 的插件
    ,new webpack.HotModuleReplacementPlugin()
  ]
};
