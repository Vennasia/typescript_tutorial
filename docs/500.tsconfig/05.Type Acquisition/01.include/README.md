If you have a JavaScript project where TypeScript needs additional guidance to understand global dependencies, or have disabled the built-in inference via disableFilenameBasedTypeAcquisition.

You can use `include` to specify which types should be used from DefinitelyTyped:
