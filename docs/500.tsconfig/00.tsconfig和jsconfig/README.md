>https://www.typescriptlang.org/docs/handbook/tsconfig-json.html#overview

JavaScript projects can use a jsconfig.json file instead, which acts almost the same but has some JavaScript-related compiler flags enabled by default.

