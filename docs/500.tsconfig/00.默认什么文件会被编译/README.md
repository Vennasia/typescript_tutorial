当`tsconfig.json`不存在时,运行`tsc`,~~会报错,因为此时必须指定一个文件(比如`tsc a.ts`)~~

不会报错,但也不会进行编译,而是会打印`tsc`的用法

```shell
tsc

Version 4.2.4
Syntax:   tsc [options] [file...]
Examples: tsc hello.ts
          tsc --outFile file.js file.ts
          tsc @args.txt
          tsc --build tsconfig.json
Options:
 -h, --help                                         Print this message.
 ...
```

---
当`tsconfig.json`存在时,运行`tsc`,比如这里的case0,因为`tsconfig.json`里什么都没有,使用默认配置,故整个`case0`都会作为ts工程目录,所有`.ts`文件都会被编译(子文件夹里的也会),我们可以发现生成了`a.js`、`b.js`、`c.js`三个文件

---
如果`"files"`和`"include"`都没有被指定(include和files没有默认值,exclude有),
+ 编译器默认包含当前目录和子目录下所有的TypeScript文件(`.ts`, `.d.ts` 和 `.tsx`)
    + 如果`allowJs`被设置为`true`,还会包含`.js`、`.jsx`文件
+ 排除在`"exclude"`里指定的文件

`"exclude"`默认情况下会排除`node_modules`,`bower_components`,`jspm_packages`和`<outDir>`目录
