>https://www.typescriptlang.org/tsconfig#extends

默认值为 false

tsconfig.json文件可以利用extends属性从另一个配置文件里继承配置。

`extends`是`tsconfig.json`文件里的顶级属性(与`compilerOptions`,`files`,`include`,和`exclude`一样)。

`extends`的值是一个字符串,包含指向另一个要继承文件的路径。
+ The path may use Node.js style resolution.
+ 配置文件里的相对路径在解析时相对于它所在的文件(查看case1, 是 `configs/a.ts` 被编译了 而不是根目录下的 `a.ts`)。
+ 如果你的配置文件里自己指定了 `files`、`include`、`exclude`, 那么继承的那个配置文件里对应的选项指定的就全部无效了(没错 不会再合并进来, 查看case2, `configs/a.ts`不会被编译)
+ 当前版本下(tsc 4.2.4) `references` 顶级属性选项 继承时会被忽略
