[toc]
> 了解更多:
> + https://www.typescriptlang.org/docs/handbook/tsconfig-json.html
>
> + https://www.typescriptlang.org/tsconfig#

```shell
tsc --init
```

## 所有配置
```
Top Level          files,extends,include andexclude

"compilerOptions"
Project Options    allowJs,checkJs,composite,declaration,declarationMap,downlevelIteration,importHelpers,incremental,isolatedModules,jsx,lib,module,noEmit,outDir,outFile,plugins,removeComments,rootDir,sourceMap,target andtsBuildInfoFile
Strict Checks      alwaysStrict,noImplicitAny,noImplicitThis,strict,strictBindCallApply,strictFunctionTypes,strictNullChecks andstrictPropertyInitialization
Module Resolution  allowSyntheticDefaultImports,allowUmdGlobalAccess,baseUrl,esModuleInterop,moduleResolution,paths,preserveSymlinks,rootDirs,typeRoots andtypes
Source Maps        inlineSourceMap,inlineSources,mapRoot andsourceRoot
Linter Checks      noFallthroughCasesInSwitch,noImplicitOverride,noImplicitReturns,noPropertyAccessFromIndexSignature,noUncheckedIndexedAccess,noUnusedLocals andnoUnusedParameters
Experimental       emitDecoratorMetadata andexperimentalDecorators
Advanced           allowUnreachableCode,allowUnusedLabels,assumeChangesOnlyAffectDirectDependencies,charset,declarationDir,diagnostics,disableReferencedProjectLoad,disableSizeLimit,disableSolutionSearching,disableSourceOfProjectReferenceRedirect,emitBOM,emitDeclarationOnly,explainFiles,extendedDiagnostics,forceConsistentCasingInFileNames,generateCpuProfile,importsNotUsedAsValues,jsxFactory,jsxFragmentFactory,jsxImportSource,keyofStringsOnly,listEmittedFiles,listFiles,maxNodeModuleJsDepth,newLine,noEmitHelpers,noEmitOnError,noErrorTruncation,noImplicitUseStrict,noLib,noResolve,noStrictGenericChecks,out,preserveConstEnums,reactNamespace,resolveJsonModule,skipDefaultLibCheck,skipLibCheck,stripInternal,suppressExcessPropertyErrors,suppressImplicitAnyIndexErrors,traceResolution anduseDefineForClassFields
Command Line       preserveWatchOutput andpretty

"watchOptions"
watchOptions       watchFile,watchDirectory,fallbackPolling,synchronousWatchDirectory,excludeDirectories andexcludeFiles


"typeAcquisition"
typeAcquisition    enable,include,exclude anddisableFilenameBasedTypeAcquisition
```

## `TypeScript`项目的根目录
如果一个目录下存在一个`tsconfig.json`文件,那么它意味着这个目录是`TypeScript`项目的根目录。 `tsconfig.json`文件中指定了用来编译这个项目的根文件和编译选项。 一个项目可以通过以下方式之一来编译：

+ 不带任何输入文件的情况下调用`tsc`,编译器会从当前目录开始去查找`tsconfig.json`文件,逐级向上搜索父目录。

+ 不带任何输入文件的情况下调用`tsc`,且使用命令行参数`--project`(或`-p`)指定一个包含`tsconfig.json`文件的**目录**。(或则直接指定到一个有效的`.json`文件)

当命令行上指定了输入文件时,`tsconfig.json`文件会被忽略。

## TSConfig Bases
>https://www.typescriptlang.org/docs/handbook/tsconfig-json.html#tsconfig-bases
>
[github.com/tsconfig/bases](https://github.com/tsconfig/bases/) 这里有一些写好的tsconfig的预设配置
```json
{
  "extends": "@tsconfig/node12/tsconfig.json",
  "compilerOptions": {
    "preserveConstEnums": true
  },
  "include": ["src/**/*"],
  "exclude": ["node_modules", "**/*.spec.ts"]
}
```
+ 这样你只需要根据不同的开发环境选择继承已有的预设配置 然后 再进行一些自定义的选项覆盖即可


## compilerOptions,及默认配置
`"compilerOptions"`可以被忽略,这时编译器会使用默认值。在这里查看完整的[编译器选项列](https://www.tslang.cn/docs/handbook/compiler-options.html)表。


## 关于自动引入@types
默认所有可见的`@type`包会在编译过程中被包含进来。 `node_modules/@types`文件夹下以及它们子文件夹下的所有包都是可见的； 也就是说, `./node_modules/@types/`,`../node_modules/@types/`和`../../node_modules/@types/`等等。

果指定了`typeRoots`,只有`typeRoots`下面的包才会被自动包含进来。 比如：
```shell script
{
   "compilerOptions": {
       "typeRoots" : ["./typings"]
   }
}
```
这个配置文件会包含所有`./typings`下面的包,而不包含`./node_modules/@types`里面的包。

如果指定了`types`,只有被列出来的包才会被包含进来。 比如：
```shell script
{
   "compilerOptions": {
        "types" : ["node", "lodash", "express"]
   }
}
```
根据这个`tsconfig.json`文件,将仅会包含 `./node_modules/@types/node`,`./node_modules/@types/lodash`和`./node_modules/@types/express`。 `node_modules/@types/*`里面的其它包不会被引入进来。

So指定`types`是用来禁用自动`@types`包的

>注意: 自动引入只在你使用了全局的声明（相对于模块）时是重要的。 如果你使用 import "foo"语句,TypeScript仍然会查找node_modules和node_modules/@types文件夹来获取foo包

## compileOnSave

在最顶层设置compileOnSave标记,可以让`IDE`在保存文件的时候根据`tsconfig.json`重新生成文件。

```shell script
{
    "compileOnSave": true,
    "compilerOptions": {
        "noImplicitAny" : true
    }
}
```
要想支持这个特性需要Visual Studio 2015, TypeScript1.8.4以上并且安装atom-typescript插件。

