+ 默认值为: `[]` if `files` is specified, otherwise `["**/*"]`
+ 支持的通配符(`wildcard characters`)如下:
 - `*` matches zero or more characters (excluding directory separators)
 - `?` matches any one character (excluding directory separators)
 - `**/` matches any directory nested to any level (匹配嵌套到任何级别的任何目录)

If a glob pattern doesn’t include a file extension, then only files with supported extensions are included (e.g. `.ts`, `.tsx`, and `.d.ts` by default, with `.js` and `.jsx` if `allowJs` is set to true).
