该选项主要用来过滤 `include` 选项包含的的文件, 它不是一种阻止文件被包含在代码库中的机制——它只是改变了`include`设置找到的内容。

+ 默认值： `["node_modules", "bower_components", "jspm_packages"]`, plus the value of `outDir` if one is specified.
+ `exclude` only changes which files are included as a result of the `include` setting. A file specified by exclude can still become part of your codebase due to an `import` statement in your code, a `types` inclusion, a `/// <reference` directive, or being specified in the `files` list.

---

```shell
tsc
```
+ 运行本例, 你会发现虽然我们 `include` 里包含了 `src/1`, 但由于我们 `exclude` 对其进行了排除, 故对改文件下的ts文件不会进行编译
