如果`"files"`和`"include"`都没有被指定(include和files没有默认值,exclude有),
+ 编译器默认包含当前目录和子目录下所有的TypeScript文件(`.ts`, `.d.ts` 和 `.tsx`)
  + 如果`allowJs`被设置为`true`,还会包含`.js`、`.jsx`文件
+ 排除在`"exclude"`里指定的文件

如果指定了 `"files"`或`"include"`,编译器会将它们结合一并包含进来。

使用 `"outDir"`指定的目录下的文件永远会被编译器排除,除非你明确地使用`"files"`将其包含进来(这时就算用`exclude`指定也没用,`exclude`只是用来对`include`包含的进行过滤的)。

使用`"include"`引入的文件可以使用`"exclude"`属性过滤。 然而,通过 `"files"`属性明确指定的文件却总是会被包含在内,不管`exclude`如何设置。

如果没有特殊指定, `"exclude"`默认情况下会排除`node_modules`,`bower_components`,`jspm_packages`和`<outDir>`目录。

任何被`"files"`或`"include"`指定的文件所引用的文件也会被包含进来。 `A.ts`引用了`B.ts`,因此`B.ts`不能被排除,除非引用它的`A.ts`在`"exclude"`列表中。

需要注意编译器不会去引入那些可能做为输出的文件；比如,假设我们包含了`index.ts`,那么`index.d.ts`和`index.js`会被排除在外。 通常来讲,不推荐只有扩展名的不同来区分同目录下的文件。

`tsconfig.json`文件可以是个空文件,那么所有默认的文件(如上面所述)都会以默认配置选项编译。

在命令行上指定的编译选项会覆盖在`tsconfig.json`文件里的相应选项。
