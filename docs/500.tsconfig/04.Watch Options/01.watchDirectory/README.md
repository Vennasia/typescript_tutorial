The strategy for how entire directory trees are watched under systems that lack recursive file-watching functionality.

在缺乏递归文件监视功能的系统下如何监视整个目录树的策略。

+ `fixedPollingInterval`: Check every directory for changes several times a second at a fixed interval.

  在固定的时间间隔内，每秒多次检查每个目录的更改。
+ `dynamicPriorityPolling`: Use a dynamic queue where less-frequently modified directories will be checked less often.

  使用动态队列，在其中较少修改的目录将被较少检查。
+ `useFsEvents` (the default): Attempt to use the operating system/file system’s native events for directory changes.

  尝试使用操作系统/文件系统的本地事件来更改目录。
