>You can use `excludeFiles` to drastically reduce the number of files which are watched during `--watch`. This can be a useful way to reduce the number of open file which TypeScript tracks on Linux.
>
>你可以使用excludeFiles来大幅减少——watch期间被监视的文件数量。这是一种减少TypeScript在Linux上跟踪的打开文件数量的有效方法。

```json
{
  "watchOptions": {
    "excludeDirectories": ["**/node_modules", "_build", "temp/*"]
  }
}
```
