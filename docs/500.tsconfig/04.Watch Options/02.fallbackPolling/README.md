>When using file system events, this option specifies the polling strategy that gets used when the system runs out of native file watchers and/or doesn’t support native file watchers.
>
> 当使用文件系统事件时，此选项指定当系统用完本机文件监视器和/或不支持本机文件监视器时使用的轮询策略。
>

+ `fixedPollingInterval`: Check every file for changes several times a second at a fixed interval.

  在固定的时间间隔内，每秒多次检查每个文件的更改。
+ `priorityPollingInterval`: Check every file for changes several times a second, but use heuristics to check certain types of files less frequently than others.

  每秒检查每一个文件的更改几次，但是使用启发式方法检查某些类型的文件的频率要低于其他类型的文件。
+ `dynamicPriorityPolling`: Use a dynamic queue where less-frequently modified files will be checked less often.

  使用动态队列，在其中较少修改的文件将被较少检查。
+ `synchronousWatchDirectory`: Disable deferred watching on directories. Deferred watching is useful when lots of file changes might occur at once (e.g. a change in node_modules from running npm install), but you might want to disable it with this flag for some less-common setups.

  禁用目录上的延迟监视。当大量文件变化同时发生时(例如，node_modules在运行npm install时发生变化)，延迟监视是很有用的，但对于一些不太常见的设置，你可能想用这个标志禁用它。
