You can use excludeFiles to remove a set of specific files from the files which are watched.

```json
{
  "watchOptions": {
    "excludeFiles": ["temp/file.ts"]
  }
}
```
