The strategy for how individual files are watched.

+ `fixedPollingInterval`: Check every file for changes several times a second at a fixed interval.

  在固定的时间间隔内，每秒多次检查每个文件的更改

+ `priorityPollingInterval`: Check every file for changes several times a second, but use heuristics to check certain types of files less frequently than others.

  每秒检查每一个文件的更改几次，但是使用启发式方法检查某些类型的文件的频率要低于其他类型的文件。

+ `dynamicPriorityPolling`: Use a dynamic queue where less-frequently modified files will be checked less often.

  使用动态队列，在其中较少修改的文件将被较少检查。

+ `useFsEvents` (the default): Attempt to use the operating system/file system’s native events for file changes.

  尝试使用操作系统/文件系统的本地事件进行文件更改。
+ `useFsEventsOnParentDirectory`: Attempt to use the operating system/file system’s native events to listen for changes on a file’s parent directory.This can use fewer file watchers, but might be less accurate.

  尝试使用操作系统/文件系统的本机事件来监听文件父目录上的更改
