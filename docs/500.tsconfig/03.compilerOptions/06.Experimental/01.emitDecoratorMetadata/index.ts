import 'reflect-metadata';

@Reflect.metadata('role', 'admin')
class Post {}

const metadata = Reflect.getMetadata('role', Post);

console.log(metadata);  // admin

console.log(Reflect.getMetadata("design:type",Post))
console.log(Reflect.getMetadata("design:paramtypes",Post))
console.log(Reflect.getMetadata("design:returntype",Post))
