默认值为false, 即不会在控制台保留之前的信息

Whether to keep outdated console output in watch mode instead of clearing the screen every time a change happened.
