默认import时,引入的那个文件的字母的大小写可以不是一样的

比如你的文件叫 `Abc.ts`, 你 `import` 时可以通过 `abc` 来引入, 并且实际运行时也不会报错

你可以通过设置 `forceConsistentCasingInFileNames` 为true, 这样在编译时就会报错
