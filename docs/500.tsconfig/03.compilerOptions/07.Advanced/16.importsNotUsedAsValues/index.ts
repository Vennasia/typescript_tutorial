import {x} from './cat';
//error:
//index.ts:1:1 - error TS1371: This import is never used as a value and must use 'import type' because 'importsNotUsedAsValues' is set to 'error'.
//remove:
//生成的js文件里, 不再会生成 require('./cat') 这一句
//preserve:
//生成的js文件里, 会保留 require('./cat') 这一句

let a: x = 123;

console.log(a);
