>The maximum dependency depth to search under node_modules and load JavaScript files.
>
>This flag is can only be used when allowJs is enabled, and is used if you want to have TypeScript infer types for all of the JavaScript inside your node_modules.
>
>Ideally this should stay at 0 (the default), and d.ts files should be used to explicitly define the shape of modules. However, there are cases where you may want to turn this on at the expense of speed and potential accuracy.
>
>

> 在node_modules下搜索并加载JavaScript文件的最大依赖深度。
>
>这个标志只能在allowJs启用时使用，如果你想让你的node_modules内的所有JavaScript都有TypeScript的推断类型，就使用这个标志。
>
>理想情况下，这个值应该保持在0(默认值)，并且应该使用d.ts文件显式地定义模块的形状。然而，在某些情况下，您可能希望以牺牲速度和潜在的准确性为代价启用此功能。
