默认值为: false

> Do not emit compiler output files like JavaScript source code, source-maps or declarations if any errors were reported.
>
> 如果报告了任何错误，不要发出编译器输出文件，如JavaScript源代码、源代码映射或声明。

>This defaults to false, making it easier to work with TypeScript in a watch-like environment where you may want to see results of changes to your code in another environment before making sure all errors are resolved.
>
> 此默认为false，使您可以更轻松地使用Watch的环境中的类型，您可能希望在确保解决所有错误之前，您可能希望在另一个环境中查看对代码的更改的结果。
