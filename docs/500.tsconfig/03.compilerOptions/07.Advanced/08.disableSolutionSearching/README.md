当使用 composite TypeScript projects 时，该选项提供了一种方法，用于在使用诸如查找所有引用或在编辑器中跳转到定义等特性时，声明不希望包含项目。

您可以使用此标志来提高大型组合项目的响应性。

>When working with composite TypeScript projects, this option provides a way to declare that you do not want a project to be included when using features like find all references or jump to definition in an editor.
>
>This flag is something you can use to increase responsiveness in large composite projects.
