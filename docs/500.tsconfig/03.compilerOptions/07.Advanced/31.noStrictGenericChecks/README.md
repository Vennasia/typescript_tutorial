默认值: false

>TypeScript will unify type parameters when comparing two generic functions.
>
> 当比较两个泛型函数时，TypeScript会统一类型参数。
