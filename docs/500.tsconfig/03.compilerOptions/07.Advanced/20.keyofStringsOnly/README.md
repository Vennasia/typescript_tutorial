默认值为 false

该选项处于 废弃(Deprecated) 状态

This flag changes the keyof type operator to return string instead of string | number when applied to a type with a string index signature.

This flag is used to help people keep this behavior from before TypeScript 2.9’s release.
