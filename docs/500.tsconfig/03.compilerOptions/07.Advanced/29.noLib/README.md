默认值为 false, 和 `lib` 选项相反

>Disables the automatic inclusion of any library files. If this option is set, lib is ignored.
>
>TypeScript cannot compile anything without a set of interfaces for key primitives like: Array, Boolean,Function, IArguments, Number, Object, RegExp, and String. It is expected that if you use noLib you will be including your own type definitions for these.
>
> TypeScript不能编译任何东西，如果没有一组关键基元的接口，比如:数组、布尔值、函数、IArguments、数字、对象、RegExp和字符串。如果您使用noLib，那么您应该包含您自己的类型定义。

