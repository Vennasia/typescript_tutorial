默认值为false

You can use this flag to discover where TypeScript is spending its time when compiling. This is a tool used for understanding the performance characteristics of your codebase overall.

你可以用这个标志来发现TypeScript在编译时花了多少时间。这是一个用来全面理解代码库性能特征的工具。

You can learn more about how to measure and understand the output in the [performance section of the wiki](https://github.com/microsoft/TypeScript/wiki/Performance).

您可以在wiki的性能部分了解关于如何度量和理解输出的更多信息。
