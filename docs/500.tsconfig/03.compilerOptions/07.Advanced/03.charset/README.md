In prior versions of TypeScript, this controlled what encoding was used when reading text files from disk. Today, TypeScript assumes UTF-8 encoding, but will correctly detect UTF-16 (BE and LE) or UTF-8 BOMs.

默认值为 utf8, 该选项已处于废弃状态, 现在能自动识别是 utf-8 还是  UTF-16 (BE and LE) or UTF-8 BOMs.
