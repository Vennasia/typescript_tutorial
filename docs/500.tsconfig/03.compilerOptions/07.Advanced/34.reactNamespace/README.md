默认值: react

Use `--jsxFactory` instead. Specify the object invoked for `createElement` when targeting `react` for TSX files.

