默认值为 false

将编译过程中生成的文件的名称打印到终端。

This flag is useful in two cases:

+ You want to transpile TypeScript as a part of a build chain in the terminal where the filenames are processed in the next command.
+ You are not sure that TypeScript has included a file you expected, as a part of debugging the file inclusion settings.

```
example
├── index.ts
├── package.json
└── tsconfig.json
```

```json
{
  "compilerOptions": {
    "declaration": true,
    "listFiles": true
  }
}
```

```
$ npm run tsc

path/to/example/index.js
path/to/example/index.d.ts
```
