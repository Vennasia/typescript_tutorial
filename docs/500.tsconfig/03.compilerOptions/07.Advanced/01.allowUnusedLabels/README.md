+ `undefined`: (default) provide suggestions as warnings to editors
+ `true`: unused labels are ignored
+ `false`: raises compiler errors about unused labels
