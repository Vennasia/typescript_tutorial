默认值为: false

You shouldn’t need this. By default, when emitting a module file to a non-ES6 target, TypeScript emits a `"use strict"`; prologue at the top of the file. This setting disables the prologue.
+ ES6 的模块自动采用严格模式，不管你有没有在模块头部加上"use strict";
