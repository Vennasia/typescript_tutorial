+ `undefined`: (default) provide suggestions as warnings to editors
+ `true`: unreachable code is ignored
+ `false`: raises compiler errors about unreachable code
