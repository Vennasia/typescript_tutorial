>In multi-project TypeScript programs, TypeScript will load all of the available projects into memory in order to provide accurate results for editor responses which require a full knowledge graph like ‘Find All References’.
>
>If your project is large, you can use the flag disableReferencedProjectLoad to disable the automatic loading of all projects. Instead, projects are loaded dynamically as you open files through your editor.
>
>在多项目的TypeScript程序中，TypeScript会将所有可用的项目加载到内存中，以便为编辑器的响应提供准确的结果，这需要一个完整的知识图，比如“Find all References”。
>
>如果你的项目很大，你可以使用标志disableReferencedProjectLoad来禁用所有项目的自动加载。相反，项目是在您通过编辑器打开文件时动态加载的。
