默认值为false

为了避免在处理非常大的JavaScript项目时可能出现的内存膨胀问题，TypeScript分配的内存数量是有上限的。打开这个标志将取消限制。

To avoid a possible memory bloat issues when working with very large JavaScript projects, there is an upper limit to the amount of memory TypeScript will allocate. Turning this flag on will remove the limit.
