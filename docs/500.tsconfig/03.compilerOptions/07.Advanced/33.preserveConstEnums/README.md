默认值:false

```tsx
const enum Album {
  JimmyEatWorldFutures = 1,
  TubRingZooHypothesis = 2,
  DogFashionDiscoAdultery = 3,
}

const selectedAlbum = Album.JimmyEatWorldFutures;
if (selectedAlbum === Album.JimmyEatWorldFutures) {
  console.log("That is a great choice.");
}
```

The default `const enum` behavior is to convert any `Album.Something` to the corresponding number literal, and to remove a reference to the enum from the JavaScript completely.

```tsx
"use strict";
const selectedAlbum = 1 /* JimmyEatWorldFutures */;
if (selectedAlbum === 1 /* JimmyEatWorldFutures */) {
    console.log("That is a great choice.");
}
```

With `preserveConstEnums` set to `true`, the `enum` exists at runtime and the numbers are still em
itted.

```tsx
"use strict";
var Album;
(function (Album) {
    Album[Album["JimmyEatWorldFutures"] = 1] = "JimmyEatWorldFutures";
    Album[Album["TubRingZooHypothesis"] = 2] = "TubRingZooHypothesis";
    Album[Album["DogFashionDiscoAdultery"] = 3] = "DogFashionDiscoAdultery";
})(Album || (Album = {}));
const selectedAlbum = 1 /* JimmyEatWorldFutures */;
if (selectedAlbum === 1 /* JimmyEatWorldFutures */) {
    console.log("That is a great choice.");
}
```

This essentially makes such `const enums` a source-code feature only, with no runtime traces.

