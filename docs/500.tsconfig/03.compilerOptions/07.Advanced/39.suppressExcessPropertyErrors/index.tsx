type Point = { x: number; y: number };
const p: Point = { x: 1, y: 3, m: 10 }; //TS2322: Type '{ x: number; y: number; m: number; }' is not assignable to type 'Point'.   Object literal may only specify known properties, and 'm' does not exist in type 'Point'.
