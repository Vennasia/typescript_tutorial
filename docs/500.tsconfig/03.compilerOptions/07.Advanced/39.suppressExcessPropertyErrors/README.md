默认值: false

This disables reporting of excess property errors, such as the one shown in the following example:

```tsx
type Point = { x: number; y: number };
const p: Point = { x: 1, y: 3, m: 10 };
Type '{ x: number; y: number; m: number; }' is not assignable to type 'Point'.
  Object literal may only specify known properties, and 'm' does not exist in type 'Point'.
```

We don’t recommend using this flag in a modern codebase, you can suppress one-off cases where you need it using `// @ts-ignore`.
