Skip type checking of declaration files.

>This can save time during compilation at the expense of type-system accuracy. For example, two libraries could define two copies of the same `type` in an inconsistent way. Rather than doing a full check of all `d.ts` files, TypeScript will type check the code you specifically refer to in your app’s source code.
>
>A common case where you might think to use `skipLibCheck` is when there are two copies of a library’s types in your `node_modules`. In these cases, you should consider using a feature like `yarn’s resolutions` to ensure there is only one copy of that dependency in your tree or investigate how to ensure there is only one copy by understanding the dependency resolution to fix the issue without additional tooling.
>
>
>这可以节省编译期间的时间，但代价是牺牲类型系统的准确性。例如，两个库可以以不一致的方式定义同一类型的两个副本。TypeScript不会对所有的d.ts文件进行全面检查，而是会对你在应用程序源代码中具体引用的代码进行类型检查。
>
>你可能会考虑使用skipLibCheck的一个常见情况是，在你的node_modules中有两个库类型的副本。在这些情况下，您应该考虑使用像yarn的分辨率这样的特性来确保在您的树中只有该依赖项的一个副本，或者研究如何通过理解依赖项解析来确保只有一个副本，从而在不使用其他工具的情况下修复问题。
