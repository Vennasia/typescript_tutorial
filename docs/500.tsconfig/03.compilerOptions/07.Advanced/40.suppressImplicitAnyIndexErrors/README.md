Turning `suppressImplicitAnyIndexErrors` on suppresses reporting the error about implicit anys when indexing into objects, as shown in the following example:

```tsx
const obj = { x: 10 };
console.log(obj["foo"]);
Element implicitly has an 'any' type because expression of type '"foo"' can't be used to index type '{ x: number; }'.
  Property 'foo' does not exist on type '{ x: number; }'.
```

Using `suppressImplicitAnyIndexErrors` is quite a drastic approach. It is recommended to use a `@ts-ignore` comment instead:

```tsx
const obj = { x: 10 };
// @ts-ignore
console.log(obj["foo"]);
```


>实际使用上 这选项貌似没撒作用 并不会报错 (4.24)
