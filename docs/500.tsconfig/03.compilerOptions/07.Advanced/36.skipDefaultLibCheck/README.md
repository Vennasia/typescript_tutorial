默认值: false

> Use `--skipLibCheck` instead. Skip type checking of default library declaration files.
>
> 使用——skipLibCheck代替。跳过默认库声明文件的类型检查。
