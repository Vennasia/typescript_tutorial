>US: [.daɪəɡ'nɑstiks]
>
>UK: [.daɪəɡ'nɒstɪks]
>
>n.	诊断学；诊断语句
>
>Web	诊断功能；诊断程序；诊断法

该选项处于废弃状态, 推荐使用 `extendedDiagnostics`
