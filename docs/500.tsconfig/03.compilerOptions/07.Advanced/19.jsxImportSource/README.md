Declares the module specifier to be used for importing the `jsx` and `jsxs` factory functions when using jsx as `"react-jsx"` or "react-jsxdev" which were introduced in TypeScript 4.1.

With `React 17` the library supports a new form of JSX transformation via a separate import.


```json
{
  "compilerOptions": {
    "target": "esnext",
    "module": "commonjs",
    "jsx": "react-jsx"
  }
}
```
```jsx
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsx_runtime_1 = require("react/jsx-runtime");
const react_1 = __importDefault(require("react"));
function App() {
    return (0, jsx_runtime_1.jsx)("h1", { children: "Hello World" }, void 0);
}
```

```json
{
  "compilerOptions": {
    "target": "esnext",
    "module": "commonjs",
    "jsx": "react-jsx",
    "jsxImportSource": "preact",
    "types": ["preact"]
  }
}
```
+ 或则你也可以直接在tsx文件的上方通过魔法注释改变该编译选项设置 `/** @jsxImportSource preact */`
```jsx
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const jsx_runtime_1 = require("preact/jsx-runtime");
function App() {
    return (0, jsx_runtime_1.jsx)("h1", { children: "Hello World" }, void 0);
}
exports.App = App;
```
