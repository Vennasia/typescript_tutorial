// With strictBindCallApply on
function fn(x: string) {
  return parseInt(x);
}

const n1 = fn.call(undefined, "10");

const n2 = fn.call(undefined, false); //TS2345: Argument of type 'boolean' is not assignable to parameter of type 'string'.
