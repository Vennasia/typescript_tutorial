var UserAccount = /** @class */ (function () {
    function UserAccount(name) {
        this.accountType = "user";
        this.name = name;
        // Note that this.email is not set
    }
    return UserAccount;
}());
