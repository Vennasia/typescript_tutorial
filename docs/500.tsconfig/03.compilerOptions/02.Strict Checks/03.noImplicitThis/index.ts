class Rectangle {
  width: number;
  height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }

  getAreaFunction() {
    return function () {
      return this.width * this.height; //TS2683: 'this' implicitly has type 'any' because it does not have a type annotation.
    };
  }
}
