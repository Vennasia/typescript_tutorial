默认值为:false

该选项可以将多个不同的目录 合并当做 一个目录, 这样文件间互相引用就可以直接通过 `./xxx` 来引用了（ts不会报错）

但这只影响编译是否报错, 并不影响实际生成的js代码, 也就是说如果撒也不做, 实际生成的代码是跑不起来的（该选项生成js文件时并不会打平不同的目录层级）
