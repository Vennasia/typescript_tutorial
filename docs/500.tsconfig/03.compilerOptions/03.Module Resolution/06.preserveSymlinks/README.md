默认值:false

和硬链接软连接有关的一个选项,

硬链接会从链接到的位置开始查找依赖，而软链接会从文件原始位置开始查找依赖。

而只要将 `preserveSymlinks` 置为true, 则软链接也会按照所在的位置开始查找

```json
ln -s ./src/folderA/a.ts ./src/folderB/b.ts
```

>With this enabled, references to modules and packages (e.g. `imports` and `/// <reference type="..." />` directives) are all resolved relative to the location of the symbolic link file, rather than relative to the path that the symbolic link resolves to.

>https://meixg.cn/2021/01/25/ln-nodejs/
> 
>https://meixg.cn/2021/02/02/nodejs-preserve-symlinks/
