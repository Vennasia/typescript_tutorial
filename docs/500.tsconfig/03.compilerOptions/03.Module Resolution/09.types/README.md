>This feature differs from `typeRoots` in that it is about specifying only the exact types you want included, whereas `typeRoots` supports saying you want particular folders.
>
>这个特性与typeroot的区别在于，它只指定您想要包含的确切类型，而typeroot支持表示您想要特定的文件夹。

```json
{
  "compilerOptions": {
    "types": ["node", "jest", "express"]
  }
}
```
+ This tsconfig.json file will only include `./node_modules/@types/node`, `./node_modules/@types/jest` and `./node_modules/@types/express`. Other packages under `node_modules/@types/*` will not be included.
