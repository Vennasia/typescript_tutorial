/*
  即是命名空间又是函数 可以这样写↓↓
*/
declare function jQuery(selector: string): HTMLElement

declare namespace jQuery {
  function ajax(url: string): void
}

export default jQuery;
