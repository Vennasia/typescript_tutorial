>In this case, you can tell the TypeScript file resolver to support a number of custom prefixes to find code. This pattern can be used to avoid long relative paths within your codebase

查看 `-01模块解析/01baseUrl和路径映射`

本例中 由于我们配置了
```json
"paths": {
  "jquery": ["types/jquery.d.ts"] // this mapping is relative to "baseUrl"
}
```

故 `import * as jQuery from 'jquery';` 时, 会找到  `./types/jquery.d.ts`
