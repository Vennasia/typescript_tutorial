使用babel编译ts的话, 如果你 `import xx` 的是一个 `module.exports`导出的, babel默认会在 `module.exports` 下挂个default, 并把这个default给 import

但我们这里用tsc进行类型检查时, 会报: `TS1192: Module '"F:/js_workplace/tutorial/typescript_tutorial/00tsconfig/03.compilerOptions/03.Module Resolution/01.allowSyntheticDefaultImports/utilFunctions"' has no default export.`

想要回避这个报错, 可以使用 `"allowSyntheticDefaultImports": true`
+ 但需要注意的是此时如果你引入的不是一个js文件, 而是一个ts文件, 会报另外一个错误 `utilFunctions.ts is not a module`


>默认值:
>
>module === "system" or esModuleInterop
