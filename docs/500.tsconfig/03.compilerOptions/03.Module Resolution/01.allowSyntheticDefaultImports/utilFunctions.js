const getStringLength = (str) => str.length;

module.exports = {
  getStringLength
};
