默认值: false

>①
>
>`TypeScript`将`CommonJS/AMD/UMD`模块的命名空间导入(例如`import * as foo from "foo"`)等同于`const foo = require("foo")`。这里的事情很简单，但是如果要导入的主要对象是一个原语、一个类或一个函数，它们就不能工作了。ECMAScript规范规定命名空间记录是一个普通对象，命名空间导入(上面例子中的foo)是不可调用的，尽管TypeScript允许这样做
>
>使用 `--esModuleInterop` 后, 如果使用 `import * as foo from "foo"` 这样导入的, 如果我们直接 `foo()`, 它会报错
>
> 产出的代码中 `import * as foo from "foo";` 会被垫子代码 `var foo = __importStar(require("foo"));` 所替换


>②
>
> 支持默认导入: `import express from 'express'; express()`,
>
> 原先还需要这样导入 `import * as express from "express"; express();`
>
> 产出的代码中 `import b from "bar"` 会被垫子代码 `var bar_1 = __importDefault(require("bar"));` 所替换
+ 需要注意的是开启 `esModuleInterop` 会同时开启 `allowSyntheticDefaultImports`.
+ `allowSyntheticDefaultImports` 只是让ts不报错, 而`esModuleInterop`是会生成对应的垫子代码, 让运行时不报错
