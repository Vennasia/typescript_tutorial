"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var foo_1 = __importDefault(require("./foo"));
console.log(foo_1["default"].a);
console.log(foo_1["default"].b);
//如果esModuleInterop不置为true, 这里最后生成的dist里的代码, 点击右键运行, es6引入commonJS会报错
