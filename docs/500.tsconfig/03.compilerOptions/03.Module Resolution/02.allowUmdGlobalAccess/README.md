默认值: false

默认你是无法在一个模块文件里 直接使用 umd 全局导出变量的, 除非你不是在一个模块文件里使用(没有`imports/exports`), 或则你的 `allowUmdGlobalAccess` 选项置为 `true` , 否则你只能通过先 import 这个全局umd变量 再使用
