interface EnvironmentVars {
  NAME: string;
  OS: string;

  // Unknown properties are covered by this index signature.
  [propName: string]: string;
}

declare const env: EnvironmentVars;

// Declared as existing
const sysName = env.NAME;
const os = env.OS;

// Not declared, but because of the index
// signature, then it is considered a string
const nodeEnv = env.NODE_ENV; //开启noUncheckedIndexedAccess选项后, 该nodeEnv的值的类型就不仅是string了 还可能是undefined
