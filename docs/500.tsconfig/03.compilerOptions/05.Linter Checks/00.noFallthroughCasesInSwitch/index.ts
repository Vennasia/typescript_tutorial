const a: number = 6;

switch (a) {
  case 0:
    console.log("even");
    // break //因为设置了 noFallthroughCasesInSwitch, 故这里会报错, 每一个case必须要有break或return
  case 1:
    console.log("odd");
    break;
}
