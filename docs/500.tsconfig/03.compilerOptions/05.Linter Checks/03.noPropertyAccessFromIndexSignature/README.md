https://www.typescriptlang.org/tsconfig#noPropertyAccessFromIndexSignature

该选项开启时, 索引类型只能通过 `[]` 的形式来访问, 而不能通过`.`的形式
