declare function getSettings(): GameSettings;
interface GameSettings {
  speed: "fast" | "medium" | "slow";
  quality: "high" | "low";
  [key: string]: string;
}
// ---cut---
const settings = getSettings();
settings.speed;
settings.quality;

// This would need to be settings["username"];
settings.username; //×
settings['username']; //√
