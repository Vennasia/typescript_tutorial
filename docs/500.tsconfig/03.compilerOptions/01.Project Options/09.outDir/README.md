默认产出的目录是和你的`.ts`文件在同一目录下, 除非你指定了 `outDir`

```
example
├── index.js
└── index.ts
```
```json
{
  "compilerOptions": {
    "outDir": "dist"
  }
}
```
+ 注意这是个相对路径, 相对于 `rootDir`
```
example
├── dist
│   └── index.js
├── index.ts
└── tsconfig.json
```
