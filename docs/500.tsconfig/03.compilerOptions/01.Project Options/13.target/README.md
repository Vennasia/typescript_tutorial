默认值为: `ES3`

注意和`module`区别, `module`是用来指定打包成什么样的模块

`target`是用来指定打包成什么标准的语法(以供浏览器兼容)

更改 `target` 的值 会对应更改 `lib`的默认值

+ ES3
+ ES5
+ ES6/ES2015(synonymous)
+ ES6/ES2016
+ ES2017
+ ES2018
+ ES2019
+ ES2020
+ ESNext
