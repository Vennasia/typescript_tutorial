默认值为: false

置为true后, 会不产出 JavaScript source code, source-maps or declarations.

而只会做类型检查

这通常是因为使用了其它编译工具,比如`babel`。

这样的话, 我们主要把typescript(tsconfig.json)用来作编辑器集成 和 类型检查
