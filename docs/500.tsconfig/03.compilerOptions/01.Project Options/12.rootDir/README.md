>case1 会报错:
>
> error TS6059: File 'F:/js_workplace/tutorial/typescript_tutorial/00tsconfig/03.Project Options/12.rootDir/x.ts' is not under 'rootDir' 'F:/js_workplace/tutorial/ty
pescript_tutorial/00tsconfig/03.Project Options/12.rootDir/src'. 'rootDir' is expected to contain all source files.
The file is in the program because:
Matched by include pattern '**/*' in 'F:/js_workplace/tutorial/typescript_tutorial/00tsconfig/03.Project Options/12.rootDir/tsconfig.json'
>
>
>Found 1 error.
>
>虽然控制台会报错, 但文件还是生成了, 但需要注意的是x.ts不是生成在指定的outDir中, 而是和x.ts处于同一目录下(而`src/`下的都生成在了outDir中)


即如果手动指定了 `rootDir` , 那么所有源文件必须放在手动指定的 `rootDir` 下, 否则会报错(↑)

没有指定的话, 就是所有输入文件(非`.d.ts`)的最长公共路径(如果`composite `为`true`, 则是`tsconfig.json`所处目录)

如果想像case2一样,输出的目录结构中包含`src/`这个目录, 则将`rootDir`置为`.`即可, 但这样的话, `x.ts` 也会被打包编译
