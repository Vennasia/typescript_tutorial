`declaration` 和 `declarationMap` 默认值都为 `false`
  - 如果 `composite` 为true, 则declaration默认值为true

>declaration
>
>Generate `.d.ts` files for every TypeScript or JavaScript file inside your project. These `.d.ts` files are type definition files which describe the external API of your module. With `.d.ts` files, tools like TypeScript can provide intellisense and accurate types for un-typed code.

>declarationMap
>
>Generates a source map for `.d.ts` files which map back to the original `.ts` source file. This will allow editors such as VS Code to go to the original `.ts` file when using features like Go to Definition.
>
>You should strongly consider turning this on if you’re using project references.
