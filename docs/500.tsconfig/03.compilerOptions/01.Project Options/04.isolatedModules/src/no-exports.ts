// 不仅export可以 import 也可以
// import {A} from './types';

const xx = 'xxx'; //TS1208: 'no-exports.ts' cannot be compiled under '--isolatedModules' because it is considered a global script file. Add an import, export, or an empty 'export {}' statement to make it a module.

// 是个空 {} 也可以阻止报错
// export {}
