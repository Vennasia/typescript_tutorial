/*import {A} from './types';

export const a:A = {
  name: 'ahhh',
  age: 123
}

// export {A} //打开会报如下错误： Uncaught SyntaxError: The requested module '/src/types.ts' does not provide an export named 'A'
//之所以报错是因为：
/!*
其它的 转译器, 比如 `babel`, 没有tsc这样的全局把控类型系统的能力, 只能同一时间操作一个文件, 这样的话, 一些原本在tsc环境下能正确编译处理的, 比如你在 `someModule.ts` 里导出了一个类型, 然后在 `index.ts` 里引入了, tsc能正确识别它是一个类型, 并不会在打包编译时将其输出 (然babel这种 single-file transpilation是不能正确识别的)
*!/
//但如果我们 tsconfig.json 里设置了 "isolatedModules": true
//虽然依然不能解决这个问题, 但当我们这样在非tsc的编译下强制导出一个类型时 我们ts能给我们一个提示
//TS1205: Re-exporting a type when the '--isolatedModules' flag is provided requires using 'export type'.
//另外如果你用tsc进行编译, 也会报这个错从而阻止编译

//但↓ 这样导出就不会报错 因为显示的声明导出的就是一个类型
//export type {A}*/



//-------------------------
declare const enum Num {
  First = 0,
  Second = 1
}

export const a = {
  name: 'ahhh',
  age: Num.First //TS2748: Cannot access ambient const enums when the '--isolatedModules' flag is provided.

}

//运行时会直接报： Uncaught ReferenceError: Num is not defined
/*
因为非tsc编译器 不认识 ambient const enums 这种语法的 会把 `declare const enum Num {}` 直接去掉 (因为它不认识)
也就是说这里
export const a = {
  name: 'ahhh',
  age: Num.First
}
并不会像tsc一样编译成
export const a = {
  name: 'ahhh',
  age: 1
}
而是会保留:
export const a = {
  name: 'ahhh',
  age: Num.First
}
故就报 ---> Uncaught ReferenceError: Num is not defined 这个错误了
*/

//开启 --isolatedModules 后, ts能给出提示 --- TS2748: Cannot access ambient const enums when the '--isolatedModules' flag is provided.
//
//同样的如果用tsc进行编译, 此时就会阻止编译并报以上错误
