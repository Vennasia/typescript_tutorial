type someType = (person: Object) => Object

let someFunction = ()=>123;

export {
  someType,
  someFunction
}
//↑ TS1205: Re-exporting a type when the '--isolatedModules' flag is provided requires using 'export type'.
//alt+enter 会提示你改成下面这样导出类型
//export type { someType };

