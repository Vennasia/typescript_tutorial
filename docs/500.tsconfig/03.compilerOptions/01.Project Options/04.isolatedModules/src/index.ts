import { someType, someFunction } from "./someModule";

someFunction();

export { someType, someFunction };
//↑ TS1205: Re-exporting a type when the '--isolatedModules' flag is provided requires using 'export type'.
//alt+enter 会提示你改成下面这样导出类型
//export type { someType };

/*declare */const enum Numbers {
  Zero = 0,
  One = 1,
}
console.log(Numbers.Zero + Numbers.One);
