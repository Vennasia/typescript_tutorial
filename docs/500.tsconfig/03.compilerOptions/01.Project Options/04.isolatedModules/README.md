
## compilerOptions.isolatedModules 选项
### 作用1：防止非显示声明下在模块文件中导出类型
>While you can use TypeScript to produce JavaScript code from TypeScript code, it’s also common to use other transpilers such as `Babel` to do this. However, other transpilers only operate on a single file at a time, which means they can’t apply code transforms that depend on understanding the full type system. This restriction also applies to TypeScript’s `ts.transpileModule` API which is used by some build tools.
>
>These limitations can cause runtime problems with some TypeScript features like `const enums` and `namespaces`. Setting the `isolatedModules` flag tells TypeScript to warn you if you write certain code that can’t be correctly interpreted by a single-file transpilation process.
>

其它的 转译器, 比如 `babel`, 没有tsc这样的全局把控类型系统的能力, 只能同一时间操作一个文件, 这样的话, 一些原本在tsc环境下能正确编译处理的, 比如你在 `someModule.ts` 里导出了一个类型, 然后在 `index.ts` 里引入了, tsc能正确识别它是一个类型, 并不会在打包编译时将其输出 (然babel这种 single-file transpilation是不能正确识别的)

故为了书写的ts代码在使用其它非tsc编译器进行编译后,运行时不报错,我们可以开启`isolatedModules`这个选项, 这样在开发时就会给出提示

---


非tsc编译环境下(使用其它编译器), 模块里如果导出一个类型, 是会报错的

```tsx
// types.ts
export interface A {
  name: string
}

//test.ts
import {A} from './types';

export const a:A = {
  name: 'ahhh',
  age: 123
}

// export {A} //打开会报如下错误： Uncaught SyntaxError: The requested module '/src/types.ts' does not provide an export named 'A'
//之所以报错是因为：
/*
其它的 转译器, 比如 `babel`, 没有tsc这样的全局把控类型系统的能力, 只能同一时间操作一个文件, 这样的话, 一些原本在tsc环境下能正确编译处理的, 比如你在 `someModule.ts` 里导出了一个类型, 然后在 `index.ts` 里引入了, tsc能正确识别它是一个类型, 并不会在打包编译时将其输出 (然babel这种 single-file transpilation是不能正确识别的)
*/
//但如果我们 tsconfig.json 里设置了 "isolatedModules": true
//虽然依然不能解决这个问题, 但当我们这样在非tsc的编译下强制导出一个类型时 我们ts能给我们一个提示
//TS1205: Re-exporting a type when the '--isolatedModules' flag is provided requires using 'export type'.
//另外如果你用tsc进行编译, 也会报这个错从而阻止编译

//但↓ 这样导出就不会报错 因为显示的声明导出的就是一个类型
//export type {A}
```

### 作用2: 防止在模块中使用ambient const enums导致报错
```tsx
declare const enum Num {
  First = 0,
  Second = 1
}

export const a = {
  name: 'ahhh',
  age: Num.First //TS2748: Cannot access ambient const enums when the '--isolatedModules' flag is provided.

}

//运行时会直接报： Uncaught ReferenceError: Num is not defined
/*
因为非tsc编译器 不认识 ambient const enums 这种语法的 会把 `declare const enum Num {}` 直接去掉 (因为它不认识)
也就是说这里
export const a = {
  name: 'ahhh',
  age: Num.First
}
并不会像tsc一样编译成
export const a = {
  name: 'ahhh',
  age: 1
}
而是会保留:
export const a = {
  name: 'ahhh',
  age: Num.First
}
故就报 ---> Uncaught ReferenceError: Num is not defined 这个错误了
*/

//开启 --isolatedModules 后, ts能给出提示 --- TS2748: Cannot access ambient const enums when the '--isolatedModules' flag is provided.
//
//同样的如果用tsc进行编译, 此时就会阻止编译并报以上错误
```
### 作用3: 强制每个ts文件必须要有一个export
见 `01.vue3/src/no-exports.ts`

```tsx
// 不仅export可以 import 也可以
// import {A} from './types';

const xx = 'xxx'; //TS1208: 'no-exports.ts' cannot be compiled under '--isolatedModules' because it is considered a global script file. Add an import, export, or an empty 'export {}' statement to make it a module.

// 是个空 {} 也可以阻止报错
// export {}
```
