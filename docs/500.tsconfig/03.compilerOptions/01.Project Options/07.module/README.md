指定打包后的模块类型

默认值: `CommonJS` (default if `target` is `ES3` or `ES5`)

可选值有:
+ ES2015
+ ES2020
+ None
+ UMD
+ AMD
+ System
+ ESNext

改变 `module` 选项同样会影响到 `moduleResolution`选项

```jsx
// @filename: index.ts
import { valueOfPi } from "./constants";

export const twoPi = valueOfPi * 2;
```


ESNext
```jsx
import { valueOfPi } from "./constants";
export const twoPi = valueOfPi * 2;
```

ES2020
```jsx
import { valueOfPi } from "./constants";
export const twoPi = valueOfPi * 2;
```
+ If you are wondering about the difference between ES2015 and ES2020, ES2020 adds support for `dynamic imports`, and `import.meta`.


CommonJS
```jsx
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.twoPi = void 0;
const constants_1 = require("./constants");
exports.twoPi = constants_1.valueOfPi * 2;
```

None
```jsx
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.twoPi = void 0;
const constants_1 = require("./constants");
exports.twoPi = constants_1.valueOfPi * 2;
```
+ emmm, 和 CommonJS 没区别？

UMD
```jsx
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./constants"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.twoPi = void 0;
    const constants_1 = require("./constants");
    exports.twoPi = constants_1.valueOfPi * 2;
});
```

AMD
```jsx
define(["require", "exports", "./constants"], function (require, exports, constants_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.twoPi = void 0;
    exports.twoPi = constants_1.valueOfPi * 2;
});
```

System
```jsx
System.register(["./constants"], function (exports_1, context_1) {
    "use strict";
    var constants_1, twoPi;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (constants_1_1) {
                constants_1 = constants_1_1;
            }
        ],
        execute: function () {
            exports_1("twoPi", twoPi = constants_1.valueOfPi * 2);
        }
    };
});
```
