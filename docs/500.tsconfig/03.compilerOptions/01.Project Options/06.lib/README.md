> 默认lib是有值的, 并且一旦你手动指定了lib, 那么默认生效的那些就失效了, 比如本例中lib只指定了es6, 没有指定dom, So index.ts里我们用到document的话 会报Cannot find name 'document

TypeScript includes a default set of `type definitions` for built-in JS APIs (like `Math`), as well as type definitions for things found in browser environments (like `document`). TypeScript also includes APIs for newer JS features matching the `target` you specify; for example the definition for `Map` is available if `target` is `ES6` or newer.

You may want to change these for a few reasons:
+ Your program doesn’t run in a browser, so you don’t want the `"dom"` type definitions
+ Your runtime platform provides certain JavaScript API objects (maybe through polyfills), but doesn’t yet support the full syntax of a given ECMAScript version
+ You have polyfills or native implementations for some, but not all, of a higher level ECMAScript version

## High Level libraries
|Name|Contents|
|---|---|
|ES5|Core definitions for all ES3 and ES5 functionality|
|ES2015|Additional APIs available in ES2015 (also known as ES6) - array.find, Promise, Proxy, Symbol, Map, Set, Reflect, etc.|
|ES6|Alias for “ES2015”|
|ES2016|Additional APIs available in ES2016 - array.include, etc.|
|ES7|Alias for “ES2016”|
|ES2017|Additional APIs available in ES2017 - Object.entries, Object.values, Atomics, SharedArrayBuffer, date.formatToParts, typed arrays, etc.|
|ES2018|Additional APIs available in ES2018 - async iterables, promise.finally, Intl.PluralRules, rexexp.groups, etc.|
|ES2019|Additional APIs available in ES2019 - array.flat, array.flatMap, Object.fromEntries, string.trimStart, string.trimEnd, etc.|
|ES2020|Additional APIs available in ES2020 - string.matchAll, etc..|
|ESNext|Additional APIs available in ESNext - This changes as the JavaScript specification evolves|
|DOM|DOM definitions - window, document, etc.|
|WebWorker|APIs available in WebWorker contexts|
|ScriptHost|APIs for the Windows Script Hosting System|

## individual library components
like DOM.Iterable、ES2015.Core、ES2015.Collection、ES2015.Generator...

>https://github.com/microsoft/TypeScript/tree/master/lib
