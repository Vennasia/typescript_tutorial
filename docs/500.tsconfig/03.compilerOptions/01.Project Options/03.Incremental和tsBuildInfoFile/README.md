`incremental` 选项主要用来加快ts的构建速度(不会影响js运行时)
  - 默认会在你指定的 `outDir` 下创建`tsconfig.tsbuildinfo`文件,用以存储上一次的构建信息
  - 默认值: `true` if `composite`, `false` otherwise

`tsBuildInfoFile`用来自定义`.tsbuildinfo`文件的位置
