define("dog", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    exports.dog = void 0;
    var dog = "dog";
    exports.dog = dog;
});
define("index", ["require", "exports", "dog"], function (require, exports, dog_1) {
    "use strict";
    exports.__esModule = true;
    var fs = require('fs');
    console.log(fs);
    console.log(dog_1.dog);
});
