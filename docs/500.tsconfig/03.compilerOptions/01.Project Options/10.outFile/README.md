适用于非模块文件(没有import/export等导入导出关键字)

适用于`module`选项置为`System`或`AMD`的模块文件 (但不适用于module为`CommonJS`或`ES6 modules`)

打包文件的输出位置 不会受 outDir 影响

> https://www.typescriptlang.org/tsconfig#outFile
>
>If specified, all global (non-module) files will be concatenated into the single output file specified.
>
>If module is system or amd, all module files will also be concatenated into this file after all global content.
>
>Note: outFile cannot be used unless module is None, System, or AMD. This option cannot be used to bundle CommonJS or ES6 modules.
