target ES6 以下, 会生成一些垫子代码, 如果有多个模块用到了要生成垫子代码的语法 多个模块就都会生成这些垫子代码

故你可以使用 `importHelpers` 这个选项, 它会编译成从 `tslib` 里这个库里引某个函数来使用

```tsx
var tslib_1 = require("tslib");
function fn(arr) {
  var arr2 = tslib_1.__spreadArray([1], arr);
}
```
而不是在每个需要的模块里直接生成
```tsx
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
function fn(arr) {
    var arr2 = __spreadArray([1], arr);
}
```

另外 tslib 也支持为 `downlevelIteration` 选项提供垫子代码的方法

