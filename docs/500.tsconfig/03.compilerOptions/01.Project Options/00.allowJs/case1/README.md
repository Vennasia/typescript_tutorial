默认为 false

如果没有设置 `allowJs` 的话, `card.js` 并不会被打包编译到 `outDir` 下, 即使设置了 `"include": ["src/**/*.*"]`

但只要设置了 `allowJs` 为 `true`, 那么 js 文件也会被打包编译到指定的 `outDir` 下
