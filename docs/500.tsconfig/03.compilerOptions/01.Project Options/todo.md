## composite
>composite
>
>US: [kəm'pɑzɪt]
>UK: ['kɒmpəzɪt]
>
>n.	复合材料；混合物；合成物
>
>adj.	合成的；混成的；复合的
>
>Web	组合；合成模式；复合视频

composite
  - 默认值为false

设置为true时,会有以下影响:
+ The `rootDir` setting, if not explicitly set, defaults to the directory containing the `tsconfig.json` file.
+ All implementation files must be matched by an `include` pattern or listed in the `files` array. If this constraint is violated, `tsc` will inform you which files weren’t specified.
+ `declaration` defaults to true
+ `incremental`选项 默认会变为 true

## downlevelIteration
`downlevelIteration`: Downleveling is TypeScript’s term for transpiling to an older version of JavaScript. This flag is to enable support for a more accurate implementation of how modern JavaScript iterates through new concepts in older JavaScript runtimes.
  - 默认值为false
  - 一般用不到

## importHelpers
`importHelpers`: 如果你开启了 `downlevelIteration`, TypeScript 会注入一些助手函数, 因为可能多个模块之间会注入相同的助手函数造成代码冗余, 故你可以开启 `importHelpers` 这个选项, 这样它就会从 `tslib` 里引入这些助手函数, 而不是在每个模块中单独注入 (注意,你需要确保运行时时,`tslib`是被引入的)
  - 默认值为false

## plugins
>https://www.typescriptlang.org/tsconfig#plugins
>

## sourceMap
默认为false
