默认为 false

开启 `checkJs` 后, 你会发现 `parseFloat(3.1415)` 画红了, 因为 `parseFloat` 对应的 `.d.ts` 文件生效了 ---> `lib.es5.d.ts` ---> `declare function parseFloat(string: string): number;`

除了内置的一些lib的`.d.ts`生效外, 还能够检测那些没有声明但却使用了的变量、方法
