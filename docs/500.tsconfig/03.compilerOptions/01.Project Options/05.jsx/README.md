只对 `.tsx` 文件有效
+ 默认值为 undefined
+ `react`: Emit `.js` files with JSX changed to the equivalent `React.createElement` calls
+ `react-jsx`: Emit `.js` files with the JSX changed to `_jsx` calls
+ `react-jsxdev`: Emit `.js` files with the JSX to `_jsx` calls
+ `preserve`: Emit `.jsx` files with the JSX unchanged
+ `react-native`: Emit `.js` files with the JSX unchanged

```jsx
export const helloWorld = () => <h1>Hello world</h1>;
```

"react" 输出的文件后缀为 `.js`
```jsx
export const helloWorld = () => React.createElement("h1", null, "Hello world");
```

"preserve", 输出的文件后缀为 `.jsx`
```jsx
export const helloWorld = () => <h1>Hello world</h1>;
```

"react-native", 输出的文件后缀为 `.js`
```jsx
export const helloWorld = () => <h1>Hello world</h1>;
```

"react-jsx", 输出的文件后缀为 `.js`
```jsx
import { jsx as _jsx } from "react/jsx-runtime";
export const helloWorld = () => _jsx("h1", { children: "Hello world" }, void 0);
```

"react-jsxdev", 输出的文件后缀为 `.js`
```jsx
import { jsxDEV as _jsxDEV } from "react/jsx-dev-runtime";
const _jsxFileName = "/home/runner/work/TypeScript-Website/TypeScript-Website/packages/typescriptlang-org/index.tsx";
export const helloWorld = () => _jsxDEV("h1", { children: "Hello world" }, void 0, false, { fileName: _jsxFileName, lineNumber: 7, columnNumber: 32 }, this);
```
