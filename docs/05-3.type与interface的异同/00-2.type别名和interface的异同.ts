//https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#differences-between-type-aliases-and-interfaces
//类型别名和接口非常相似，在许多情况下，您可以自由选择它们。
//接口的几乎所有特性在类型中都是可用的，关键的区别是类型不能重新打开以添加新属性，而接口总是可扩展的。
//
//除此之外:

//1) interface中不能用in操作符, type是可以的

//2) type可以使用联合类型或者交叉类型, interface不能

//3) type不能继承和实现, interface可以

//4) type不能重复定义, interface重复定义可以合并
//× TS2300: Duplicate identifier 'A'.
/*type A = string;
type A = number;*/
//√
interface B {}
interface B {}



//5) type 和 接口 之间有等价的写法
/*type ICreateArray = <T>(times: number, val: T) => T[];
//↕ 等价
interface ICreateArray2<T> {
  (times: number, val: T):T[];
}*/

type ICreateArray<T> = (times: number, val: T) => T[];
//↕ 等价
interface ICreateArray2 {
  <T>(times: number, val: T):T[];
}


export = {}
