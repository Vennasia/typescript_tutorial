export = {}

//ReturnType<Type>
function getUser(){
  return {name: 'ahhh', age: 123};
}

// infer表示推导, 后面的R, 就相当于一个变量, 会帮我们自动推导出值并赋上
// type ReturnType<T extends (...args: any) => any> = (...args: any) => infer R //←这样写无法返回 必须搭配条件类型(即三元表达式)
type ReturnType<T extends (...args: any[]) => any> = T extends (...args: any[]) => infer R ? R : any;
type returnType = ReturnType<typeof getUser>
/*
type returnType = {
  name: string;
  age: number;
}
*/
