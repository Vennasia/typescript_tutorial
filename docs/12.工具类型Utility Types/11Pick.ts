export = {}


interface Person {
  name?: string;
  age: number;
  address: string;
  company: any;
}


//Pick<Type, Keys>
// Constructs a type by picking the set of properties Keys from Type.
type Pick<T, K extends keyof T> = {
  [P in K]: T[P];
};

type PickPerson = Pick<Person, 'name' | 'age'>
let person:PickPerson = {
  age:123
}
