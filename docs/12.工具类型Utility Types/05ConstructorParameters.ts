export = {}

// 取构造函数类型
//Constructs a tuple or array type from the types of a constructor function type. It produces a tuple type with all the parameter types (or the type never if Type is not a function).
class Animal {
  constructor(public name: string,public age: number) {
    this.name = name
    this.age = age
  }
}

type ConstructorParameters<T extends {new (...args:any[]):any}> = T extends { new(...args: infer P): any;}?P:any
type MyConstructor = ConstructorParameters<typeof Animal> //type MyConstructor = [string, number]

type x = typeof Animal //这样取的是Animal的构造函数
let o:x = {} //TS2741: Property 'prototype' is missing in type '{}' but required in type 'typeof Animal'.
let o2:Animal = {} //TS2739: Type '{}' is missing the following properties from type 'Animal': name, age
