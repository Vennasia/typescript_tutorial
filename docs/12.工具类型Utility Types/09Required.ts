export = {}
interface Person {
  name?: string;
  age: number;
  address: string;
  company: any;
}

//Required<Type>
//Constructs a type consisting of all properties of T set to required. The opposite of Partial.
type Required<T> = { [K in keyof T]-?/*-？ 减掉?修饰符 表示必填*/: T[K] };

type MyPerson = Required<Person>

let person:MyPerson = {

}
