export = {}

//Parameters<Type>
//Constructs a tuple type from the types used in the parameters of a function type Type.

function getUser(name:string,age:number){
  return {name, age};
}

type Parameters<T extends (...args: any[]) => any> = T extends (...args: infer P) => any ? P : any;
type returnType = Parameters<typeof getUser> //type returnType = [string, number]
