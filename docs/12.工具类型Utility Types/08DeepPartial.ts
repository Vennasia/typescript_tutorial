export = {}

interface Company {
  name: string;
  age: number;
  address: string;
}

interface Person {
  name: string;
  age: number;
  company: Company;
}

type Partial<T> = { [K in keyof T]?: T[K] };
type DeepPartial<T> = { [K in keyof T]?: T[K] extends object ? DeepPartial<T[K]>:T[K] };

type MyPerson = Partial<Person>;

// let person:MyPerson = {
//   company: { //TS2739: Type '{}' is missing the following properties from type 'Company': name, age, address
//
//   }
// }

let person:DeepPartial<Person> = {
  company: {

  }
} // 实现一个深层次的嵌套树类型 children 可能就是可有可无
