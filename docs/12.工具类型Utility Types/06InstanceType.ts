export = {};
// InstanceType<Type>
// Constructs a type consisting of the instance type of a constructor function in Type.

class Animal {
  constructor(public name: string,public age: number) {
    this.name = name
    this.age = age
  }
}
type InstanceType<T extends {new (...args:any[]):any}> = T extends { new(...args: any[]): infer R;}?R:any
type MyConstructor = InstanceType<typeof Animal> //type MyConstructor = Animal

// let x: MyConstructor;
// x.name
// x.age
