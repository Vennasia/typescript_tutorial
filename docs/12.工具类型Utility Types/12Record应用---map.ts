export = {}

function map<T extends keyof any/*string|number|symbol*/,K,U>(obj:Record<T,K>,cb:(item:K,key:T)=>U):Record<T, U>{
  let result = {} as Record<T, U>;
  for (const key in obj) {
    result[key] = cb(obj[key],key)
  }
  return result
}


map({name:'ahhh',age:123},(item,key)=>{
  return item + 'ok'
})
//function map<"name" | "age", string | number, string>(obj: Record<"name" | "age", string | number>, cb: (item: string | number, key: "name" | "age") => string): Record<"name" | "age", string>

//type X = Record<"name" | "age", any> //type Record<K extends string | number | symbol, T> = { [P in K]: T; }
