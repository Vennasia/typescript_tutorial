export = {}

// Record<Keys,Type>
// Constructs a type with a set of properties Keys of type Type. This utility can be used to map the properties of a type to another type.
// 构造具有一组类型的属性键的类型。此工具方法可用于将一种类型的属性映射到另一种类型。
//
// Record是用来描述对象字面量的, 有哪些key？返回值是怎样的？
// 缺点是并不能准确的描述哪些key的返回类型详细是什么类型, 即只能准确的描述key, 不能准确的描述key对应的返回类型

// let obj = {name:'ahhh',age:123}
/*
let obj: {
  name: string;
  age: number;
}
*/

type Record<K extends keyof any, T> = {
  [P in K]: T; //[string|number|symbol]:T
};
// type test = keyof any //type test = string | number | symbol

// let obj = {name:'ahhh',age:123}
/*
let obj: {
  name: string;
  age: number;
}
*/
//↑等价(准确来说并不完全等价)于↓
let obj:Record<'name' | 'age', string | number> = {name:'ahhh',age:123}
//比如这里的age:123 也可以是 age:'123'

let obj2:Record<string/*key的类型*/, any/*值的类型*/> = {name:'ahhh',age:123}
obj2.xx //不会报错
// obj2.name. //点不出来本应该属于字符串类型的方法
obj.name.split() //能点出来本应该属于字符串类型的方法


// Record<string|number, any>
//↑生成的类型,相当于↓
interface X{
  [k:string]: any;
  [k:number]: any; //题外话：实际上这一行是不需要的, [k:string]: any就包含[k:number]: any了, 因为js里key都会调用toString
}
let x:X = {0/*js里key都会调用toString*/:1,'a':2}
