export = {}
interface Person {
  name: string;
  age: number;
  address: string;
  company: any;
}

//Partial<Type>
//Constructs a type with all properties of Type set to optional. This utility will return a type that represents all subsets of a given type.
// 将所有属性 变为可选属性
type Partial<T> = { [K in keyof T]?: T[K] };

type MyPerson = Partial<Person>
/* MyPerson ↓
type MyPerson = {
  name?: string | undefined;
  age?: number | undefined;
  address?: string | undefined;
  company?: any;
}
*/
