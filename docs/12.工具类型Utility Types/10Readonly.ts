export = {}
interface Person {
  name?: string;
  age: number;
  address: string;
  company: any;
}

//Readonly<Type>
//Constructs a type with all properties of Type set to readonly, meaning the properties of the constructed type cannot be reassigned.
type Readonly<T> = { readonly [K in keyof T]: T[K] };

type MyPerson = Readonly<Person>

let person:MyPerson = {
  age:123,
  address:'123',
  company:''
}
person.age = 222 //Attempt to assign to const or readonly variable
