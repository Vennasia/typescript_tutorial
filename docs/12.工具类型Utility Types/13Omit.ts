export = {}

interface IPerson {
  name?: string;
  age: number;
  company: Company;
}

interface Company {
  name: string;
  age: number;
  address: string;
}


// type iperson = Omit<IPerson,'company'> & {company?:Company}
// let p:iperson = {
//   name:'ahhh',
//   age:123,
//   //company可选了
// }

//Omit<Type, Keys>
//Constructs a type by picking all properties from Type and then removing Keys.
type Omit<T,K extends keyof T> = Pick<T,Exclude<keyof T, K>>
type iperson = Omit<IPerson, 'company'>
/*
type iperson = {
  name?: string | undefined;
  age: number;
}
*/
