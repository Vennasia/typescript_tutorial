export = {}

type NonNullable<T> = T extends null | undefined ? never : T
type returnType = NonNullable<string|number|null|boolean>
type x = ReturnType<any>
