export = {}
import * as React from "react";

interface Layout2 extends React.FunctionComponent{
  Header?: React.FunctionComponent;
}

/** 接口名和变量名相同不会冲突,为什么可以这样呢？
 因为ts可以非常明确的知道,当你写layout2的时候,你到底指的是接口,还是值的变量
 如果你在`:`的后面或则`interface`关键字开头 很明显是类型 这不会弄错的*/
const Layout2: Layout2 = () => {
  return <div>div</div>;
};

Layout2.Header = () => {
  return <div>hi</div>
};


/** 但需要注意的是declare和变量名是会发生冲突的
  但declare我们一般不会发生这种情况,因为是写在专门的.d.ts文件里的
  .d.ts文件里是不允许写实现的,故也就不会发生冲突*/
const calendar = (options) => {

};
declare calendar = (x:string):void
