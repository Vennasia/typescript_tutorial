var Calculator = /** @class */ (function () {
    function Calculator() {
        this.n1 = null;
        this.n2 = null;
        this.result = null;
        this.keys = [["clear", "÷"], ["7", "8", "9", "x"], ["4", "5", "6", "-"], ["1", "2", "3", "+"], ["0", ".", "="]];
        this.createContainer();
        this.createOutput();
        this.createButtons();
        this.bindEvents();
    }
    Calculator.prototype.createButton = function (text, container, className) {
        var button = document.createElement('button');
        button.textContent = text;
        if (className) {
            button.className = className;
        }
        container.appendChild(button);
        return button;
    };
    Calculator.prototype.createContainer = function () {
        var calcContainer = document.createElement('div');
        calcContainer.classList.add('calculator');
        document.body.appendChild(calcContainer);
        this.calcContainer = calcContainer;
    };
    Calculator.prototype.createOutput = function () {
        var output = document.createElement('div');
        output.classList.add('output');
        var span = document.createElement('span');
        span.textContent = '0';
        output.appendChild(span);
        this.calcContainer.appendChild(output);
        this.output = output;
        this.span = span;
    };
    Calculator.prototype.createButtons = function () {
        var _this = this;
        this.keys.forEach(function (textList) {
            var div = document.createElement('div');
            div.classList.add('row');
            textList.forEach(function (text) {
                _this.createButton(text, div, "button text-" + text);
            });
            _this.calcContainer.appendChild(div);
        });
    };
    Calculator.prototype.bindEvents = function () {
        var _this = this;
        this.calcContainer.addEventListener('click', function (event) {
            if (event.target instanceof HTMLButtonElement) {
                var button = event.target;
                var text = button.textContent /*event.target并不一定有textContent属性,故这里用instanceof进行了类型保护,确保了button的类型*/;
                _this.updateNumbersOrOperator(text);
            }
        });
    };
    Calculator.prototype.updateN = function (which, text) {
        if (this[which]) {
            this[which] += text;
        }
        else {
            this[which] = text;
        }
        this.span.textContent = this[which].toString();
    };
    Calculator.prototype.updateNumbers = function (text) {
        if (this.operator) {
            this.updateN('n2', text);
        }
        else {
            this.updateN('n1', text);
        }
    };
    Calculator.prototype.updateResult = function () {
        var result;
        var n1 = parseFloat(this.n1);
        var n2 = parseFloat(this.n2);
        if (this.operator === '+') {
            result = n1 + n2;
        }
        else if (this.operator === '-') {
            result = n1 - n2;
        }
        else if (this.operator === 'x') {
            result = n1 * n2;
        }
        else if (this.operator === '÷') {
            result = n1 / n2;
        }
        result = result.toPrecision(12).replace(/0+$/, '').replace(/0+e/g, 'e');
        if (n2 === 0) {
            result = '不是数字';
        }
        this.span.textContent = result.toString();
        this.n1 = null;
        this.n2 = null;
        this.operator = null;
        this.result = result;
    };
    Calculator.prototype.updateOperator = function (text) {
        if (!this.n1) {
            this.n1 = this.result;
        }
        this.operator = text;
    };
    Calculator.prototype.updateNumbersOrOperator = function (text) {
        if ('0123456789.'.indexOf(text) >= 0) {
            this.updateNumbers(text);
        }
        else if ('+-x÷'.indexOf(text) >= 0) {
            this.updateOperator(text);
        }
        else if ('='.indexOf(text) >= 0) {
            this.updateResult();
        }
        else if (text === 'Clear') {
            this.n1 = null;
            this.n2 = null;
            this.operator = null;
            this.result = null;
            this.span.textContent = '0';
        }
    };
    return Calculator;
}());
new Calculator();
