#!/usr/bin/env ts-node

// 因为我们安装了@types/node 故下面会报错(鼠标点击.argv 可看见其声明为argv: string[])
// let /*TS2322: Type 'string' is not assignable to type 'number'.*/a:number = process.argv[2];

let a:number = parseInt(process.argv[2]);
let b:number = parseInt(process.argv[3]);

// ↓↓ 有可能会报错 是因为typescript默认内置类型库只支持到es5(es2014)
if (Number.isNaN(a) || Number.isNaN(b)) {
  console.log('只接受整数');
  process.exit(1); // 非0 表示非正常退出
}

console.log(a + b);
process.exit(0);

export {}
