[toc]

```shell script
npm i -g typescript --force
npm i -g ts-node --force
```
`ts-node`搭配编辑器的插件(`webstorm`搜索`Run Configuration for TypeScript`,`vscode`的话`run coder`插件即可)

就能实现 右键 运行typescript的程序(像右键运行js文件一样)

可能还需要安装
```shell script
npm i -D @types/node
```
ts-node只是能让我们像 node xxx 一样 使用ts-node xxx 运行ts文件

但ts文件里如果使用了node的内置类,需要它们的声明文件,`ts-node`这个包并没有自带这些声明文件,故需要下载`@types/node`

这里是下载到了项目根目录的`node_modules`下

```
tsc --init # 生成tsconfig.json
tsc xxx # 可以将ts文件编译成js文件
tsc --watch # 监控ts文件变化生成js文件
```

## 让ts-node作为蛇棒使用
查看 00ts-node和@types斜杠node/01.ts

![](readme-assets/00.png)
