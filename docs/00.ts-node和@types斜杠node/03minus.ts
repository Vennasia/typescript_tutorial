#!/usr/bin/env ts-node

let a:number = parseInt(process.argv[2]);
let b:number = parseInt(process.argv[3]);

// ↓↓ 有可能会报错 是因为typescript默认内置类型库只支持到es5(es2014)
if (Number.isNaN(a) || Number.isNaN(b)) {
  console.log('只接受整数');
  process.exit(1); // 非0 表示非正常退出
}

console.log(a - b);
process.exit(0);

export {} // ← 注意如果不加这个 typescript 就会认为你代码写在全局的 上面的a、b 就会报redclare
