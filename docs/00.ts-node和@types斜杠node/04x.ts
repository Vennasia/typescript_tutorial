#!/usr/bin/env ts-node

class Person {
  // children: Person[]/*←如果不写typescript检查不到代码实际上运行时会报错,so一般在tsconfig.json将strictPropertyInitialization置为true*/;
  children: Person[] = []/*←如果不写typescript检查不到代码实际上运行时会报错,so一般在tsconfig.json将strictPropertyInitialization置为true*/;
  constructor(public name: string) {}

  addChild(child: Person): void {
    this.children.push(child);
  }

  introduceFamily(n?: number): void {
    if(!n) n = 0;
    let prefix = '---'.repeat(n);
    console.log(`${prefix}${this.name}`);
    this.children.forEach(child => {
      child.introduceFamily(n! + 1);
    });
  }
}

let a = new Person('灭霸');
let achild1 = new Person('灭霸他儿');
let achild2 = new Person('灭霸他女儿');
a.addChild(achild1);
a.addChild(achild2);

let bchild1 = new Person('灭霸他儿的儿');
let bchild2 = new Person('灭霸他儿的女儿');
achild1.addChild(bchild1);
achild1.addChild(bchild2);

a.introduceFamily();

export {}
