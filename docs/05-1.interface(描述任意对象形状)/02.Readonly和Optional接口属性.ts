export = {}
/** 只读属性 可选属性*/
interface Shape {
  head: string;
  body: string;
}
interface Human {
  readonly name: string;  /** Readonly properties*/
  age: number;
  shape: Shape;
  likeGame?: Array<string>;   /** Optional Properties*/
  // 有名字的是描述方法 没名字的是描述函数
  say(word: string): void;
}

let ahhh: Human = {name: 'ahhh', age: 123, shape: {head: '○', body: '□'}, say(word:string){console.log(`${this.name}:${word}`)}}; // 没有声明likeGame 也不会报错

ahhh.name = 'ahhh2'; // TS2540: Cannot assign to 'name' because it is a read-only property.
ahhh.say('I am ahhh');
