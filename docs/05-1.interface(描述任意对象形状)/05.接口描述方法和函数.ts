// 当一个函数是一个对象的属性时
// 我们把这个函数称之为该对象的方法

/** 接口定义方法时这样定义*/
interface i1 {
  //一般用 ↓ 的写法来描述原型上的方法
  fn(): void;

  //一般用 ↓ 的写法来描述实例上的方法 ---> xxx:()=>{} or this.xxx = function(){}
  // fn: () => void

  //但ts中并不是强制区分的
  //它识别不了是实例上还是原型上的
  //但在抽象类中是有区别的 是能识别的
}

let obj:i1 = {
  // fn 是 obj 的方法 (method)
  fn: function () {
    console.log(2);
  }
};


/** 接口定义函数时这样定义*/
interface i2 {
  (): void;
  n: string;
}

// function fn(){} //√
const fn = function(){} //√
// let fn = function(){} //× //todo
fn.n = '123';

// fn2是个函数
let fn2: i2 = fn;

