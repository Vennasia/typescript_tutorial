export = {};

// function create(C){
//   return new C();
// }

// 怎么让ts知道C是一个类呢？
/** 这样ts就能知道C必须能够用new去调他,在ts中能用new去掉 只有类了*/
// function create(C: { new(): any;/*默认就是any,可以省去*/}){
//   return new C();
// }


// 即若我们期望创建一个特定类型的对象
function create<T>(C: { new(): T }) {
  return new C();
}

class Human {
  a: string = '';
}

class Animal{
  b: string = '';
  // a: string = '';
}

const ahhh = create<Human>(Animal);
// TS2345: Argument of type 'typeof Animal' is not assignable to parameter of type 'new () => Human'.
// Property 'a' is missing in type 'Animal' but required in type 'Human'
/*↑
typescript类型判断是鸭式判断,只要源对象上包含有目标对象上所有属性即可,而不管这个源对象和目标对象是不是同一个,
So 如果我们把Animal中的 `b: string = ''` 改为 `a: string = ''` 就不会报错了*/

//查看 04.classes/00-1.构造函数.ts
