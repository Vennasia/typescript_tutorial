export = {};
/** 与C#或Java里接口的基本作用一样，TypeScript也能够用它(interface)来明确的强制一个类去符合某种契约（去implements它）。
One of the most common uses of interfaces in languages like C# and Java, that of explicitly enforcing that a class meets a particular contract, is also possible in TypeScript.

或则你可以这样理解 不同类之间**公有**的属性，可以抽象成一个接口(Interface)(没错接口只能约束类的公有属性)
*/

/** class A implements B
这是要求A这个类的实例都符合 B接口的的定义*/
// interface ClockInterface {
//   currentTime: Date;
// }
//
// class Clock implements ClockInterface {
//   currentTime: Date = new Date();
//   constructor(h: number, m: number) { }
// }



//你也可以在接口中描述一个方法，在类里实现它，如同下面的setTime方法一样：
//You can also describe methods in an interface that are implemented in the class, as we do with setTime in the below example:
// interface ClockInterface {
//   currentTime: Date;
//   setTime(d: Date);
// }
//
// class Clock implements ClockInterface {
//   currentTime: Date;
//   setTime(d: Date) {
//     this.currentTime = d;
//   }
//   constructor(h: number, m: number) { }
// }
/** ↑
接口描述了类的公共部分，而不是公共和私有两部分。 它不会帮你检查类是否具有某些私有成员。*/
//Interfaces describe the public side of the class, rather than both the public and private side. This prohibits you from using them to check that a class also has particular types for the private side of the class instance.



//当你操作类和接口的时候，你要知道类是具有两个类型的：静态部分的类型和实例的类型。 你会注意到，当你用构造器签名去定义一个接口并试图定义一个类去实现(implements)这个接口时会得到一个错误：
// interface ClockConstructor {
//   new (hour: number, minute: number);
// }
//
// class Clock implements ClockConstructor {
//   //TS2420: Class 'Clock' incorrectly implements interface 'ClockConstructor'.
//   //Type 'Clock' provides no match for the signature 'new (hour: number, minute: number): any'.
//   currentTime: Date;
//   constructor(h: number, m: number) { }
// }
/** ↑
这里因为当一个类实现(implements)一个接口时，只对其实例部分进行类型检查。 constructor存在于类的静态部分，所以不在检查的范围内。
 emmm... 实际上实例上也能访问到constructor这个属性,这里只能说,typescript中,就是不能implements一个定义了构造函数的接口*/



/** 你可以通过下面这种方式，class expressions的方式来限定构造函数*/
interface ClockConstructor {
  new (hour: number, minute: number):any;
}

interface ClockInterface {
  tick();
}

const Clock: ClockConstructor = class Clock implements ClockInterface {
  constructor(h: number, m: number) {}
  tick() {
    console.log("beep beep");
  }
}
/* 法二： 很麻烦,不推荐↓
interface ClockConstructor {
    new (hour: number, minute: number): ClockInterface;
}
interface ClockInterface {
    tick();
}

function createClock(ctor: ClockConstructor, hour: number, minute: number): ClockInterface {
    return new ctor(hour, minute);
}

class DigitalClock implements ClockInterface {
    constructor(h: number, m: number) { }
    tick() {
        console.log("beep beep");
    }
}
class AnalogClock implements ClockInterface {
    constructor(h: number, m: number) { }
    tick() {
        console.log("tick tock");
    }
}

let digital = createClock(DigitalClock, 12, 17);
let analog = createClock(AnalogClock, 7, 32);
*/
