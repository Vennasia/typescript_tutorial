/**
 当接口继承了一个类类型时，它会继承类的成员但不包括其实现。 就好像接口声明了所有类中存在的成员，但并没有提供具体实现一样。

 接口同样会继承到类的private和protected成员。
 这意味着当你创建了一个接口继承了一个拥有私有或受保护的成员的类时，这个接口类型只能被这个类或其子类所实现（implement）。

 当你有一个庞大的继承结构时这很有用，但要指出的是你的代码只在子类拥有特定属性时起作用。 这个子类除了继承至基类外与基类没有任何关系。 例：
*/
class Control {
  private state: any = 'state1';
}

interface SelectableControl extends Control {
  select(): void;
}

// let x:SelectableControl = {select(): void {},state:1}
// ↑ TS2322: Type '{ select(): void; state: number; }' is not assignable to type 'SelectableControl'.
// Property 'state' is private in type 'SelectableControl' but not in type '{ select(): void; state: number; }'.

class Button extends Control implements SelectableControl {
  select() { }
}

class TextBox extends Control {
  select() { }
}


// 错误：“Image”类型缺少“state”属性。
class Image2 implements SelectableControl {
  //TS2420: Class 'Image2' incorrectly implements interface 'SelectableControl'.   
  // Property 'state' is missing in type 'Image2' but required in type 'SelectableControl'.
  select() { }
}

class Location2 {

}
