export = {};

/** 多继承
一个接口可以继承多个接口
 如果继承的多个接口之间有相同name的属性,类型必须相同否则报错*/
interface Animal {
  move(): void;
}

interface 有机物 {
  c: boolean;
  h: boolean;
  o: boolean;
}

interface Human extends Animal,有机物{
  name: string;
  age: number;
}

// let ahhh: Human = {name: 'ahhh', age: 123}; // TS2741: Property 'move' is missing in type '{ name: string; age: number; }' but required in type 'Human'.

// let ahhh: Human = {name: 'ahhh', age: 123, move(){console.log('我在dong')}}; // TS2739: Type '{ name: string; age: number; move(): void; }' is missing the following properties from type 'Human': c, h, o

// ahhh.move()

let ahhh: Human = {name: 'ahhh', age: 123, move(){console.log('我在dong')},c:true,h:true,o:true};



/** 串行继承
如果继承链上的多个接口之间存在相同name的属性,类型必须相同,否则报错*/
interface 有机物2 {
  c: boolean;
  h: boolean;
  o: boolean;
}

interface Animal2 extends 有机物2{
  move(): void;
}

interface Human2 extends Animal2{
  name: string;
  age: number;
}

let ahhh2: Human2 = {name: 'ahhh', age: 123, move(){console.log('我在dong')},c:true,h:true,o:true};
