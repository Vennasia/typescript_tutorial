interface propType {
  [key:string]: string
}
type dataType = {
  title: string
}
interface dataType1 {
  title: string
}
const data: dataType = {title: '订单页面'};
const data1: dataType1 = {title: '订单页面'};

let props: propType = data
let props2: propType = data1
//↑ TS2322: Type 'dataType1' is not assignable to type 'propType'.
//  Index signature is missing in type 'dataType1'

//这可能的原因是 interface dataType1 并不能满足 interface propType 的 [key:string]: string
//So 为嘛呢？
//   为什么type dataType 就可以呢?


//因为interface是可以继续被扩展的, 而type xxx是一旦定义就无法修改的
//
//https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#differences-between-type-aliases-and-interfaces
//the key distinction is that a type cannot be re-opened to add new properties vs an interface which is always extendable.
//
//我们可以在后面,  像 ↓ 这样对dataType1再进行扩展一下
/*interface dataType1 {
  age: number
}*/
//这样 dataType1 显然就不能赋给 propType 了 (除非你将 [key:string]: string ---> [key:string]: any)
//So 就是因为存在这种可能性，故我们ts不让这样赋值

export {}
