>https://www.typescriptlang.org/docs/handbook/triple-slash-directives.html
>
Triple-slash directives are single-line comments containing a single XML tag. The contents of the comment are used as compiler directives.

Triple-slash directives are only valid at the top of their containing file.

A triple-slash directive can only be preceded by single or multi-line comments, including other triple-slash directives.

If they are encountered following a statement or a declaration they are treated as regular single-line comments, and hold no special meaning.


```
/// <reference path="..." />
```

emmm。。。 就是把目录内容 复制粘贴到本文件 `/// <reference path="..." />` 所在位置

但如果 `/// <reference path="..." />` 之前有非注释语句, `/// <reference path="..." />` 就只会被当做普通的注释处理

---

> https://www.jianshu.com/p/e0912df68c3e
>
> 我们知道在 TS 项目中，文件之间相互引用是通过 import 来实现的，那么声明文件之间的引用呢？没错通过三斜线指令。

## `/// <reference types="react-scripts" />`

```ts
/// <reference types="react-scripts" />
```

声明文件，引用了一个第三方的声明文件包 react-scripts。问题来了， 我们去哪找它呢？没错也在 node_modules 里面找，reference types 的索引规则，完全不知道在哪去找， 经过的我实验，知道了它的索引规则为：
+ node_modules/@types/react-scripts/index.d.ts
+ node_modules/@types/react-scripts/package.json 的 types 字段
+ node_modules/react-scripts/index.d.ts
+ node_modules/react-scripts/package.json 的 types 字段
+ 接下里就是 NodeJs 查找模块的套路了

Create-React-App 的 react-scripts 包，就在 node_modules/react-scripts 里面，打开 react-scripts/package.json 的 types 字段指定的声明文件 index.d.ts 内容如下。

## `/// <reference path="./video" />`
上面是三斜线引用第三方声明文件的写法，我们见识到了 types 的用法，接下来我们见识下引用自己写声明文件的用法——path。
```ts
/// <reference path="./video" />
```

```ts
//video.d.ts
declare module "*.mp4"
```

```tsx
import React from 'react';
import macaque from "./macaque.mp4";
function App() {
  return <video controls width={400} autoPlay src={macaque}></video>;
};

export default App;
```

## `/// <reference lib="es2019.array" />`
使用 `lib=` 引用ts内置的类型
