export = {}
/** And the other is the as-syntax: */
let someValue: any = "this is a string";

console.log((someValue as string).split(''));




/** 双重断言, 会破坏原有类型*/
let ele = document.getElementById('#app');
// (ele as boolean)
//↑
// error TS2352: Conversion of type 'HTMLElement | null' to type 'boolean' may be a mistake because neither type sufficiently overlaps with the other. If this was intentional, convert the expression to 'unknown' first.
// ele as boolean //Type 'HTMLElement' is not comparable to type 'boolean'.
//↓
(ele as any) as boolean
