export = {}
/** type assertion operator*/
// 如果编译器不能够去除 null或 undefined，你可以使用类型断言(type assertion operator)手动去除。
// 语法是添加`!`后缀： `identifier!` 从 identifier的类型里去除了 null和 undefined：
/*
注意和js中的 `?.` 链判断预算符相区分, ?. 只是一个取值操作符, 如果给取的东西赋值的话一样会报错
*/


let ele:HTMLElement|null = document.getElementById('#app');
ele!.style!.color = 'red'

function broken(name: string | null): string {
  name = name || "Bob";
  return postfix("great");
  function postfix(epithet: string) {
    return name.charAt(0) + '.  the ' + epithet; // error, 'name' is possibly null
  }
}
/*↑
明明我们在调用 postfix 之前,name就通过||运算,给了一个底值'Bob',但这里依然报错了
emmm..ts是个笨蛋*/

function fixed(name: string | null): string {
  name = name || "Bob"; // 因为这里
  return postfix("great");
  function postfix(epithet: string) {
    return name!/*←告诉ts,这货绝壁不可能是null或undefined*/.charAt(0) + '.  the ' + epithet; // ok
  }
}


/** typescript 为什么无法推断出null已经被去掉了呢？
 本例使用了嵌套函数，因为编译器无法去除嵌套函数的null（除非是立即调用的函数表达式）。 因为它无法跟踪所有对嵌套函数的调用，尤其是你将内层函数做为外层函数的返回值。 如果无法知道函数在哪里被调用，就无法知道调用时 name的类型。

 The example uses a nested function here because the compiler can’t eliminate nulls inside a nested function (except immediately-invoked function expressions). That’s because it can’t track all calls to the nested function, especially if you return it from the outer function. Without knowing where the function is called, it can’t know what the type of name will be at the time the body executes.
 */


/** 注意和最新的es语法 ?. 区分*/
// aa?.bb //等价于 aa&aa.bb
// aa!.bb //!. 表示 aa一定不为空 是用户主观臆断  让ts放行
