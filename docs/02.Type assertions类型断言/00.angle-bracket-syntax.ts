export = {}
/**
有时候你会遇到这样的情况，你会比TypeScript更了解某个值的详细信息。 通常这会发生在你清楚地知道一个实体具有比它现有类型更确切的类型。

通过类型断言这种方式可以告诉编译器，“相信我，我知道自己在干什么”。 类型断言好比其它语言里的类型转换，但是不进行特殊的数据检查和解构。 它没有运行时的影响，只是在编译阶段起作用。 TypeScript会假设你，程序员，已经进行了必须的检查。

类型断言有两种形式。 其一是“尖括号”语法：
*/

let someValue: any = "this is a string";
// someValue.split(''); // ← 这样写是有隐患的,因为它并不一定是字符串
console.log(/*主观断言其一定是字符串并告诉typescript要相信我→*/(<string>someValue).split(''));
/*↑
主观上的保证 不代表客观上的正确
*/


/** 断言另外一种常用情景是 赋初始值*/
interface Shape {
    color: string;
}

interface PenStroke {
    penWidth: number;
}

interface Square extends Shape, PenStroke {
    sideLength: number;
}

let square = <Square>{};
square.color = "blue";
square.sideLength = 10;
square.penWidth = 5.0;

//但尖括号的写法 在jsx里使用可能会产生错误
