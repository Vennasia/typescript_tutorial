/*
  参数装饰器
    参数装饰器声明在一个参数声明之前
    访问装饰器会在调用时传入下列3个参数:
      1. 对于静态成员来说是类的构造函数，对于实例成员来说是类的原型对象
      2. 方法的名称
      3. 参数在函数参数列表中的索引

    注意:
       参数装饰器不能用在声明文件（.d.ts），重载或其它外部上下文（比如 declare的类）里。
*/

function logParams(params:any){
  return function (target: any, methodName: any, paramsIndex:any) {
    console.log('target:',target); //target: {getData: ƒ, constructor: ƒ}
    console.log('methodName:',methodName); //methodName: getData //← 如果没有methodName说明是构造函数
    console.log('paramsIndex:',paramsIndex); //paramsIndex: 1

    target.apiUrl = params;
  };
}

class HttpClient{
  public url: any | undefined;
  constructor(){

  }

  getData(x:number,@logParams('uuid') uid:any){
    console.log('我在getData里打印')
  }
}

const http:any = new HttpClient();
http.getData(123, 222);

console.log(http.apiUrl); //uuid

export let a = 1;
