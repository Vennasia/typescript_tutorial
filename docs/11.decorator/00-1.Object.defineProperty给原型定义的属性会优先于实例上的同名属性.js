function Clazz1(){
  this.a = 'aaaa';
}
toUpper(Clazz1.prototype, 'a');

let o = new Clazz1();
console.log(o.a); //'AAAA'

function toUpper(target/*此时target是类的原型*/, key) {
  let val = '';
  //给原形上添加属性
  Object.defineProperty(target, key, {
    get() {
      return val.toUpperCase();
    },
    //如果原型上有
    set(newVal) {
      val = newVal;
    }
  });
}

//正常情况下 是会先获取实例上的 再获取原型上的同名属性
//但如果使用了Object.defineProperty
//则会优先走通过Object.defineProperty给原型上添加的同名属性
