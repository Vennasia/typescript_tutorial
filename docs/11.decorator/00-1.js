"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Person = /** @class */ (function () {
    function Person() {
    }
    return Person;
}());
//@ts-ignore
Person.prototype.xxx = 123;
//如果要给每个类的原形上都添加这么一个属性呢?
//可以用一个函数
function addXxx(Person) {
    Person.prototype.xxx = 123;
}
//但这样不大好看
//So 我们就可以用语法糖把它编译一下咯
var Person2 = /** @class */ (function () {
    function Person2() {
        /** 3. 为实例属性添加装饰器*/
        this.name = 'ahhh';
    }
    __decorate([
        toUpper(true)
    ], Person2.prototype, "name", void 0);
    Person2 = __decorate([
        addSpeak
    ], Person2);
    return Person2;
}());
function addSpeak(target /*此时target是类*/) {
    /** 1. 为类添加原型方法*/
    target.prototype.speak = function () {
        console.log('say');
    };
    /** 2. 为类添加静态属性*/
    target.type = '人';
}
var person = new Person2();
//默认点不出来我们@addSpeak添加的
// person.
//
//So 只能在Person2里先声明 (注意我们这里严格来说声明的是一个实例方法 但并不影响我们点出来 (类里,只要不是抽象类,ts是区分不了原型方法和实例方法的))
person.speak();
/** 2. 为类添加静态属性*/
(function (Person2) {
})(Person2 || (Person2 = {}));
Person2.type;
/** 3. 为实例属性添加装饰器*/
function toUpper(isUpper) {
    return function (target /*此时target是类的原型*/, key) {
        var val = '';
        //给原形上添加属性
        Object.defineProperty(target, key, {
            get: function () {
                return isUpper ? val.toUpperCase() : val.toLowerCase();
            },
            //如果原型上有
            set: function (newVal) {
                val = newVal;
            }
        });
    };
}
console.log('person:', person);
console.log('person.name:', person.name); //AHHH
