export {};
/*
装饰器
tsconfig.json↓
"experimentalDecorators": true,
or
tsc --target ES5 --experimentalDecorators

装饰器模式

在尽可能不改变类(对象)结构的情况下 扩展其功能

装饰器是一种特殊类型的声明，它可以被附加到
1.类声明、
2.属性、
3.方法、
4.参数
5.或访问符上

装饰器的写法:
1. 普通装饰器(无法传参)
2. 装饰器工厂(可传参)

//TODO Metadata?
   http://www.typescriptlang.org/docs/handbook/decorators.html
   https://github.com/rbuckton/ReflectDecorators
*/
class Person{
  username:string;
  age:number;

  constructor(username:string,age:number){
    this.username = username;
    this.age = age;
  }

  say(){
    console.log('say');
  }
}

class Baby extends Person{
  wawa(){
    console.log('娃娃~~~~~~');
  }
}


class  Student extends Person{
  study(){
    console.log('study');
  }
}

class Teacher extends Person{
  study(){
    console.log('study');
  }
  teach(){
    console.log('teach');
  }
}

/*
这个时候就发现不好继承了
如果只继承最公有的那个 其它特性就要自己再都写一遍
*/
class SuperMan extends Person{
  study(){
    console.log('study');
  }
}

