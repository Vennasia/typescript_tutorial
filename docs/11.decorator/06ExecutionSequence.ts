/*
执行顺序
*/

function logClass1(params:string){
  console.log('装饰器工厂 logClass1')
  return function(target:any){
    console.log('类装饰器1');
  }
}

function logClass2(params:string){
  console.log('装饰器工厂 logClass2')
  return function(target:any){
    console.log('类装饰器2');
  }
}

function logAttribute(params?:string){
  console.log('装饰器工厂 logAttribute')
  return function(target:any,attrName:any){
    console.log('属性装饰器');
  }
}

function logMethod(params?:string){
  console.log('装饰器工厂 logMethod')
  return function(target:any,attrName:any){
    console.log('方法装饰器');
  }
}

function logParams1(params?:string){
  console.log('装饰器工厂 logParams1')
  return function(target:any,attrName:any,desc:any){
    console.log('方法参数装饰1');
  }
}

function logParams2(params?:string){
  console.log('装饰器工厂 logParams2')
  return function(target:any,attrName:any,desc:any){
    console.log('方法参数装饰2');
  }
}

@logClass1('xxx')
@logClass2('yyy')
class HttpClient{

  constructor(){

  }

  @logMethod()
  getData(){
    console.log('我在getData方法中打印')
  }

  setData(@logParams1() attr1:any,@logParams2() attr2:any){

  }

  @logAttribute()
  public apiUrl:string | undefined;
}

const http: any = new HttpClient();
// console.log('http.apiUrl:',http.apiUrl);


/*
装饰器工厂 logMethod
方法装饰器
装饰器工厂 logParams1
装饰器工厂 logParams2
方法参数装饰2
方法参数装饰1
装饰器工厂 logAttribute
属性装饰器
装饰器工厂 logClass1
装饰器工厂 logClass2
类装饰器2
类装饰器1

一句话总结
从里到外(非类装饰器，即类内部的装饰器先执行(方法参数装饰器会在方法装饰器执行后执行)，再执行类装饰器)
内部从上到下(在类内部 从上到下 哪个装饰器先出现就先执行 不论是哪种装饰器)
从后到前(对于同一个东西进行多次装饰的话，多个装饰器会从后到前执行)
*/
export let a = 1;
