"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/*
装饰器
tsconfig.json↓
"experimentalDecorators": true,
or
tsc --target ES5 --experimentalDecorators

装饰器模式

在尽可能不改变类(对象)结构的情况下 扩展其功能

装饰器是一种特殊类型的声明，它可以被附加到
1.类声明、
2.属性、
3.方法、
4.参数
5.或访问符上

装饰器的写法:
1. 普通装饰器(无法传参)
2. 装饰器工厂(可传参)

//TODO Metadata?
   http://www.typescriptlang.org/docs/handbook/decorators.html
   https://github.com/rbuckton/ReflectDecorators
*/
var Person = /** @class */ (function () {
    function Person(username, age) {
        this.username = username;
        this.age = age;
    }
    Person.prototype.say = function () {
        console.log('say');
    };
    return Person;
}());
var Baby = /** @class */ (function (_super) {
    __extends(Baby, _super);
    function Baby() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Baby.prototype.wawa = function () {
        console.log('娃娃~~~~~~');
    };
    return Baby;
}(Person));
var Student = /** @class */ (function (_super) {
    __extends(Student, _super);
    function Student() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Student.prototype.study = function () {
        console.log('study');
    };
    return Student;
}(Person));
var Teacher = /** @class */ (function (_super) {
    __extends(Teacher, _super);
    function Teacher() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Teacher.prototype.study = function () {
        console.log('study');
    };
    Teacher.prototype.teach = function () {
        console.log('teach');
    };
    return Teacher;
}(Person));
/*
这个时候就发现不好继承了
如果只继承最公有的那个 其它特性就要自己再都写一遍
*/
var SuperMan = /** @class */ (function (_super) {
    __extends(SuperMan, _super);
    function SuperMan() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SuperMan.prototype.study = function () {
        console.log('study');
    };
    return SuperMan;
}(Person));
