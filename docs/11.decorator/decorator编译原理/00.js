class Log {
  print(msg){
    console.log(msg)
  }
}

function decorate(target,property,decriptor){
  const oldValue = decriptor.value;
  decriptor.value = msg => {
    msg = `[${msg}]`;
    return oldValue.apply(null,[msg])
  };

  return decriptor;
}

const anotation = (target, property, decorate) => {
  const descriptor = decorate(Log.prototype, property, Object.getOwnPropertyDescriptor(Log.prototype, property));
  Object.defineProperty(Log.prototype, property, descriptor);
}
anotation(Log, 'print', decorate);

const log = new Log();
log.print('hello')
