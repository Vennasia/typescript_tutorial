interface IIndexService {
  log(str: string): void;
}

class IndexService implements IIndexService{
  log(str: string) {
    console.log("熊", str);
  }
}

class IndexController{
  private indexService: IndexService;
  constructor() {
    this.indexService = new IndexService();
  }
  info(){
    this.indexService.log('ahhh');
  }
}

//1. 傻瓜写法
const indexController = new IndexController();
indexController.info(); // ahhh

//todo 这种写法侵入性太强
