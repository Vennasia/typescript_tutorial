interface IIndexService {
  log(str: string): void;
}

class IndexService implements IIndexService{
  log(str: string) {
    console.log("熊", str);
  }
}


class IndexController{
  private indexService: IIndexService;
  constructor(indexService:IIndexService) {
    this.indexService = indexService;
  }
  info(){
    this.indexService.log('ahhh');
  }
}


// 无侵入式
const indexService = new IndexService();
const indexController = new IndexController(indexService);
indexController.info(); // ahhh
