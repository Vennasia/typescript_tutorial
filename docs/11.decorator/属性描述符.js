/*
对象可以设置属性，如果属性值是函数，那这个函数称为方法。
每一个属性和方法在定义的时候，都伴随三个属性描述符configurable、writable和enumerable
，分别用来描述这个属性的可配置性、可写性和可枚举性。
这三个描述符，需要使用 ES5 才有的 Object.defineProperty 方法来设置
，我们来看下如何使用：
*/

var obj = {};
Object.defineProperty(obj, "name", {
  value: "lison",
  writable: false,
  configurable: true,
  enumerable: true
});


/** writable*/
console.log(obj);
// { name: 'lison' }
obj.name = "test";
console.log(obj); // writable: false,
// { name: 'lison' }
Object.defineProperty(obj, "name", {
  writable: true
});
obj.name = "test";
console.log(obj);
// { name: 'test' }


/** enumerable*/
for (let key in obj) {
  console.log(key);
}
// 'name'
Object.defineProperty(obj, "name", {
  enumerable: false
});
for (let key in obj) {
  console.log(key);
}
// 什么都没打印


/** configurable*/
Object.defineProperty(obj, "name", {
  configurable: false
});
Object.defineProperty(obj, "name", {
  writable: false
});
// error Cannot redefine property: name
