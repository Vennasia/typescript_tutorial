export {};
/*
属性装饰器
  属性装饰器声明在一个属性声明之前
  访问装饰器会在调用时传入下列2个参数:
    1. 对于静态成员来说是类的构造函数，对于实例成员来说是类的原型对象
    2. 成员的名称

属性装饰器没法操作属性的属性描述符，它只能用来判断某各类中是否声明了某个名字的属性。

注意
  属性装饰器不能用在声明文件中（.d.ts），或者任何外部上下文（比如 declare的类）里。
*/


function logClass(params:string){
  return function(target:any){
    target.prototype.apiUrl = params;
  }
}

function logProperty(params:any){
  return function(target:any,attr:any,x:any){
    console.log('x:',x);
    console.log('target:',target);
    console.log('attr:',attr);
    target[attr] = params;
  }
}

@logClass('xxx')
class HttpClient{

  // @ts-ignore
  @logProperty('http://abc.com')
  public url: any | undefined;

  constructor(){

  }

  getData(){
    console.log('this.url:',this.url)
  }
}

var http = new HttpClient();
http.getData()
