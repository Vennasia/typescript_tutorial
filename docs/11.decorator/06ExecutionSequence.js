"use strict";
/*
执行顺序
*/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
function logClass1(params) {
    console.log('装饰器工厂 logClass1');
    return function (target) {
        console.log('类装饰器1');
    };
}
function logClass2(params) {
    console.log('装饰器工厂 logClass2');
    return function (target) {
        console.log('类装饰器2');
    };
}
function logAttribute(params) {
    console.log('装饰器工厂 logAttribute');
    return function (target, attrName) {
        console.log('属性装饰器');
    };
}
function logMethod(params) {
    console.log('装饰器工厂 logMethod');
    return function (target, attrName) {
        console.log('方法装饰器');
    };
}
function logParams1(params) {
    console.log('装饰器工厂 logParams1');
    return function (target, attrName, desc) {
        console.log('方法参数装饰1');
    };
}
function logParams2(params) {
    console.log('装饰器工厂 logParams2');
    return function (target, attrName, desc) {
        console.log('方法参数装饰2');
    };
}
var HttpClient = /** @class */ (function () {
    function HttpClient() {
    }
    HttpClient.prototype.getData = function () {
        console.log('我在getData方法中打印');
    };
    HttpClient.prototype.setData = function (attr1, attr2) {
    };
    __decorate([
        logMethod()
    ], HttpClient.prototype, "getData");
    __decorate([
        __param(0, logParams1()), __param(1, logParams2())
    ], HttpClient.prototype, "setData");
    __decorate([
        logAttribute()
    ], HttpClient.prototype, "apiUrl");
    HttpClient = __decorate([
        logClass1('xxx'),
        logClass2('yyy')
    ], HttpClient);
    return HttpClient;
}());
var http = new HttpClient();
// console.log('http.apiUrl:',http.apiUrl);
/*
装饰器工厂 logMethod
方法装饰器
装饰器工厂 logParams1
装饰器工厂 logParams2
方法参数装饰2
方法参数装饰1
装饰器工厂 logAttribute
属性装饰器
装饰器工厂 logClass1
装饰器工厂 logClass2
类装饰器2
类装饰器1

一句话总结
从里到外(非类装饰器，即类内部的装饰器先执行(方法参数装饰器会在方法装饰器执行后执行)，再执行类装饰器)
内部从上到下(在类内部 从上到下 哪个装饰器先出现就先执行 不论是哪种装饰器)
从后到前(对于同一个东西进行多次装饰的话，多个装饰器会从后到前执行)
*/
exports.a = 1;
