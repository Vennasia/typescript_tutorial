//数组一般概念是存储【一类】类型的东西, 但js比较灵活, 它的数组更类似于java、c#里的List
let arr1: number[] = [1, 2, 3];
//↑↓等价
//泛型的方式
let arr2: Array<number> = [1, 2, 3];
//联合类型↓
let arr3: Array<number | string> = [1, 2, 3, '4'];
let arr4: (number | string)[] = [1, 2, 3, '4'];

export {};
