let x: string[][];
// x = [1,2,3] //TS2322: Type 'number' is not assignable to type 'string[]'.
// x = ['a', 'b', 'c']; //TS2322: Type 'string' is not assignable to type 'string[]'.
// x = [[1, 2], ['a', 'b']]; //TS2322: Type 'number' is not assignable to type 'string'.
x = [['a', 'b'], ['c', 'd']]; //√
