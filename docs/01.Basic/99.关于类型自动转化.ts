export = {}
/** typescript里禁用了js的自动类型转化*/
let n = 123;
// n = n + ''/*n+''结果是一个字符串,它是不能赋值给number的*/; // TS2322: Type 'string' is not assignable to type 'number'.

let str = '123';
// str = str - 0; // TS2362: The left-hand side of an arithmetic operation must be of type 'any', 'number', 'bigint' or an enum type.

let a: number = 123;
let b: string = a.toString();

let c: string = '123';
let d: number = parseFloat(c);

let e: number = 2;
let f: boolean = Boolean(e);
console.log(f);

let obj = {name: 'ahhh', age: 11};
// let string = obj.toString()
// console.log(string) // [object Object]
let string = JSON.stringify(obj);
console.log(string); // {"name":"ahhh","age":11}
console.log(typeof string); // string

let string2 = `{"name":"ahhh","age":"11"}`;
let obj2 = JSON.parse(string2);
console.log(obj2); // { name: 'ahhh', age: '11' }
console.log(typeof obj2); // object
