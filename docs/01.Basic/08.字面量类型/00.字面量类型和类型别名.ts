// 基础类型的值也可以作为类型来使用
// let a:1 = 2; //TS2322: Type '2' is not assignable to type '1'.

// let direction:'up'|'down'|'left'|'right'
// direction = 'upxx'

//上面为一个变量声明类型 后面那一坨类型太长了。
//我们可以利用类型别名来帮我们解决这个问题
type Direction = 'up' | 'down' | 'left' | 'right';
let direction : Direction
let direction1 : Direction
direction = 'up123'


//类型别名 和 我们的变量名并不会冲突, 但它俩确实很容易弄混
//, 故我们一般将类型别名首字母大写
let Direction = 123;


let a: {a:1; b: string;} = 123; //TS2322: Type 'number' is not assignable to type '{ a: 1; b: string; }'.

export {};
