// ↓表示是一个独立的作用域 防止模块间的干扰, 比如ts全局作用域下 name这个声明已经被占用了
export = {};
/*
https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Data_structures
js:
1.null
2.undefined
3.boolean
4.string
5.number
6.object
7.symbol
8.BigInt
9.function （一般不算作数据类型）


ts:
10. void
11. never
12. unknown
13. 元组tuple(固定长度固定类型的数组
14. any
*/

/** 1.js中的七种基本类型（数组不算在对象里就是8种） */
/** 其中string、number、boolean ctrl+左键点击 可以跳转 发现它们其实是一个interface*/
let a: null = null;
let b: undefined = undefined;
let c: boolean = true;
let d: string = 'string';
let e: number = 1;
let obj: object = {}; //除基本类型以外的类型都可以
let s: symbol = Symbol('just a symbol'); //独一无二
let s2: symbol = Symbol('just a symbol');

//symbol
// console.log('s == s2:',s == s2); //s == s2: false
// let obj2 = {[s]:'xxx'}
// obj2[s] //TS2538: Type 'symbol' cannot be used as an index type. //但js里是可以的

//bigint
console.log('Number.MAX_SAFE_INTEGER + 1:',Number.MAX_SAFE_INTEGER + 1); //9007199254740992
console.log('Number.MAX_SAFE_INTEGER + 2:',Number.MAX_SAFE_INTEGER + 2); //9007199254740992
console.log('Number.MAX_SAFE_INTEGER + 1 == Number.MAX_SAFE_INTEGER + 2:',Number.MAX_SAFE_INTEGER + 1 == Number.MAX_SAFE_INTEGER + 2); //true
console.log('BigInt(Number.MAX_SAFE_INTEGER) + BigInt(1):',BigInt(Number.MAX_SAFE_INTEGER) + BigInt(1)); //9007199254740992n
console.log('BigInt(Number.MAX_SAFE_INTEGER) + BigInt(2):',BigInt(Number.MAX_SAFE_INTEGER) + BigInt(2)); //9007199254740993n
console.log('BigInt(Number.MAX_SAFE_INTEGER) + BigInt(1) == BigInt(Number.MAX_SAFE_INTEGER) + BigInt(2):',BigInt(Number.MAX_SAFE_INTEGER) + BigInt(1) == BigInt(Number.MAX_SAFE_INTEGER) + BigInt(2)); //false

//object
function create(target: object){}
create({});
create([]);
create(function(){});

//字面量类型
type Direction = 'up' | 'down' | 'left' | 'right';
let direction : Direction
// direction = 'up' //√
// direction = 'up1' //TS2322: Type '"up1"' is not assignable to type 'Direction'.

type RecordSet = Record<'up' | 'down' | 'left' | 'right',any>
/*
type RecordSet = {
  up: any;
  down: any;
  left: any;
  right: any;
}
*/
type RecordKeyType = keyof any //type RecordKeyType = string | number | symbol
// 'up' | 'down' | 'left' | 'right' 这种字符串字面量的联合类型 是属于 字符串类型的

// type XX = ('up' | 'down' | 'left' |'a') extends  'up' | 'down' | 'left'?'cc':'ll'


/** any类型: 一旦写了any之后, 任何的校验都会失效
 * , 如果不给类型默认就是any类型 */
// 一旦类型定了就不能改,除非之前它是一个any类型
let x: any = 1;
x = 'abc';

/** 枚举类型*/
// 枚举类型常常用于防止你手写字符串时出错(两次书写不一致)
enum Gender{
  Man,
  Woman
}

console.log('Gender.Man:',Gender.Man); //0
console.log('Gender[0]:',Gender[0]) //Man


/** unknown*/
//unknown类型是TypeScript在3.0版本新增的类型，它表示未知的类型，这样看来它貌似和any很像，但是还是有区别的，也就是所谓的“unknown相对于any是安全的”。怎么理解呢？我们知道当一个值我们不能确定它的类型的时候，可以指定它是any类型；但是当指定了any类型之后，这个值基本上是“废”了，你可以随意对它进行属性方法的访问，不管有的还是没有的，可以把它当做任意类型的值来使用，这往往会产生问题，如下：
let value: any
console.log(value.name)
console.log(value.toFixed())
console.log(value.length)
/*↑
上面这些语句都不会报错，因为value是any类型，所以后面三个操作都有合法的情况，当value是一个对象时，访问name属性是没问题的；当value是数值类型的时候，调用它的toFixed方法没问题；当value是字符串或数组时获取它的length属性是没问题的。

而当你指定值为unknown类型的时候，如果没有通过基于控制流的类型断言来缩小范围的话，是不能对它进行任何操作的，关于类型断言，我们后面小节会讲到。总之这里你知道了，unknown类型的值不是可以随便操作的。

我们这里只是先来了解unknown和any的区别，unknown还有很多复杂的规则，但是涉及到很多后面才学到的知识，所以需要我们学习了高级类型之后才能再讲解。
*/

