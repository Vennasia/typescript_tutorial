//声明一个void类型的变量没有什么大用,因为你只能为它赋予undefined和null(严格模式下只能赋undefined)：
let unusable: void = undefined;

//void 和 any 相反, any 是表示任意类型,
// 而 void 是表示没有任意类型,就是什么类型都不是,这在我们定义函数,函数没有返回值时会用到
// (即它是TS中默认的函数返回值类型)：
function print(x: any): void {
  console.log(x)
}
// let a1: number = print(1); //TS2322: Type 'void' is not assignable to type 'number'.
let a1 = print(1);
console.log(a1); // undefined // 即使 strictNullChecks为true 也不会报错

//undefined可以赋给void, void不能赋给undefined
function sum(): void{
  // return void 1;
  return undefined;
}
//↓ TS2355: A function whose declared type is neither 'void' nor 'any' must return a value.
function sum2(): undefined {

} // ← So这也是为什么我们函数默认返回值为void的原因, 省事


/*
在js中
void是一种操作符，可以让任何表达式返回undefined

js中undefined不是一个保留字，我们甚至可以自定义一个undefined变量去覆盖全局的
function(){
  var undefined = 0;
  console.log(undefined); //0
}()

void能确保返回的值一定是undefined

<<< void 0
>>> undefined
*/
console.log(void 0); // undefined
