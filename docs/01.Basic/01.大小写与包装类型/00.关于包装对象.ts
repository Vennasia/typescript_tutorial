export = {};

// JavaScript 的类型分为两种：原始数据类型（Primitive data types）和对象类型（Object types）。
// 当调用基本数据类型方法的时候，JavaScript 会在原始数据类型和对象类型之间做一个迅速的强制性切换(包装)
let num:number = 123;
num.toFixed(); //← 这是一个基本类型(primitive) 但是却可以调用对象上的方法 这是因为内部进行了一个包装 (new Number(num))


let isOK: boolean = true; // 编译通过
let isOK1: boolean = Boolean(1) // 编译通过
let isOK2: boolean = new Boolean(1); // 编译失败   期望的 isOK 是一个原始数据类型
/*↑
TS2322: Type 'Boolean' is not assignable to type 'boolean'.   
'boolean' is a primitive, but 'Boolean' is a wrapper object.
Prefer using 'boolean' when possible.*/
let isOK3: Boolean = new Boolean(1);



let s1:string = "abc";
/*
TS2322: Type 'String' is not assignable to type 'string'.   'string' is a primitive, but 'String' is a wrapper object. Prefer using 'string' when possible.
↓ * */
let s2:string = new String("abc");
//我们标识类型的时候 基本类型全部用小写的, 如果描述实例则用类的类型(类可以描述实例: 类类型)
let s3: String = new String("abc");
class Dog {}
let dog: Dog = new Dog();

//也就是说我们用String这样的类类型来描述时 会扩大范围 可能不是那么安全
let s4: string = "abc";
let s5: String = "abc"; //字符串也是String的一个实例
let s6: String = new String("abc"); //字符串也是String的一个实例
