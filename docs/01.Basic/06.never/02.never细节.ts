/** never 类型是任何类型的子类型，所以它可以赋值给任何类型；*/
let endless: () => never = ()=>{
  while (true){}
};
let a: never = endless();
let b: number = a;


/** 而没有类型是 never 的子类型，所以除了它自身没有任何类型可以赋值给 never 类型，
 *  any 类型也不能赋值给 never 类型。*/
let neverVariable = (() => {
  while (true) {}
})();
// neverVariable = 123; // error 不能将类型"number"分配给类型"never"
/* ↑
上面例子我们定义了一个立即执行函数，也就是"let neverVariable = "右边的内容。右边的函数体内是一个死循环，所以这个函数调用后的返回值类型为 never，所以赋值之后 neverVariable 的类型是 never 类型，当我们给 neverVariable 赋值 123 时，就会报错，因为除它自身外任何类型都不能赋值给 never 类型。
*/


/** never 和 void 的区别*/
// void 可以被赋值为 null 和 undefined的类型。 never 则是一个不包含值的类型。
// 拥有 void 返回值类型的函数能正常运行。拥有 never 返回值类型的函数无法正常返回，无法终止，或会抛出异常。


/** 关于交叉类型 没有交集就是never*/
interface P{
  name:string
}
interface P2{
  name:number
}

type P3 = P & P2;
let x:P3 = {name:123}  //TS2322: Type 'number' is not assignable to type 'never'.


/** 联合类型中 never不会被并入*/
type xx = never|string|number //type xx = string | number
