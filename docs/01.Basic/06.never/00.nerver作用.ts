/*
在 TS 中， null 和 undefined 是任何类型的有效值，所以无法正确地检测它们是否被错误地使用。
于是 TS 引入了 --strictNullChecks 这一种检查模式
由于引入了 --strictNullChecks ，在这一模式下，null 和 undefined 能被检测到。

所以 TS 需要一种新的底部类型（ bottom type ）。
所以就引入了 never。
*/

// Compiled with --strictNullChecks
function fn(x: number | string) {
  if (typeof x === 'number') {
    // x: number 类型
  } else if (typeof x === 'string') {
    // x: string 类型
  } else {
    // x: never 类型
    // --strictNullChecks 模式下，这里的代码将不会被执行，x 无法被观察
  }
}



/** never: 永远无法到达的 */
// never(never不像null和undefined,它只是个类型（编译后撒也不是）,而null和undefined即是类型又是值,并且undefined这个值还可以被更改)
/*
  never 类型 表示永远不会有返回值(不 会 返 回 值 即使是null或则undefined)
  1. throw,即使你捕获了,包裹throw的函数也不会有返回值
  2. 死循环(比如启动了一个后台运行的...)
  3. 类型判断时校验完整性(?) 参看 01.Basic/06.never/01.使用never对代码完整型进行保护.ts
*/

/** throw*/
let error: () => never = ()=>{
  throw new Error('error');

  //↓ TS2322: Type '() => number' is not assignable to type '() => never'.   Type 'number' is not assignable to type 'never'.
  // return 123;
};

//↓ TS2322: Type '() => void' is not assignable to type '() => never'.   Type 'void' is not assignable to type 'never'.
let error2: () => never = () => {

};

try{
  console.log('error():',error());
}catch (e) {
  console.log('e:',e);
}


/** 死循环*/
let endless: () => never = ()=>{
  while (true){}
};



export {};
