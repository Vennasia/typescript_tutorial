export {};

/** 使用never对代码完整型进行保护*/
//圆
interface ICircle {
  kind: 'circle',//← 字面量类型
  r: number
}

//长方形
interface IRect {
  kind: 'rect',
  width: number,
  height: number
}

interface IRect2 {
  kind: 'rect2',
  width: number,
  height: number
}


//方形
interface ISquare {
  kind: 'square',
  width: number
}

const assert = (obj: never) => {throw new Error('永远不会执行')}

const getArea = (obj:ICircle|IRect|IRect2|ISquare) => {
  switch (obj.kind) {
    case 'rect':
      return obj.width * obj.height
    default:
      return assert(obj)
    //↑TS2345: Argument of type 'ICircle | IRect2 | ISquare' is not assignable to parameter of type 'never'.   Type 'ICircle' is not assignable to type 'never'.
    //这里never就起到了帮我们校验代码完整性的作用
    //只有我们将所有可能的分支都完成后, 才不会走到default, 才不会让obj有值, 这里ts never才不会报错
  }
}

getArea({kind:'rect2',width:100,height:100})
//↑此时 虽然ts不报错了, 但其实运行起来是有问题的, 我们getArea里根本没有完成 rect2的分支
