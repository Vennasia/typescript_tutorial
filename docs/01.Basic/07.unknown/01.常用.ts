export = {};

function sum(a: number, b: unknown) {
  if(typeof b === 'string'){
    return a + b;
  }

  if(typeof b === 'number'){
    return a + b;
  }

  return b as boolean;
}
