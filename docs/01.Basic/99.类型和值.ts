export let a = 1;

/*
类既可以作为类型使用，也可以作为值使用，接口只能作为类型使用

关键字	作为类型使用	作为值使用
class	yes	yes
enum	yes	yes
interface	yes	no
type	yes	no
function	no	yes
var,let,const	no	yes
*/

class Person{
  name:string=''
}
let p1:Person;//作为类型使用
let p2 = new Person();//作为值使用

//--- --- ---

interface Animal{
  // name:string = 'str' //←报错 接口类型不能用作值
  name:string
}

let a1:Animal;

//let a2 = Animal;// 接口类型不能用作值 TS2693: 'Animal' only refers to a type, but is being used as a value here.
//>referes to:指的是
