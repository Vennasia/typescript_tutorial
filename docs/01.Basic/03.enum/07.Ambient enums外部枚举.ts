/*
外部枚举和非外部枚举之间有一个重要的区别，在正常的枚举里，没有初始化方法的成员被当成常数成员。

对于外部枚举(非常数的)而言，没有初始化方法时被当做需要经过计算的。

One important difference between ambient and non-ambient enums is that, in regular enums, members that don’t have an initializer will be considered constant if its preceding enum member is considered constant. In contrast, an ambient (and non-const) enum member that does not have initializer is always considered computed.
*/

declare enum Enum {
  A = 1,
  B,
  C = 2
}
/* ↑
  意思是B不会自动被赋予一个根据前面数字成员来定的自增长值
  它不会被自动赋予值
  这意味着 如果这个B不是外部已经赋过值的一个变量引用
  那么它在运行时会报错
*/
