export = {};
/** 常量枚举tutorial1 */
/*
  我们定义了枚举值之后，编译成 JavaScript 的代码会创建一个对应的对象，这个对象我们可以在程序运行的时候使用。
  但是如果我们使用枚举只是为了让程序可读性好，并不需要编译后的对象呢？这样会增加一些编译后的代码量。

  所以 TypeScript 在 1.4 新增 const enum*(完全嵌入的枚举)*，在之前讲的定义枚举的语句之前加上const关键字，这样编译后的代码不会创建这个对象，只是会从枚举里拿到相应的值进行替换，来看我们下面的定义：
*/
enum Status {
  Off,
  On
}
const enum Animal {
  Dog,
  Cat
}
const status = Status.On;
const animal = Animal.Dog;

// 上面的例子编译成 JavaScript 之后是这样的：
// var Status;
// (function(Status) {
//   Status[(Status["Off"] = 0)] = "Off";
//   Status[(Status["On"] = 1)] = "On";
// })(Status || (Status = {}));
// var status = Status.On;
// var animal = 0; /* Dog */

/* ↑
我们来看下 Status 的处理，先是定义一个变量 Status，然后定义一个立即执行函数，在函数内给 Status 添加对应属性，首先Status[“Off”] = 0是给Status对象设置Off属性，并且值设为 0，这个赋值表达式的返回值是等号右边的值，也就是 0，所以Status[Status[“Off”] = 0] = "Off"相当于Status[0] = “Off”。创建了这个对象之后，将 Status 的 On 属性值赋值给 status；


再来看下 animal 的处理，我们看到编译后的代码并没有像Status创建一个Animal对象，而是直接把Animal.Dog的值0替换到了const animal = Animal.Dog表达式的Animal.Dog位置，这就是const enum的用法了。
* */


/** 常量枚举tutorial2 */
//用const声明的枚举,常量枚举的特性是在编译后会被移除(编译后不存在任何代码),非常量枚举(即普通枚举)都存在编译后代码

const enum Month{
  Jan
  ,Feb
  ,Mar
}

let month = [Month.Jan, Month.Feb, Month.Mar]; // 会直接编译为 month=[0/*Jan*/,1/*Feb*/,2/*Mar*/]
// console.log(Month); //TS2475: 'const' enums can only be used in property or index access expressions or the right hand side of an import declaration or export assignment or type query. // 常量枚举不能直接打印他们的集
// console.log(Month[0]); // 也不能用索引打印 当然咯 它们的集都没有 更别说Month[0]了
