枚举最终会编译为一个对象 而不是空气


```tsx
enum Role{
  // Reporter = 1 //可以进行自定义赋值，后面的值会以此为基础递增
  Reporter
  ,Developer
  ,Maintainer
  ,Owner
  ,Guest
}

//↑ 会编译为 ↓

var Role;
  (function (Role) {
      // Reporter = 1 //可以进行自定义赋值，后面的值会以此为基础递增
      Role[Role["Reporter"] = 0] = "Reporter";
      Role[Role["Developer"] = 1] = "Developer";
      Role[Role["Maintainer"] = 2] = "Maintainer";
      Role[Role["Owner"] = 3] = "Owner";
      Role[Role["Guest"] = 4] = "Guest";
  })(Role || (Role = {}));
```

如果不想生成这么一个对象呢（用不到浪费空间）

可以给枚举加上 `const` , 变成常量枚举, 查看 `01.Basic/99.enum/05.常量枚举.ts`

