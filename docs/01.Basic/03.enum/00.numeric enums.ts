export = {};
/** 1.数字枚举*/
// enum Direction {
//   Up = 1,
//   Down,
//   Left,
//   Right
// }
/* ↑
如上，我们定义了一个数字枚举， Up使用初始化为 1。
其余的成员会从 1开始自动增长。 换句话说， Direction.Up的值为 1， Down为 2， Left为 3， Right为 4。
*/

//我们还可以完全不使用初始化器：
enum Direction {
  Up,
  Down,
  Left,
  Right,
}
/* ↑
现在, Up的值为 0, Down的值为 1等等。 当我们不在乎成员的值的时候,这种自增长的行为是很有用处的,但是要注意每个枚举成员的值都是不同的。
*/
console.log('Direction:',Direction);
/*
{
  '0': 'Up',
  '1': 'Down',
  '2': 'Left',
  '3': 'Right',
  Up: 0,
  Down: 1,
  Left: 2,
  Right: 3
}
*/

//数字枚举在定义值的时候，可以使用计算值和常量(constant enum member,而不是 const x = 1这种)。
//但是要注意，如果某个字段使用了计算值，那么该字段后面紧接着的字段必须设置初始值，这里不能使用默认的递增值了，来看例子：
const getValue = () => {
  return 0;
};
// enum ErrorIndex {
//   a = getValue(),
//   b, // error 枚举成员必须具有初始化的值
//   c
// }
enum RightIndex {
  a = getValue(),
  b = 1, // 紧接着赋初始值 这样就不会报错
  c
}
const Start = 1;
/* ↑
注意虽然文档中说 enums without initializers either need to be first, or have to come after numeric enums initialized with numeric constants or other constant enum members 满足这样条件的 才能不设置初始值 并且根据前一个的值+1来设置

但这里的constant enum members 的const是指constant的枚举成员 而不是 普通的const

故这里也会报错
*/
// enum Index {
//   a = Start, // 但如果改成 RightIndex.b 就不会报错
//   b, // error 枚举成员必须具有初始化的值
//   c
// }

/** 1.1反向映射*/
//我们定义一个枚举值的时候，可以通过 Enum[‘key’]或者 Enum.key 的形式获取到对应的值 value。
//TypeScript 还支持反向映射，但是反向映射只支持数字枚举，我们后面要讲的字符串枚举是不支持的。
// 来看下反向映射的例子：

enum Status {
  Success = 200,
  NotFound = 404,
  Error = 500
}
console.log(Status["Success"]); // 200
console.log(Status[200]); // 'Success'
console.log(Status[Status["Success"]]); // 'Success'


enum Role{
  // Reporter = 1 //可以进行自定义赋值，后面的值会以此为基础递增
  Reporter
  ,Developer
  ,Maintainer
  ,Owner
  ,Guest
}
console.log(Role);
/*
0: "Reporter"
1: "Developer"
2: "Maintainer"
3: "Owner"
4: "Guest"
Developer: 1
Guest: 4
Maintainer: 2
Owner: 3
Reporter: 0
*/

