/** 2. 字符串枚举*/
// enum Message {
//   Error = "Sorry, error",
//   Success = "Hoho, success"
// }
// console.log(Message.Error); // 'Sorry, error'

//再来看我们使用枚举值中其他枚举成员的例子：
enum Message {
  Error = "error message",
  ServerError = Error,
  ClientError = Error
}
console.log(Message.Error); // 'error message'
console.log(Message.ServerError); // 'error message'
/*↑
注意，这里的其他枚举成员指的是同一个枚举值中的枚举成员，因为字符串枚举不能使用常量或者计算值，所以也不能使用其他枚举值中的成员(Other.xx)。
* */

//字符串枚举是不支持反向映射
