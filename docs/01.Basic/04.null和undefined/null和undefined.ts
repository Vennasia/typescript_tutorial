//js中 null 和 undefined 两等号下是相等的 三等号下是不相等的

let n1; //TS7043: Variable 'n1' implicitly has an 'any' type, but a better type may be inferred from usage.

let n2: undefined = 123;
let n3: undefined = undefined;

/*
TS2322: Type 'null' is not assignable to type 'undefined'.

但如果tsconfig.json中如下设置 则可以互相赋值
"compilerOptions": {
  //When type checking, take into account `null` and `undefined`.
  "strictNullChecks": false
}
↓ */
let n4: undefined = null;
let n5: null = undefined;



//非严格模式下null和undefined是所有类型的子类型。
let a:number = null;
let b:number = undefined;

export {};
