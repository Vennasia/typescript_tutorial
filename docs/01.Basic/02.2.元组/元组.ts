// 元组可以看做是数组的拓展,它表示已知元素数量和类型的数组。确切地说,是已知数组中每一个位置上的元素的类型
let tuple: [number, string] = [0, '1'];
//数组的方法依旧可以用 但如果导致越界 越界的不能用
tuple.push(2);
console.log(tuple); // [0, "1", 2]
// console.log(tuple[2]); // TS2493: Tuple type '[number, string]' of length '2' has no element at index '2'.
/*↑
+ 在 2.6 及之前版本中，超出规定个数的元素称作越界元素，但是只要越界元素的类型是定义的类型中的一种即可。比如我们定义的类型有两种：string 和 number，越界的元素是 string 类型，属于联合类型 string | number，所以没问题，联合类型的概念我们后面会讲到。
+ 在 2.6 之后的版本，去掉了这个越界元素是联合类型的子类型即可的条件，要求元组赋值必须类型和个数都对应。

在 2.6 之后的版本，[string, number]元组类型的声明效果上可以看做等同于下面的声明：
*/
/*
interface Tuple extends Array<number | string> {
  0: string;
  1: number;
  length: 2;
}
*/

//↓ 使用越界 会报错
console.log(tuple[2]); //TS2493: Tuple type '[number, string]' of length '2' has no element at index '2'.

export {};
