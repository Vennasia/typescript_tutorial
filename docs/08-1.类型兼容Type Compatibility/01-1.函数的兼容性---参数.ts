export = {}

//赋值的函数的参数要小于等于被赋值的函数(和接口、对象 的情况正好相反
//即参数是少的赋给多的 (核心宗旨是安全 是安全 是安全, 因为参数多个那个能处理更多的参数类型)
let sum1 = (a:string, b:string)=>{};
let sum2 = (a:string)=>{};

sum1 = sum2;

//应该从安全性方面来记忆
//比如下面的forEach函数
//它接受的函数的参数 可以接受两个, 也可以接受1个
// [1,2,3].forEach((item,index)=>{
//
// })

type ForEachFn<T> = (item: T, index: number) => void
function forEach<T>(arr: T[], cb: ForEachFn<T>) {
  for (let i = 0; i < arr.length; i++) {
    cb(arr[i], i);
  }
}
forEach<number>([1,2,3,4,5],(item)=>{

})


