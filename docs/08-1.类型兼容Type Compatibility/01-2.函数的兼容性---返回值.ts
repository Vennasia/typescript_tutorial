export = {}

// 少的可以赋给多的
// type sum1 = () => string | number
// type sum2 = () => string
//
// let s1!:sum1;
// let s2!:sum2;
//
// // s1 = s2 0//√
// s2 = s1 //×


// 多的可以赋给少的
type sum1 = () => { name:string, age: number; }
type sum2 = () => {name:string}

let s1!:sum1;
let s2!:sum2;

// s1 = s2 //×
s2 = s1 //√
