export = {}

// 函数作为另一个函数的参数传递时, 这个函数接受的实参的类型 可以比 形参对应的类型 多（反正多余的类型并不会走
function getFn2(cb: (person: string | number) => number|string){

  //我们只会给cb传递string和number两种情况
  //故只要我们的cb函数实际传递的时候 情况比这多就可以处理 （实际能处理的比我们这里定义时模拟的情况还多 不就更安全了嘛）
  cb(2)
  cb('2')
  //cb(true) //TS2345: Argument of type 'true' is not assignable to parameter of type 'string | number'.
}

// ↓实际接受的参数更多是因为, 我们定义时可能接受的情况包含两种string|number, 故我们实际实现时至少要考虑这俩种情况这样才安全
getFn2((person: string | number | boolean/*person*/) => 'xx');
