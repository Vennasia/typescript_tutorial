export = {}

class Person1 {
  readonly/*readonly和protected可共存*/ name:string = 'ahhh'

  protected age = 1
}

class Person2 {
  /*protected*//*← 打开就不兼容了*/ name:string = 'ahhh1'
}

let p1!:Person1
let p2!:Person2

p2 = p1; //√
// p1 = p2; //√

//比较的是实例长啥样, 同字面量对象, 只能多不能少

//如果类中出现了private protected 永远不兼容 (同名的属性的话)
