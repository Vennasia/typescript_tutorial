export = {};

/** 接口多的可以赋给少的*/

interface IFruit {
  color: string
}

interface IVeg {
  color: string,
  taste: string
}

let fruit: IFruit = {color: 'red'};
let veg: IVeg = {color: 'red', taste: '酸的'};

// 这里我们要思考 被赋值的接口 是否全部满足, 如果满足即可赋值, 但是非法属性不能被调用
fruit = veg;
// fruit.taste //TS2339: Property 'taste' does not exist on type 'IFruit'.
