export = {}

// 泛型 感觉最终结果来确定是否兼容, 返回的结果一样就兼容
interface A<T>{
  [key:number]:T
}
interface B<T>{
  [key:number]:T
}

//↓不兼容
// type A1 = A<string>;
// type B1 = A<number>;

//↓兼容
type A1 = A<string>;
type B1 = A<string>;


let a1!:A1;
let a2!:B1;
a1=a2
