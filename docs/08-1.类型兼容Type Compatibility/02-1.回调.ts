export = {}

/** case1*/
/*function fn(callback: (val?: string) => any) {
  callback('abc'); //我们里面一定会传 但外面的val确是 `？` 故不安全
}

fn((val: string) => {

});*/



/** case2*/
class Parent {
  money!: string
}

class Son extends Parent {
  car!: string
}

class GrandSon extends Son {
  play!: string
}

let son = new Son();
let parent = new Parent();
let grandson = new GrandSon();
function fn(callback:(val:Son)=>Son){
  callback(son); //这里的callback最多给你传的是一个son， 故回调要求的范围不能比Son大
}
// √
// fn((val: Son) => new Son());
// √
// fn((val: Parent) => new Son());
// ×
fn((val: GrandSon) => new Son()); //因为我们这里要求至少传的是个GrandSon ---> 即至少要有 money、car、play 三个属性, 而我们fn里调用回调时至多传的是一个son(只有money+car), 故不行
