let base1!: string | number;
let base2!: string | number | boolean;

//在基本类型中范围大的不能赋给范围小的
base1 = base2; //TS2322: Type 'string | number | boolean' is not assignable to type 'string | number'.   Type 'boolean' is not assignable to type 'string | number'.

//√
base2 = base1; //TS2322: Type 'string | number | boolean' is not assignable to type 'string | number'.   Type 'boolean' is not assignable to type 'string | number'.

export {}
