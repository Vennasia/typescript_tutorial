// 四则运算
type Operator = (x: number, y: number) => number;

const add: Operator = (x, y) => x + y;
const minus: Operator = (x, y) => x - y;
const multiply: Operator = (x, y) => x * y;
const divide: Operator = (x, y) => x / y;
const mod: Operator = (x, y) => x % y;

/*
注意不要混淆了 TypeScript 中的 => 和 ES6 中的 =>。
在 TypeScript 的类型定义中，=> 用来表示函数的定义，左边是输入类型，需要用括号括起来，右边是输出类型。
在 ES6 中，=> 叫做箭头函数。

上例中我们 type Operator 时 这里的`=>` 就是typescript里的`=>`

而add、minus... 这些方法中的 `=>` 是es6里的箭头函数的箭头
*/

declare const $: (selector: string) => { // ←注意这不是箭头函数 这就是一个ts里的函数声明(表达式的方式)
  click(x: () => void): void;
  width(length: number): void;
};

export = {}
