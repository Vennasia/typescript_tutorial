export = {};

// 想对属性做一些扩展 做一些数据劫持

// 早期是没有defineProperty的, 那是怎么做的呢?

const school = {
  name: 'ahhh',
  age: 15
}

type IProxy<T> = {
  get(): T;
  set(value: any): boolean;
}

type IProxify<T> = {
  [K in keyof T]: IProxy<T[K]>
};

//为每一个属性都增加一个get和set ===> 装包
function proxify<T>(obj: T): IProxify<T> {
  let r = {} as IProxify<T>;
  for (let key in obj) {
    let value = obj[key];
    r[key] = {
      get() {
        return value;
      }
      , set(newValue) {
        return value = newValue;
      }
    }
  }

  return r;
}

let r = proxify(school);
r.name.set(123)
/** ↑ 装包*/

/** ↓ 拆包*/
function unProxify<T>(obj: IProxify<T>):T{
  let r = {} as T;
  for (let key in obj) {
    let value = obj[key];
    r[key] = value.get();
  }
  return r;
}
let r1 = unProxify(r);
r1.name
r1.age
