export = {}
import 'reflect-metadata';

const user = {
  name: 'ahhh'
}

//在user这个对象上定义一些元数据
Reflect.defineMetadata('key1','juti de zhi',user)

console.log(user);
//{ name: 'ahhh' } //元数据默认是看不到的

console.log(Reflect.getMetadata('key1', user)); //具体的值

@Reflect.metadata('class-meta','test')
class User {
  @Reflect.metadata('attr-meta','test2')
  name = 'ahhh!'

  @Reflect.metadata('attr-meta2','test3')
  getName(){}
}

class Teacher extends User{}

console.log(Reflect.getMetadata('class-meta', User)); //test
console.log(Reflect.getMetadata('attr-meta', User.prototype, 'name')); //test2
console.log(Reflect.getMetadata('attr-meta2', User.prototype, 'getName')); //test3

console.log(Reflect.hasMetadata('class-meta', User)); //true
console.log(Reflect.hasMetadata('class-meta', Teacher)); //true
console.log(Reflect.hasOwnMetadata('class-meta', Teacher)); //false

console.log(Reflect.getMetadataKeys(User.prototype)); //[]
console.log(Reflect.getMetadataKeys(User.prototype,'name')); //[ 'attr-meta' ]
console.log(Reflect.getMetadataKeys(User.prototype,'getName')); //[ 'attr-meta2' ]
console.log(Reflect.getMetadataKeys(Teacher.prototype,'getName')); //[ 'attr-meta2' ]
console.log(Reflect.getOwnMetadataKeys(Teacher.prototype,'getName')); //[]

//delete metadata from an object or property
// Reflect.deleteMetadata(metadataKey,target)
// Reflect.deleteMetadata(metadataKey,target,propertyKey)
