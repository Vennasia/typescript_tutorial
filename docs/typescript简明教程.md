![](readme-assets/000.png)

![](readme-assets/001.png)

> 对于非基本类型使用`|`, 和之前一样, 只需牢记一点, `|`表示是要么A要么B
>
> ![](readme-assets/002.png)


> 对于非基本类型使用`&`, 和之前一样, 只需牢记一点, `&`表示是既要满足A又要满足B
>
> 比如下面就不可能
>
> ![](readme-assets/003.png)
>
> ![](readme-assets/004.png)

> 相比较于type, interface 只能用来描述对象和函数, 不能用来描述基础类型、联合类型(比如`type x = I1|I2` 这种), 但interface是可以继承的、而且还可以使用泛型
>
> 能使用接口就用接口, 否则就用别名, 没有继承没有扩展也可以直接使用别名(type)
>
> ![](readme-assets/005.png)


> 泛型
>
> ![](readme-assets/006.png)

---

ts里类型 可以做运算, 即根据现有的类型得到一个新的类型

能使用接口就用接口(接口无法描述联合类型 `I1|I2` 这种并不支持), 否则就用别名, 没有继承没有扩展也可以直接使用别名(type)

---

ts里与类型有关的、具有特殊意义的操作符
+ extends
+ typeof
+ keyof
+ in
+ is

---

注意联合类型 在 各种操作符 下是怎么运作的

---

>https://www.typescriptlang.org/docs/handbook/declaration-files/publishing.html

声明文件的查找:
1. 默认先查找node_modules ---> 对应包的package.json ---> types字段
2. 如果没有types字段, 则会找对应包下有没有index.d.ts
3. 如果没有说明此包没有用ts来写
4. 找 @types/对应的包 下的 types字段

---带修正---

5. 如果上面还是没找到, 会找当前工程下所有.d.ts文件
6. 如果这样都没找到,可以手动指定查找路径,tsconfig.json里的:
    + `paths`字段
    + `include`字段

---
## todo
>package.json typings字段 和 types字段 的区别
>
> 没有区别 --->https://www.typescriptlang.org/docs/handbook/declaration-files/publishing.html

>ts.transpileModule、ts.readConfigFile 等 typescript包下的命令, emmm 官网貌似没有近一步的说明
