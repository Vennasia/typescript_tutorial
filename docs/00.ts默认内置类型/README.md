内置类型如下

![](readme-assets/00.png)

![](readme-assets/01.png)

![img.png](readme-assets/02.png)

需要注意的是全局下 我们ts中有一个name属性(window.name)

全局下有的我们就不能重复在全局下声明了

```tsx
let name = '123'
```
+ ↑ 这样会报错

但你可以在自有模块下声明自己的
```tsx
let name = '123'

export {}
```
+ `export {}` 表示本文件是一个模块 不是放在全局下的 也就可以防止冲突
