export = {};

/** 1. typeof*/
function getVal(val: string | number) {
  if (typeof val === 'string') {
    val //val: string
  }else {
    val //val: number
  }
}


/** 2. instanceof*/
class Person {}
class Dog {}
function getInstance(val: Person | Dog) {
  if(val instanceof Person){
    val //val: Person
  }else {
    val //val: Dog
  }
}


/** 3. in*/
type a = { a: 1;};
type b = { b: 2;};
function getV(val: a | b) {
  if('a' in val){
    val //val: a
  }else {
    val //val: b
  }
}


/** 4. 可辨识类型 （增加了一个标识而已 没有撒特殊的）*/
type c = { c: 1; flag: 'a'};
type d = { d: 1; flag: 'd'};
function getV2(val: c | d){
  if(val.flag === 'a'){
    val //val: c
  }else {
    val //val: d
  }
}

