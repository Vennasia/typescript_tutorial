export = {}
/** 类型保护与区分类型（Type Guards and Differentiating Types）*/

interface Bird {
  fly();
  layEggs();
}

interface Fish {
  swim();
  layEggs();
}

function getSmallPet(): Fish | Bird {
  // ...
  return
}

let pet = getSmallPet();

// 联合类型适合于那些值可以为不同类型的情况。
// 但当我们想确切地了解是否为 Fish时怎么办？
// JavaScript里常用来区分2个可能值的方法是检查成员是否存在。
// 如之前提及的，我们只能访问联合类型中共同拥有的成员。

// 每一个成员访问都会报错
if (pet.swim) {
  // 之所以会报错 是因为pet有可能是Fish类型有可能是Bird类型
  // 而如果是Bird类型就不能pet.swim 只要.swim ts就会报错
  // 反之如果是Fish类型 则pet.fly 也会报错
  pet.swim();
}
else if (pet.fly) {
  pet.fly();
}

/** 为了让这段代码工作，我们要使用类型断言：*/
let pet2 = getSmallPet();

if ((<Fish>pet2).swim) {
  (<Fish>pet2).swim();
}
else {
  (<Bird>pet2).fly();
}
/*↑
这里可以注意到我们不得不多次使用类型断言。
假若我们一旦检查过类型，就能在之后的每个分支里清楚地知道 pet的类型的话就好了。
*/


