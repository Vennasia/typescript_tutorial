export = {}


function sum(a: number, b: unknown) {
  if(typeof b === 'string'){
    return a + b;
  }

  if(typeof b === 'number'){
    return a + b;
  }

  return b as boolean;
}

//------------------

let c: unknown = 1;
function isFunction(val:any):val is ()=>void {
  return typeof val === 'function';
}
if(isFunction(c)){
  c();
}

//---------------------

// const data = JSON.parse(`{name:'ahhh',age:123}`);
// data.name // √
// data.age //√
// data.job //√
//↑ 但这样其实是不安全的 因为撒属性都可以点 并且 我们的JSON.parse也可能解析错误

const data: unknown = JSON.parse(`{name:'ahhh',age:123}`);

function isXxx(val: any): val is { name: string; age: number;} {
  return val.name && val.age;
}

if(isXxx(data)){
  data.name
}
