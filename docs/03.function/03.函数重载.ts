export = {}

/** 为什么要有函数重载*/
// function add(n1,n2){
//   return n1 + n2;
// }
//
// add(1, 2); //3
// add('ahhh', 'fancier'); //'ahhhfancier'
// add(1, '2'); //'12' ← 不是我们所期望的


/** 函数重载*/
function add(n1: number, n2: number): number
function add(n1: string, n2: string): string
function add(n1: any, n2: any):any /** ← 这里声明的类型实际无所谓了, 没有实际作用(不影响下面实际调用时对类型的验证), 但这里必须写, 且指定的类型应该兼容上面所有重载的类型, 不然会报错*/{
  //如果你是以重载形式写的,最后一排(种)类型就只是用来实现,不用来当做类型
  //故虽然这里显示的n1、n2的类型是any,但其实没有用
  //下面还是会报错
  return n1 + n2;
}

add(1, 2); //3
add('ahhh', 'fancier'); //'ahhhfancier'
add(1, '2');
/*↑
TS2769: No overload matches this call.
Overload 1 of 2, '(n1: number, n2: number): any', gave the following error.
Argument of type '"2"' is not assignable to parameter of type 'number'.
Overload 2 of 2, '(n1: string, n2: string): any', gave the following error.
Argument of type '1' is not assignable to parameter of type 'string'.
*/


/** 虽然ts里的重载并不像其它语言那么强大(支持不同个数参数)
    但泛型依然不能完全替代重载的作用
    泛型太泛了,这种情景下可以使用重载*/
function add2<T>(n1: T, n2: T): T {
  return n1 + n2; // TS2365: Operator '+' cannot be applied to types 'T' and 'T'.
}
