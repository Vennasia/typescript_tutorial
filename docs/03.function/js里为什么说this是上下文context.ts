
/*
 this 是一个代词

 它 代指什么
 你就要看上下文了

 this 是什么
 你就要看上下文了

 this代表上下文里的一个东西
 那到底代表什么东西
 那就是从上下文里面去找
 那什么是上下文
 你所需要知道的所有东西都是上下文
 如果你想要知道this是什么
 你就把你所有东西都猜一遍
 然后猜那个可能是你最想要的东西
 那就是this代指的东西
*/

// 当我们处理点击的时候 [最需要]的是什么
// 我们点击的是谁
// 故这里 this就是b这个按钮
// b.onclick



// this就是你new出来的对象
// 因为你在处理一个vue的实例的时候,你[最需要]知道的东西就是这个实例
/*new Vue({
  el: '#xxx',
  created() {
    console.log(this);
  }
});*/


//裸函数里
function 在公园(){
  console.log(this); // window or undefined
}

在公园(); // 谁在公园啊？ "谁"是谁啊 不知道啊 -> undefined



var object = {
  见父母() {
    console.log(this);
  }
};

object.见父母(); // 对象.见父母 打印出this(ta) 这个ta当然是对象咯


let fn = object.见父母;

fn(); // 因为↑隐藏了上下文 浏览器没那么聪明 就以为你是在调用一个裸的函数,这里this就是window(call)
