export = {}

function defaultFn(a = '', b?: string /** ↔ 并不等价, 虽然你把鼠标移上去是这么显示类型的, 但你可以看下面调用 是会报错的*/ /*b: string|undefined*/) {
  console.log(a);
  if(b){
    console.log('b:',b);
  }
}

/*
function defaultFn(
  a?: string,
  b?: string | undefined
): void
*/
defaultFn();
