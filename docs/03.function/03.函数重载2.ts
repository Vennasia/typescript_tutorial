export  = {}
//希望把一个字符串,或则数字 转成数组
// 123 => [1,2,3]
// '123' => ['1','2','3']
function toArray(value:number):number[]
function toArray(value:string):string[]
function toArray(value: number | string): any {
  if (typeof value == 'string') {
    return value.split('');
  }else {
    return value.toString().split('').map(item=>parseInt(item))
  }
}

//按住ctrl分别移动到下面的toArray, 可以发现类型已经正确显示
toArray('123');
toArray(123);
