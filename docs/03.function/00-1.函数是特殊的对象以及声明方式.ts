export = {}

/** 函数在js里是一种特殊的对象,它可以被调用(call),而普通对象是不能的*/
// xxx();
// xxx.call()

// 如果一个函数他是另外一个对象的属性,就又叫做方法(method)


/** 生成一个匿名函数 然后把地址给hi2*/
let hi = function (name:string,age:number): string {
  if(age>18){
    return 'hi';
  }else{
    return undefined //undefined默认是其它类型的子类型(非严格模式下) 故这里函数返回类型只设置为string即可
  }
};
// hi('ahhh'); // TS2554: Expected 2 arguments, but got 1.

/** 直接声明一个有名字的函数 但这个名字存的依然是其地址*/
function hi2(
  name:string,
  age?/*←可选*/:number
) {
  console.log(`Hi, ${name}`)
}
hi2('ahhh');


/** 表达式的方式声明一个函数*/
// hi3也存的是地址 因为函数是对象 存在堆内存 没有办法直接存在栈内存
//
//TS7006: Parameter 'name' implicitly has an 'any' type.
//需要将  "noImplicitAny": false , 但一般还是采用默认的true即可
// let hi3 = (name,age) =>{}
//
let hi3 = (
  name:string,
  age/*←自动推倒类型为number*/ = 15
):string|number => {
  if(age>18){
    return 'hi';
  }else{
    return 404
  }
};
hi3('ahhh');
