// 怎么给函数本身定义类型? (使用函数关键字声明的方式不需要, 只有表达式声明的方式才需要)

// 如果在函数里面标识, 返回值需要用 => 来标识, 而不是 `:`
let sum1: /**这一坨是类型 →*/(a1: string, b1: string) => string/**← 都是类型*/ = (a, b) => {
  return a + b
};
// 但一般我们不需要给函数加类型, 因为函数表达式可以自动推导
let sum2 = (a: string, b: string) => {
  return a + b
};
sum2('a','b');


//另外需要注意的是↓这样 先定义一个fn 然后把fn赋给sum, 和我们直接在sum中定义fn的意义是不一样的
//和sum一起写 我们的参数和返回值 是能和sum的类型对应起来的
//但如果把fn拿出来定义, 它就变成了把fn 赋给 sum, 看一下fn是否满足sum的类型
//这样的话 fn 自己也要表示它的类型
// const fn = (a, b) => a + b;
const fn = (a:string, b:string) => a + b;
const sum4: (a: string, b: string) => string = fn; // 这样写表示我要看这个fn是否满足sum类型


/** 我们一般只有在使用表达式声明函数的时候, 并且不立即给它赋初始值的情况下, 才需要显示的给函数本身声明类型*/
let sum5: (a: string, b: string) => string;
sum5 = fn;


type Sum = (a1: string, b1: string) => string;
let sum3: Sum = (a, b) => {
  return a + b
};


//函数关键字声明的写法是不能给函数本身添加类型的 (默认就会推到出来是个函数)
//我们只需要关注函数的参数和返回值
function sum6(): number {
  return 123;
}

export {}
