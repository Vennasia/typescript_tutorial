export = {};

function add(a: number, b: number/*a,b形式参数*/): number {
  // 当add(1,2)要进入add函数时
  // 会生成一个arguments对象
  // arguments = {0:100,2:200,length:2}
  // 形式参数实际上就是给arguments对应的数字声明一个变量
  // let a = arguments[0]
  // let b = arguments[0]

  // arguments.length //arguments: IArguments

  return a + b;
}

add(100, 200); // 实际参数

//故我们也可不使用形参 而直接使用arguments
function add2(): number {
  return arguments[0] + arguments[1];
}


// 关于this
// this是参数, 只能在调用时才能确定
// 直观理解上面这句话, this就是fn.call()接受的第一个参数
function printThis(){
  'use strict';
  console.log(this);
  console.log(arguments); // [Arguments] { '0': 100, '1': 200 }
}

printThis.call('fuck',100,200); // fuck
printThis(); // ←小白写法
// 1.1 this => window 浏览器里
// 1.2 this => global对象 node
// 1.3 this => undefined 'use strict'

let obj = {
  fn(a) {
    console.log('this:',this);
    console.log(a) // 如果面试官就这样直接问你a这个形参的值是什么 你会觉得他是神经病; this也是一样
  }
};

// obj.fn();
// this 就是 .fn 前面的东东

obj.fn.call('fuck'); // this: fuck




//其它this情况
//+ apply bind
//+ new
//+ eval
//+ with


