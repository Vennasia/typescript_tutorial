let fn = (a: number, b: number): number => {
  //箭头函数没有this arguments new.target
  //如果你非要写,你访问到的this、arguments(× 不能用)、new.target 是外面的

  //关于 new.target
  //在普通的函数调用中（和作为构造函数来调用相对），new.target的值是undefined。这使得你可以检测一个函数是否是作为构造函数通过new被调用的。
  //在类的构造方法中，new.target指向直接被new执行的构造函数。并且当一个父类构造方法在子类构造方法中被调用时，情况与之相同。

  return a + b
};

export {}
