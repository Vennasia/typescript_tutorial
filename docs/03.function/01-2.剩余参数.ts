export = {}
/** 剩余参数*/
let sum = (...args:(number|string)[])=>{

}

/** 剩余参数中any和any[]的区别*/
let sum2 = (...args: any) => {
  //如果是 ...args: any, 我们这里foreach的时候必须再显示的指定类型
  args.forEach((arg: any) => console.log(arg));
};
sum2(1, 2, 3, 4, 5);
//
let sum3 = (...args:any[])=>{
  //但如果是 ...args:any[]
  //我们这里就不需要显示的声明了 它可以自动推导
  args.forEach(arg => console.log(arg));
}
sum3(1, 2, 3);
