//传统的CommonJS 和 AMD 不支持默认导出,
//ts为了让CommonJS 和 AMD也能默认导出
//就提出了export = and import = require()
//但这种语法不适用于  "module": "ESNext" ,即生成模块为es6module的情况

import zip = require("./ZipCodeValidator");
// ↑区别在于, ↓引入的zip类型为any, 而↑的能正确识别类型为 (alias) class zip
// 也就是说↑的能点 ↓面的不能点, 但实际情况来说, 如果你用的idea, 都是能.的
// const zip = require("./ZipCodeValidator");

zip

// Some samples to try
let strings = ["Hello", "98052", "101"];

// Validators to use
let validator = new zip();

// Show whether each string passed each validator
strings.forEach((s) => {
  console.log(
    `"${s}" - ${validator.isAcceptable(s) ? "matches" : "does not match"}`
  );
});
