"use strict";
/** 使用了 --strictNullChecks，可选参数会被自动地加上 | undefined:*/
function f(x, y) {
    return x + (y || 0);
}
f(1, 2);
f(1);
f(1, undefined);
/** 当并不会自动 | null*/
f(1, null); // error, 'null' is not assignable to 'number | undefined'
module.exports = {};
