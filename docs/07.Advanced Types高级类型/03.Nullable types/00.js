"use strict";
/** tsc --watch
 可能编辑器不会报红
 运行该条命令后 可发现错误提示*/
/*
TypeScript具有两种特殊的类型， null和 undefined，它们分别具有值null和undefined. 我们在[基础类型](./Basic Types.md)一节里已经做过简要说明。 默认情况下，类型检查器认为 null与 undefined可以赋值给任何类型。 null与 undefined是所有其它类型的一个有效值。 这也意味着，你阻止不了将它们赋值给其它类型，就算是你想要阻止这种情况也不行。 null的发明者，Tony Hoare，称它为 价值亿万美金的错误。

--strictNullChecks标记可以解决此错误：当你声明一个变量时，它不会自动地包含 null或 undefined。 你可以使用联合类型明确的包含它们：
* */
var s = "foo";
s = null; // 错误, 'null'不能赋值给'string'
var sn = "bar";
sn = null; // 可以
sn = undefined; // error, 'undefined'不能赋值给'string | null'
module.exports = {};
/** 注意，按照JavaScript的语义，TypeScript会把 null和 undefined区别对待。
 * string | null， string | undefined和 string | undefined | null是不同的类型*/
