export = {};

/** this的多态
  当我们没有子类型时 multiple方法里的this就是Calc的实例类型
  当我们写了其子类型时 multiple方法里的this就是BiggerCalc的实例类型

  故将这种现象称之为多态
  所以叫它高级类型
*/
class Calc {
  public value: number;

  constructor(n: number) {
    this.value = n;
  }

  add(n: number) {
    this.value += n;
    return this;
  }

  multiple(n: number) {
    this.value *= n;
    return this; // 这个this是普通计算器类型
  }
}

const c = new Calc(111);
c.add(2).add(3).multiple(7);
console.log(c.value);

class BiggerCalc extends Calc {
  sign() {
    this.value = Math.sign(this.value);
    return this;
  }
}

const b = new BiggerCalc(1);
b.
add(2).
add(3).
multiple(7). /*multiple是普通计算器类上的方法,它返回的this应该是普通计算器对象(类型),但为什么能.sign呢? 这是因为ts牛逼! 能猜到 emmm*/
sign(). /*这里之所以能.add的原因同上*/
add(1);


function fn(this: string|void, n: number) {
  console.log(n);
}
fn(1); // this is undefined
fn.call('string', 1);
