export = {};

interface IInfo {
  num: number,
  name: string
}

interface IFruit {
  color: string,
  age: number,
  taste: string,
  info: IInfo
}

type IFruitType = Partial<IFruit>
/** 默认的Partial是不支持递归的*/
let fruit: IFruitType = {
  //TS2739: Type '{}' is missing the following properties from type 'IInfo': num, name
  info: {

  }
}

/** 结合条件类型实现递归*/
type DeepPartial<T> = {
  [K in keyof T]?: T[K] extends object ? DeepPartial<T[K]>: T[K]
}
let fruit2: DeepPartial<IFruit> = {
  info: {

  }
}
