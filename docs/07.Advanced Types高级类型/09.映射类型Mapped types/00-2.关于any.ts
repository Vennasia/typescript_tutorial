export = {};

interface Person3 {
  name?: string;
  age?: number;
  grade?: number;
}

// type Required<T> = {
//   [K in keyof any]-?: any
// };
//
// type ReadonlyType = Required<Person3>; //{[p: string]: any}



type Required<T> = {
  [K in any]-?: any
};

type ReadonlyType = Required<Person3>; //{[p: string]: any}
