export = {};
/** 映射类型修饰符的控制符*/
/*

TypeScript中增加了对映射类型修饰符的控制

具体而言，一个 readonly 或 ? 修饰符在一个映射类型里可以用前缀 + 或-来表示这个修饰符应该被添加或移除

TS 中部分内置工具类型就利用了这个特性（Partial、Required、Readonly...），这里我们可以参考 Partial、Required 的实现
*/

