export  = {};

// let obj: object = {};

//比object强一点
let obj: Record<string,number> = {
  // 'aaa':'bbb' //TS2322: Type 'string' is not assignable to type 'number'.
  'a':1
};
/*type Record<K extends keyof any, T> = {
  [P in K]: T
};*/

//mapping
function mapping<K extends keyof any/*string|number|symbol*/,V,R>(obj: Record<K, V>, callback: (key: K, value: V) => R): Record<K, R> {
  let result = {} as Record<K, R>;
  for (let key in obj){
    result[key] = callback(key, obj[key]);
  }
  return result;
}
const r = mapping({a:1,b:2,c:3,d:4},function(key,value){
  return value * 3;
});
console.log(r);
