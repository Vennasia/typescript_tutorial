export = {};

/** 6. 实现两个对象类型的合并
 *     如果两个对象之间有相同的属性 则会用后面的那个*/
/*function merge<T extends object, K extends object>(a: T, b: K) {
  return {...a,...b}
}
let r1 = merge({a: 1, c: 15}, {b: 2, a: 'aaa'}); //let r1: {a: number, c: number} & {b: number, a: string}
let r2 = merge(function(){},function(){}); //let r2: () => void
type Compute<T> = { [K in keyof T]: T[K] };
type Temp = Compute<typeof r1>; //{a: never（emm但我们这里显示的是number 可能编辑器原因？）, c: number, b: number}*/

function merge2<T extends object, K extends object>(a: T, b: K): Pick<T, Exclude<keyof T, keyof K>> & K {
  return {...a, ...b}
}
let r3 = merge2({a: 1, c: 15}, {b: 2, a: 'aaa'}); //let r3: Pick<{a: number, c: number}, "c"> & {b: number, a: string}
type Compute<T> = { [K in keyof T]: T[K] };
type Temp = Compute<typeof r3>; //{c: number, a: string, b: number}
