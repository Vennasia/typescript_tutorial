export = {}
type O = { a: 1, b: 2 };
let x: O = {a: 222, b: 333}; // TS2322: Type '222' is not assignable to type '1'.

type O2 = { a: string, b: number };
let x2: O2 = {a: 'a', b: 'b'}; // TS2322: Type 'string' is not assignable to type 'number'.
