export = {};

//怎么把元组转换成联合类型
//[string, number, boolean] => string | number | boolean

type typing = [string, number, boolean];
type typing1 = [string, number, boolean][number]; //string | number | boolean
type typing2 = [string, number, boolean][1]; //number

// 用infer怎么做？
type Transfer<T> = T extends Array<infer A> ? A : any;
type toUnion = Transfer<typing>; //string | number | boolean
