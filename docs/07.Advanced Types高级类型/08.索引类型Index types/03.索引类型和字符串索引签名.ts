export = {}
/** Index types and index signatures*/
/*
keyof and T[K] interact with index signatures.
An index signature parameter type must be ‘string’ or ‘number’. */

/** If you have a type with a string index signature, keyof T will be string | number (and not just string, since in JavaScript you can access an object property either by using strings (object['42']) or numbers (object[42])). And T[string] is just the type of the index signature:
*/
interface Dictionary<T> {
  [key: string]: T;
}
let keys: keyof Dictionary<number>; // string | number
let value: Dictionary<number>['foo']; // number

/** If you have a type with a number index signature, keyof T will just be number.*/
interface Dictionary2<T> {
  [key: number]: T;
}
let keys2: keyof Dictionary2<number>; // number
let value2: Dictionary2<number>['foo']; // Error, Property 'foo' does not exist on type 'Dictionary<number>'.
let value3: Dictionary2<number>[42]; // number
