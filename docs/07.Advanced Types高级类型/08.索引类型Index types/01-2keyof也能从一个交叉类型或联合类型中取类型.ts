export = {};

function merge<T extends object, K extends object>(a: T, b: K) {
  return {...a,...b}
}
let r1 = merge({a: 1}, {b: 2}); //let r1: {a: number} & {b: number}

type Compute<T> = { [K in keyof T]: T[K] };
type Temp = Compute<typeof r1>; //{a: number, b: number}

type x = {a: number} & {b: number};
type Temp2 = Compute<x>; //{a: number, b: number}

//↓ 联合类型就分发了
type x2 = {a: number} | {b: number};
type Temp3 = Compute<x2>; //{a: number} | {b: number}
