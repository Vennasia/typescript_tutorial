export = {};

/** keyof T, 索引类型查询操作符。 index type query operator*/
//对于任何类型 T， keyof T的结果为 T上已知的公共属性名的联合
interface Car {
  manufacturer: string;
  model: string;
  year: number;
}
let carProps: keyof Car; // the union of ('manufacturer' | 'model' | 'year')
/*↑
keyof Car是完全可以与 'manufacturer' | 'model' | 'year'互相替换的。
不同的是如果你添加了其它的属性到 Car，例如 ownersAddress: string，那么 keyof Car 会自动变为 'manufacturer' | 'model' | 'year' | 'ownersAddress'。*/

/** keyof 也可以在泛型上下文中使用*/
function pluck2<T, K extends keyof T>(o: T, propertyNames: K[]): T[K][] {
  return propertyNames.map(n => o[n]);
}
