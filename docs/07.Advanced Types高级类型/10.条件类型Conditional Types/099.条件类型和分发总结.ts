export = {}

interface Bird {
  name: '鸟'
}

interface Sky {
  color: '蓝色'
}

interface Fish {
  name: '鱼'
}

interface Water {
  color: '透明'
}

//根据传的类型来决定最终使用哪个类型
// type MyType<T> = T ? Sky:Water; // ← 并不支持这么写
type MyType<T extends Bird | Fish>/** ←约束用户传参,和我们→等号后面那个T extends Bird不是一个意思*/ =
/** ↓T是否符合（满足）xxx*/
  T extends Bird/** ←这个extends是一个三元表达式的【条件】，看下T是否满足（符合）Bird这个条件(Condition Types)*/ ?
    Sky :
    Water;


/** 联合类型 --- 条件的分发
 * 当泛型传参是联合类型时,会进行条件的分发
 * 而不是把它当做联合类型来看待 */

/** 分发的意思是拿联合类型的每一个进行判断
 *  即
 *  先拿Bird进行条件判断
 *  再拿Fish进行条件判断
 *  最后再把结果 | 在一起
 *
 *  注意：
 *      1. 分发只会发生在联合类型 &交集类型没有这种类似的效果
 *      2. 分发只能出现在裸类型(naked type)中, 比如 `type MyType<T extends Bird|Fish> = [T] extends [Bird] ? Sky: Water;`
 *         包了一层 `[]`, 是不行的, 它不会分发了, 而是会把 Bird|Fish 传参当做一个整体, 此时显然 Bird|Fish 不满足 [Bird], 故就会返回Water
 */

type x = MyType<Bird|Fish> //type x = Sky | Swimming
//会先把Bird传进去 然后拿到结果 Sky
// type x = MyType<Bird>
//然后再把Fish传进去 拿到结果Swimming
// type y = MyType<Fish>
//最终将这俩结合起来变成联合类型
// type z = x|y


// 分发只能出现在裸类型(naked type)中, 比如 `type MyType<T extends Bird|Fish> = [T] extends [Bird] ? Sky: Water;`
// 包了一层 `[]`, 是不行的, 它不会分发了, 而是会把 Bird|Fish 传参当做一个整体, 此时显然 Bird|Fish 不满足 [Bird], 故就会返回
type MyType2<T extends Bird|Fish> = [T] extends [Bird] ? Sky: Water;
type test = MyType2<Bird | Fish>; //Water
