export = {};
/** Type inference in conditional types*/
/*
		在条件类型的extend子句中
		，现在可以用 infer声明关键字来指定一个变量来代指那个最终会得到的类型(我们称其为 inferred type variables 推断类型变量)。
		这种推断类型变量可以在条件类型的true分支中引用。
		同一infer声明的变量可能会在多个位置存在。

		Within the extends clause of a conditional type
		, it is now possible to have infer declarations that introduce a type variable to be inferred.
		Such inferred type variables may be referenced in the true branch of the conditional type.
		It is possible to have multiple infer locations for the same type variable.
*/


//例如，下面提取函数类型的返回类型：
//For example, the following extracts the return type of a function type:
type ReturnType<T> = /*[*/T extends (...args: any[]) => infer R ? R : any/*]*/;


//了解更多
//TODO
//>http://www.typescriptlang.org/docs/handbook/advanced-types.html#type-inference-in-conditional-types
