export = {};
/** Distributive conditional types*/
// Conditional types in which the checked type is a naked type parameter are called distributive conditional types.
// Distributive conditional types are automatically distributed over union types during instantiation.
// Conditional types为裸类型参数的条件类型称为分配条件类型(distributive conditional types)。
// 在实例化过程中，分布条件类型自动分布在联合类型(union types)上。
//
//比如
/*
T extends U ? X : Y
T为 A|B|C

则等价于
(A extends U ? X : Y) | (B extends U ? X : Y) | (C extends U ? X : Y)

必要条件:
1) 只有联合类型能分发, 交叉类型不能分发
2) 非裸类型不能分发
*/

/** 非裸类型不能分发*/
interface Bird {
  name: '鸟'
}
interface Sky {
  name: '蓝色'
}
interface Fish {
  name: '鱼'
}
interface Water {
  name: '透明'
}
type MyType<T extends Bird|Fish> = T extends Bird ? Sky: Water;

// type MyType<T extends Bird|Fish> = [T]/*T*/ extends [Bird] ? Sky: Water;
//打开 ↑, 鼠标移到下面的IEnv上 你会发现计算结果为Water, 并不是我们所期望的
//这就是因为它不是裸类型 而是 [T] ← 它是包了一层衣服的 不是裸的

type IEnv = MyType<Bird|Fish>; //Sky | Water


//-------------------------------------------------------------------------------





