/**
 模块解析是指编译器在查找导入模块内容时所遵循的流程。假设有一个导入语句 import { a } from "moduleA"; 为了去检查任何对 a的使用，编译器需要准确的知道它表示什么，并且需要检查它的定义moduleA。

 这时候，编译器会有个疑问“moduleA的结构是怎样的？” 这听上去很简单，但 moduleA可能在你写的某个.ts/.tsx文件里或者在你的代码所依赖的.d.ts里。

 首先，编译器会尝试定位表示导入模块的文件。 编译器会遵循以下二种策略之一： Classic或Node。 这些策略会告诉编译器到 哪里去查找moduleA。

 如果上面的解析失败了并且模块名是非相对的（且是在"moduleA"的情况下），编译器会尝试定位一个外部模块声明(@types)。 我们接下来会讲到非相对导入。

 最后，如果编译器还是不能解析这个模块，它会记录一个错误。 在这种情况下，错误可能为 error TS2307: Cannot find module 'moduleA'.
*/

/** node策略下的示例参看 */
// + 00为什么需要以及如何生成声明文件/case4
// + 02如何给任意JS添加声明
/*
比如，有一个导入语句import { b } from "./moduleB"在/root/src/moduleA.ts里，会以下面的流程来定位"./moduleB"：
/root/src/moduleB.ts
/root/src/moduleB.tsx
/root/src/moduleB.d.ts
/root/src/moduleB/package.json (如果指定了"types"属性)
/root/src/moduleB/index.ts
/root/src/moduleB/index.tsx
/root/src/moduleB/index.d.ts

类似地，非相对的导入会遵循Node.js的解析逻辑，首先查找文件，然后是合适的文件夹。 因此 /root/src/moduleA.ts文件里的import { b } from "moduleB"会以下面的查找顺序解析：

/root/src/node_modules/moduleB.ts
/root/src/node_modules/moduleB.tsx
/root/src/node_modules/moduleB.d.ts
/root/src/node_modules/moduleB/package.json (如果指定了"types"属性)
/root/src/node_modules/moduleB/index.ts
/root/src/node_modules/moduleB/index.tsx
/root/src/node_modules/moduleB/index.d.ts

/root/node_modules/moduleB.ts
/root/node_modules/moduleB.tsx
/root/node_modules/moduleB.d.ts
/root/node_modules/moduleB/package.json (如果指定了"types"属性)
/root/node_modules/moduleB/index.ts
/root/node_modules/moduleB/index.tsx
/root/node_modules/moduleB/index.d.ts

/node_modules/moduleB.ts
/node_modules/moduleB.tsx
/node_modules/moduleB.d.ts
/node_modules/moduleB/package.json (如果指定了"types"属性)
/node_modules/moduleB/index.ts
/node_modules/moduleB/index.tsx
/node_modules/moduleB/index.d.ts
*/




/** class策略 */
/*
这种策略在以前是TypeScript默认的解析策略。 现在，它存在的理由主要是为了向后兼容。

相对导入的模块是相对于导入它的文件进行解析的。 因此 /root/src/folder/A.ts文件里的import { b } from "./moduleB"会使用下面的查找流程：

/root/src/folder/moduleB.ts
/root/src/folder/moduleB.d.ts
对于非相对模块的导入，编译器则会从包含导入文件的目录开始依次向上级目录遍历，尝试定位匹配的声明文件。

比如：

有一个对moduleB的非相对导入import { b } from "moduleB"，它是在/root/src/folder/A.ts文件里，会以如下的方式来定位"moduleB"：

/root/src/folder/moduleB.ts
/root/src/folder/moduleB.d.ts
/root/src/moduleB.ts
/root/src/moduleB.d.ts
/root/moduleB.ts
/root/moduleB.d.ts
/moduleB.ts
/moduleB.d.ts
*/
