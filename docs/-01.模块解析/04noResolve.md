正常来讲编译器会在开始编译之前解析模块导入。 每当它成功地解析了对一个文件 import，这个文件被会加到一个文件列表里，以供编译器稍后处理。

`--noResolve`编译选项告诉编译器不要添加任何不是在命令行上传入的文件到编译列表。 编译器仍然会尝试解析模块，但是只要没有指定这个文件，那么它就不会被包含在内。

比如

app.ts
```javascript
import * as A from "moduleA" // OK, moduleA passed on the command-line
import * as B from "moduleB" // Error TS2307: Cannot find module 'moduleB'.
```

```shell script
tsc app.ts moduleA.ts --noResolve
```

使用`--noResolve`编译`app.ts`：

+ 可能正确找到moduleA，因为它在命令行上指定了。
+ 找不到moduleB，因为没有在命令行上传递。
