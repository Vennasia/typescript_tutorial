/*
有时多个目录下的工程源文件在编译时会进行合并放在某个输出目录下。 这可以看做一些源目录创建了一个“虚拟”目录。

利用rootDirs，可以告诉编译器生成这个虚拟目录的roots； 因此编译器可以在“虚拟”目录下解析相对模块导入，就 好像它们被合并在了一起一样。

比如，有下面的工程结构：

 src
 └── views
     └── view1.ts (imports './template1')
     └── view2.ts

 generated
 └── templates
         └── views
             └── template1.ts (imports './view2')

src/views里的文件是用于控制UI的用户代码。 generated/templates是UI模版，在构建时通过模版生成器自动生成。 构建中的一步会将 /src/views和/generated/templates/views的输出拷贝到同一个目录下。 在运行时，视图可以假设它的模版与它同在一个目录下，因此可以使用相对导入 "./template"。

可以使用"rootDirs"来告诉编译器。 "rootDirs"指定了一个roots列表，列表里的内容会在运行时被合并。 因此，针对这个例子， tsconfig.json如下：

{
  "compilerOptions": {
    "rootDirs": [
      "src/views",
      "generated/templates/views"
    ]
  }
}

[重要]每当编译器在某一rootDirs的子目录下发现了相对模块导入，它就会尝试从每一个rootDirs中导入。
+ 这个配置项是针对 相对导入的
+ baseUrl+paths是非相对导入
*/
