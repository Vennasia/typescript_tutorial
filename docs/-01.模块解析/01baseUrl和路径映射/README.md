[toc]

## baseUrl
设置`baseUrl`来告诉编译器到哪里去查找模块(绝对模块)。 所有绝对模块导入都会被当做相对于 `baseUrl`。 (注意 如果找不到 会回退到对应的模块查找策略进行查找)

baseUrl的值由以下两者之一决定：

+ 命令行中`baseUrl`的值（如果给定的路径是相对的，那么将相对于当前路径进行计算）
+ `tsconfig.json`里的`baseUrl`属性（如果给定的路径是相对的，那么将相对于‘tsconfig.json’路径进行计算）

注意相对模块的导入不会被设置的`baseUrl`所影响，因为它们总是相对于导入它们的文件。

## paths
"paths"是相对于"baseUrl"进行解
```
projectRoot
├── folder1
│   ├── file1.ts (imports 'folder1/file2' and 'folder2/file3')
│   └── file2.ts
├── generated
│   ├── folder1
│   └── folder2
│       └── file3.ts
└── tsconfig.json
```
tsconfig.json↓
```shell script
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "*": [
        "*",
        "generated/*"
      ]
    }
  }
}
```
```
导入'folder1/file2'
匹配'*'模式且通配符捕获到整个名字。
尝试列表里的第一个替换：'*' -> folder1/file2。
替换结果为非相对名 - 与baseUrl合并 -> projectRoot/folder1/file2.ts。
文件存在。完成。

导入'folder2/file3'
匹配'*'模式且通配符捕获到整个名字。
尝试列表里的第一个替换：'*' -> folder2/file3。
替换结果为非相对名 - 与baseUrl合并 -> projectRoot/folder2/file3.ts。
文件不存在，跳到第二个替换。
第二个替换：'generated/*' -> generated/folder2/file3。
替换结果为非相对名 - 与baseUrl合并 -> projectRoot/generated/folder2/file3.ts。
文件存在。完成。
```
