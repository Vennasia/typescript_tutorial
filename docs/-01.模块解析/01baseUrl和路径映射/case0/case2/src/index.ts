import * as jQuery from 'jquery';
import * as React from 'react';

//这样就能点到了
// jQuery.default.ajax('123');

// 会影响原本jQuery使用吗(运行时的使用)? 不会↓↓
console.log('jQuery:', jQuery);
console.log('React:', React); // 也不会影响原本的node_modules包里的解析,So 改变paths和baseUrl只是追加了自定义的具有第一优先级的路径解析地址
