/*
外部枚举是使用declare enum定义的枚举

外部枚举是用来描述一个应该存在的枚举类型的，而不是已经存在的，它的值在编译时不存在，只有等到运行时才知道。
*/
var seasons = [
    0 /* Spring */,
    1 /* Summer */,
    2 /* Autumn */,
    3 /* Winter */
];
console.log(0 /* Spring */);
