/*
外部枚举是使用declare enum定义的枚举

外部枚举是用来描述一个应该存在的枚举类型的，而不是已经存在的，它的值在编译时不存在，只有等到运行时才知道。
比如这里的case0 实际运行时就会报错 但在编译时可以通过typescript检查
*/

/**=== case0 */
//此时运行tsc index.ts
//最终编译成的文件如case0所示
/*
var seasons = [
    Season.Spring,
    Season.Summer,
    Season.Autumn,
    Season.Winter
];
*/
declare enum Season {
  Spring,
  Summer,
  Autumn,
  Winter
}

let seasons = [
  Season.Spring,
  Season.Summer,
  Season.Autumn,
  Season.Winter
];

console.log(Season.Spring)



/**=== case1 */
/*
var seasons = [
  0 /!* Spring *!/,
  1 /!* Summer *!/,
  2 /!* Autumn *!/,
  3 /!* Winter *!/
];
*/
/*
declare const enum Season {
  Spring,
  Summer,
  Autumn,
  Winter
}

let seasons = [
  Season.Spring,
  Season.Summer,
  Season.Autumn,
  Season.Winter
];

console.log(Season.Spring)
*/
