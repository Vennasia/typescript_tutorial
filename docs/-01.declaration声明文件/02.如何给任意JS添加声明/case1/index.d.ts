// function y(a: number, b: number): number // TS1046: Top-level declarations in .d.ts files must start with either a 'declare' or 'export' modifier.
export function add(a: number, b: number): number

// TS1183: An implementation cannot be declared in ambient contexts ↓↓↓
//只能定义类型 不能定义具体实现；
// export function abc() {
//   return 1;
// }

declare function whatever(s: string): void;

export default whatever;

// declare function minus(a: number, b: number): number // 没用 index.ts里引入会报错
export function minus(a: number, b: number): number

