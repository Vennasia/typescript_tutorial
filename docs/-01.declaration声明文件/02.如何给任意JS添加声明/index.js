"use strict";
/*
有可能,你的ts项目中要用一个js库,这个库本身就不是用typescript写的
,没有自动生成的`*.d.ts`声明文件,也没有其他人帮它手动补上声明

此时就需要你自己手动给它补上声明

有两种方式可以补上声明
一: require方式
二: import + 声明文件方式
*/
exports.__esModule = true;
/** === 法一
require 方式

npm i @types/node
*/
// tsc index.ts --watch
/*interface I {
  (a: number, b: number): number;
}

const add: I = require('./case0');

add(1, 2);
add(1, '2'); // TS2345: Argument of type '"2"' is not assignable to parameter of type 'number'.*/
/** === 法二
import 方式

注意参看case0/index.d 里的注释
+ 声明文件中只能定义类型(*.d.ts) 不能定义具体实现
+ 声明文件中的Top-level declarations 必须以declare或则export开头
+ 只声明 不导出 是没有效果的 比如index.d里如果没有导出minus的声明 这里引用会报:TS2614: Module '"../../02\u5982\u4F55\u7ED9\u4EFB\u610FJS\u6DFB\u52A0\u58F0\u660E/src/case0"' has no exported member 'minus'. Did you mean to use 'import minus from "../../02\u5982\u4F55\u7ED9\u4EFB\u610FJS\u6DFB\u52A0\u58F0\u660E/src/case0"' instead?
*/
// tsc index.ts --watch
var case0_1 = require("./case0");
console.log(case0_1.add('1', 2)); //TS2345: Argument of type '"1"' is not assignable to parameter of type 'number'.
case0_1["default"](123); // TS2345: Argument of type '123' is not assignable to parameter of type 'string'.
console.log(case0_1.minus('2', 2)); // TS2345: Argument of type '"2"' is not assignable to parameter of type 'number'.
