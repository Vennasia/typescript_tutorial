```shell script
npm i -g typescript

```

npm声明文件可能的位置
+ node_modules/jquery/package.json
    + "types":"types/xxx.d.ts"
+ node_modules/jquery/index.d.ts
+ node_modules/@types/jquery/index.d.ts
+ tsconfig.json
    + baseUrl&paths


//todo
https://www.tslang.cn/docs/handbook/tsconfig-json.html

## 相关tsconfig配置
```json5
"exclude":[
  "typings/*"
]
```

```json5
"declarationDir":"./typings"
```

```json5
"declaration": true
"emitDeclarationOnly": true
```
