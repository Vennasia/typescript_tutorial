/*
在声明文件中的namespace表示一个全局变量包含很多子属性
在命名空间内部不需要使用 declare 声明属性或方法
*/

//如果说在html里通过CDN引用jquery,则会有个全局变量$
//但我们在ts工程汇总并不能直接使用它,会报错,需要↓
declare namespace $ {
  function ajax(url: string, setting: any): void;
  let name: string;
  namespace fn{
    function extend(object: any): void
  }
}

$.ajax('/users', {});
console.log($.name);
$.fn.extend({});
