[toc]
>https://www.tslang.cn/docs/handbook/declaration-files/publishing.html

两种方式:

## 法一: 与npm包捆绑在一起
通过`package.json`里的`types`or`typing`字段指定类型文件的位置

>如果你的类型声明依赖于另一个包：
+ 不要把依赖的包放进你的包里，保持它们在各自的文件里。
+ 不要将声明拷贝到你的包里。
+ 应该依赖于npm类型声明包，如果依赖包没包含它自己的声明的话。

## 法二：发布到@types
>@types下面的包是从DefinitelyTyped里自动发布的，通过 [types-publisher](https://github.com/Microsoft/types-publisher)工具。 如果想让你的包发布为@types包，提交一个pull request到 https://github.com/DefinitelyTyped/DefinitelyTyped 。 在这里查看详细信息 [contribution guidelines page](http://definitelytyped.org/guides/contributing.html)。


## 使用比较
第一种相较于第二种,用户只需要下载你的npm包而不需要额外下载`@types/x`的包,即可使用typescript特性(但需要手动指定声明文件位置)

而第二种,需要下载`@types`包(下载即可,无需其它额外操作)
```shell script
npm install --save @types/lodash
```

## 搜索发布的@types↓
>https://www.npmjs.com/~types
>
>大多数情况下，类型声明包的名字总是与它们在npm上的包的名字相同，但是有@types/前缀， 但如果你需要的话，你可以在 https://aka.ms/types这里查找你喜欢的库。
>
>注意：如果你要找的声明文件不存在，你可以贡献一份，这样就方便了下一位要使用它的人。 查看DefinitelyTyped [贡献指南](http://definitelytyped.org/guides/contributing.html)页了解详情。

## 核心库声明文件
JavaScript 中有很多内置对象，它们可以在 TypeScript 中被当做声明好了的类型
内置对象是指根据标准在全局作用域（Global）上存在的对象。这里的标准是指 ECMAScript 和其他环境（比如 DOM）的标准

这些内置对象的类型声明文件，就包含在TypeScript 核心库的类型声明文件中

>https://github.com/Microsoft/TypeScript/tree/master/src/lib

