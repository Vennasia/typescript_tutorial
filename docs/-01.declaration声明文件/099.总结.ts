export = {}
// vue //TS2304: Cannot find name 'vue'.
declare let vue:string
vue.slice()
//↑ declare并没有实现, 只是为了让我们的代码有提示功能, 并且不会报错(通过编译)

declare function sum():void
declare class Person {}
declare interface Tamoto {
  name: string
}

//编译时不会报错,运行时依然会报错↓
sum();
new Person();
let x:Tamoto;
console.log(vue);

//declare就是为了提示用,没有实现,也不允许实现
//declare let X:number = 0 //TS1039: Initializers are not allowed in ambient contexts



/** module就是namespace,不过是更老的写法, 不信你可以将鼠标移至下面的module A的A上面
 * 编辑器显示的是 namespace A*/
declare module A {
  const a:string //declare里面的内容 不需要导出 也不需要再增加declare 就可以在外边被 点(.) 出来
}
A.a


/** 声明合并*/
// declare const $: (selector: string) => {
//   height(val:number): void;
//   width(val:number): void;
// };
// $('xxx').width(100)
// $('xxx').width(2000)

// ↑上面这样写 并不能和下面的合并
// ↓需要这样写 (和上面不一样(上面的是变量声明),函数和命名空间是会自动合并声明的(命名空间和命名空间也能)）
declare function $(selector: string): {
  height(val:number): void;
  width(val:number): void;
};

//此时如果我们想为$下增加功能
declare namespace $ {
  namespace fn{
    function extend():void
  }
}
$.fn.extend()


/** 使用第三方*/
import $1 from 'jquery'
/* ↑
TS7016: Could not find a declaration file for module 'jquery'. 'D:/x/typescript_tutorial/node_modules/jquery/dist/jquery.js' implicitly has an 'any' type.   Try `npm install @types/jquery` if it exists or add a new declaration (.d.ts) file containing `declare module 'jquery';`
*/
//如果安装的库 不是用ts写的 我们在使用ts时 需要安装它对应的类型包(但并不一定有
//npm install @types/jquery


/** 扩展*/
interface String {
  double():string
}

String.prototype.double = function(){
  return this as string + this; //隐式类型转化 一方是字符串 另一方也会转换为字符串
}

declare global {

  interface Window {
    xxx:string
  }

}
window.xxx

// 1) 接口默认会合并 可以给已有的属性合并属性或者方法
// 2) 类和命名空间可以合并
// 3) 函数和命名空间可以合并
// 4) 枚举和命名空间可以合并
// 5) 交叉类型合并数据
// 6) 表达式都没法合并 撒 const xxx啊
