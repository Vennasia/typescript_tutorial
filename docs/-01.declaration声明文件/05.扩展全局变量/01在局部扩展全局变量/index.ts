/*
模块内全局扩展
*/

export {}

declare global {
  interface String {
    double():string;
  }
  interface Window {
    myname: string;
  }
}

String.prototype.double = function () {
  return this as string + this; //隐式类型转化 一方是字符串 另一方也会转换为字符串
};

console.log('hello'.double());

console.log(window.myname);

