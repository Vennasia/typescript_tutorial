//TS2339: Property 'doublue' does not exist on type '"hello"'. ↓
String.prototype.double = function () {
  return this as string + this; //隐式类型转化 一方是字符串 另一方也会转换为字符串
};

console.log('hello'.double());



// 添加↓接口即可 就不会报错了
interface String {
  double():string;
}


interface Window {
  myname: string;
}

console.log(window.myname);
