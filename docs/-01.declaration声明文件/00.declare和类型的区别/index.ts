/**
 declare 让我们可以 即使一个变量还没有赋值 但只要它的类型或则说shape确定了
 我们就可以在typescript编译阶段 使用这个declare出的东东上的属性和方法(不会报错,并且能获得我们自己写的类型作为编辑器提示)
 至于它运行时会不会报错 就是另一说了

 declare里不能有实现
*/
import a,{Person,p1} from './case0'; // 怎么查找的？ 参看模块解析-01

// console.log(Person); // TS2693: 'Person' only refers to a type, but is being used as a value here.

// declare虽然只是声明的一个变量的类型,但是它却可以被当做一个值来使用 不像上面一样会报错
// declare const a: (p: Person) => void;
console.log(a);

p1.say(); // 虽然这里运行时会报错,但这里编译时是会通过的


declare let xxx:string;

export = {};
