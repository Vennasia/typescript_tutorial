export interface Person {
    name: string;
    age: number;
    say():void;
}
declare const a: (p: Person) => void;
export declare const p1: Person;
export default a;
