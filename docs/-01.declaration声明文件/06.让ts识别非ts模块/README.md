# 让ts识别非ts模块

## .scss
```tsx
//variables.scss.d.ts

export interface ScssVariables {
  menuText: string;
  menuActiveText: string;
  subMenuActiveText: string;
  menuBg: string;
  menuHover: string;
  subMenuBg: string;
  subMenuHover: string;
  sideBarWidth: string;
  theme: string;
  settingPanelWidth: string;
}

export const variables: ScssVariables

export default variables
```
```jsx
// 导入scss变量在组件中使用
import variables from '@/styles/variables.scss'
```

## .vue
上面对于scss模块, 我们演示了如何定义一个完全限定的有名有姓的`.scss`文件的声明

其实我们也可以通过 `declare module '*.x'` 来指定某种模块的通用类型(更通用一点)

```tsx
/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
```
### 拓展自定义示例属性
```tsx
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $message: typeof ElMessage;
    $notify: typeof ElNotification;
    $confirm: typeof ElMessageBox.confirm;
    $alert: typeof ElMessageBox.alert;
    $prompt: typeof ElMessageBox.prompt;
  }
}
```

### png
```tsx
//x.d.ts
declare module "*.png"
```


