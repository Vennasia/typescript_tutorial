/**=== case0 */
// import a from './case0/index';

// 在命令行使用tsc index.ts后 会发现case0/index.ts里有关“类型”的部分都消失了
// 假如我们生成的 case0/index.js 就是我们打包共享出去的一个包
// 此时别人用ts导入这个js文件(allowJS为true),是使用不了我们case0/index.ts中定义的类型的
// 即此时别人调用a这个函数,想传什么就可以传什么(我们本来导入的如果是ts文件的话,a接受的必须是  {name: string;age: number;} 这样的,否则会报错)
// 之前的类型限制不再起作用,不再会报错 如↓ case1



/**=== case1 */
// import a from './case1/index.js';
// a(111); // ← 想传什么就可以传什么



/**=== case2 */
// 那么如果想要别人使用我们的包的时候,依然能使用打包之前定义好的typescript类型
// 我们可以使用 tsc index.ts -d 命令 此命令除了会生成编译好的js文件,还会生成一个声明文件(如果源文件叫index.ts,生成的声明文件就叫index.d.ts)
// import a from './case2/index';



/**=== case3 */
// 会发现报错了
// import a from './case2/index.js';

// a(111); // TS2345: Argument of type '111' is not assignable to parameter of type 'Person'.



/**=== 注意:
 声明文件和对应的.js文件默认应该在同一层级目录下(tsc -d 默认也是生成在一起的)

 这是给模块化库书写类型文件时 import解析的默认位置 如果是npm包 你还可以通过package.json中的types字段来指定类型文件的位置(即你可以将类型文件放在任意位置,只要在types字段进行了指向)

 （参看case5→）但如果是为全局库书写类型, 类型文件可以放在任意位置,甚至无需单独的类型声明文件(`*.d.ts`)而是直接声明在主文件中(注意如果是类型文件,此时不要有export导出,因为这样会将类型文件里declare的作为局部的,是上面所述的情景).但需要注意的是你的tsconfig.json文件的`includes`配置 需要包含该类型文件所处位置(路径)

*/



/**=== case4 */
// 声明文件的查找和npm包查找机制基本是一样的,不过一个是看package.json的main字段,一个看types字段(非第三方模块不会向上查找,即只有绝对路径模块才会向上查找)
// 通过package.json的typings字段(或则types字段,是等价的)可以指定声明文件的位置
// 运行tsc index.ts 发现报错
import a from './case4';

a(123); // 报错 TS2345: Argument of type '111' is not assignable to parameter of type 'Person'.
