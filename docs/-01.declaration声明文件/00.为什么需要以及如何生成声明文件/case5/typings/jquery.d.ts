// 没有加export,这样就是声明在全局下的,所有ts文件都可以访问到
declare const $: (selector: string) => {
  click(): void;
  width(length: number): void;
}
