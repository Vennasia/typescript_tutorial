import * as React from 'react'; /** 一般umd模块 最好使用import* as 这样引入*/

declare module 'react'/*←注意这个是你import from 后面的那个模块名 是小写*/ {
  const xxx: number;
}

//React.aaa // 报错
React.xxx // 不报错
