// class Form {
//   username: Form.Item = "";
//   password: Form.Item = ''; // TS2702: 'Form' only refers to a type, but is being used as a namespace here.
// }


class Form {
  username: Form.Item = "";
  password: Form.Item = ''; // √
}

declare namespace Form {
  class Item {} // 如果这里的命名空间并不是declare作为单纯的声明使用(如果声明了declare namespace的话,是无需export,即可访问namespace里的成员的),就需要export(这个Item); 之所以两种方式皆可,是因为类不仅可以作为值还可以作为类型(声明),另外之所以declare的namespace不需要导出,是因为你都declare了,说明是要声明出去给别人用的,故就不需要再手动声明要export了
}

(new Form()).username;
(new Form()).password;
Form.Item;
