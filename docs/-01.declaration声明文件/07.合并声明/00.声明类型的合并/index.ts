/** 合并声明
同一名称的两个独立声明会被合并成一个单一声明
*/
export {}

class Person{
  name: string = 'hello'
}

let p1:Person; // 一个类即可以作为类型来使用

// 在此处Person就是一个值了
let p2 = new Person; // 也可以作为值来使用

// interface只能作为类型使用,不能坐位置
interface Person {
  // name: string = 'hello' // TS1246: An interface property cannot have an initializer.
  age:number
}

let p3: Person;
// let p4 = Person2; // TS2693: 'Person2' only refers to a type, but is being used as a value here.


// 上面两个具有相同名称的类型声明 会合并到一起
p3 = {name:'ahhh'} // TS2741: Property 'age' is missing in type '{ name: string; }' but required in type 'Person'.
