enum Color {
  red = 1,
  yellow = 2,
  blue = 3
}

namespace Color {
  export const green = 4;
  export const purple = 5;
}

console.log(Color.red);
console.log(Color.green);
