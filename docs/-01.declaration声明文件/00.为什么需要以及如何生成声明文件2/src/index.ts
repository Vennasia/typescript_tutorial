/*
声明文件可以让我们不需要将JS重构为TS,只需要加上声明文件就可以使用系统

但我们也不是什么时候都需要*.d.ts 类型声明文件

下面的例子中,我们就是直接在全局下
*/

/** tsc index.ts [--watch] */
// $('#root').click(); // 会报错 TS2581: Cannot find name '$'. Do you need to install type definitions for jQuery? Try `npm i @types/jquery`.

/** 但当我们下面使用declare声明以后,虽然仍然会报错,但错误信息变成了:
    ESLint: '$' was used before it was defined.(@typescript-eslint/no-use-before-define)
*/

declare const $: (selector: string) => { // ←注意这不是箭头函数 这就是一个ts里的函数声明
  click(x: () => void): void;
  width(length: number): void;
};

/** 将这句代码放到 declare const $ 下面后,虽然编译时不会报错了*/
// $('#root').click();
// $('whatever').width(1);

/** 但运行时,浏览器控制台依然会报错*/
// Uncaught ReferenceError: $ is not defined

/**
 这是理所当然的,因为上面仅仅是一个声明
 ,是给typescript在编译阶段看的
 有声明的话,代码至少在编译阶段就不会报错了
 并且此时我们`.`的话,编辑器就会有提示了(提示就是我们上面写的declare声明)
 ctrl + 鼠标左键 点击上面的click、width,我们也能看到它的类型了
*/


/** 现在我们使用webpack来运行typescript,在html模板文件中先放一个jquery的cdn标签
    此时我们就既能使用typescript的特性，js也确实的运行了,不会报错了
    （此时 declare const $ 相当于声明在全局中,故正好对应全局引入的jquery)
*/
$('#root').click(()=>{alert(1)});
// export = {}
