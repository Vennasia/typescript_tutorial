// === webpack-dev-server ===

module.exports = {
  //静态文件根目录 express.static('dist')
  contentBase: './dist'
  , port: 3000
  , host: 'localhost'

  //是否自动弹出浏览器
  // ,open:false

  ,overlay:{
    //webpack编译时若存在错误，直接显示在页面上
    errors:true
  }

  ,hot: true
  //禁止全局刷新
  // ,hotOnly:true

  //单页应用时
  //如果访问http://localhost:3000/x(敲回车)
  //返回的页面就会输出 Cannot GET /x
  //置为true后,不论怎样都会返回打包后的那个html
  //并且也支持react-router-dom,刷新会跳到对应路由
  // ,这是因为Router在挂载时(componentDidMount)会拿window.location.hash.slice(1)来setState从而刷新组件路由得以加载
  ,historyApiFallback:true //默认为false

  // , proxy: {
  //   "/api":"http://localhost:3000"
  //
  //   // 如果访问 http://localhost:8080/api/user
  //   // 就会将请求转发给  http://localhost:3000/user
  //   ,pathRewrite:{'^/api':""}
  // }

  //app为webpack-dev-server内部使用的express的app
  // ,before(app){
  //   app.get('/api/users',(req,res)=>{
  //     res.send({id: 1, name: 'fancier'});
  //   })
  // }
};
