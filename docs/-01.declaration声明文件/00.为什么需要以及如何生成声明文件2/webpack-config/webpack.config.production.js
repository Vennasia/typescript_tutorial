const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
//如果是同一个MiniCssExtractPlugin实例，那多个loader配置(css、less、sass)会打包到同一个css文件里
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//压缩js
const TerserWebpackPlugin = require('terser-webpack-plugin'); //支持压缩es6
//压缩css
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const path = require('path');

module.exports = {
  //最佳化，最优化
  optimization: {
    minimize:true
    //需要将webpack的mode置为production以下插件才有效
    ,minimizer: [
      new TerserWebpackPlugin({
        parallel:true //开启多进程并行压缩
        ,cache:true //开启缓存,如果一个js文件压缩过了,会放在内存中,每次压缩时会和内存中的进行比较,如果没变就用以前的
      })
      , new OptimizeCssAssetsWebpackPlugin()
    ]

    ,splitChunks:{
      cacheGroups: {
        vendors: {
          chunks: "initial"
          , test: /node_modules/
          , name: 'vendors'
          , minChunks: 1
        }
      }
    }
  }

  , module: {
    rules: [
      {
        test: /\.css$/

        , use: [MiniCssExtractPlugin.loader, 'css-loader', {loader: 'postcss-loader', options: {config: {path: 'webpack-config'}}}]

      }

      //sass

      , {
        test: /\.scss/
        , use: [MiniCssExtractPlugin.loader, 'css-loader', {loader: 'postcss-loader', options: {config: {path: 'webpack-config'}}}, 'sass-loader']
      }

      //less

      // , {
      //   test: /\.less/
      //   , use: [{
      //     loader: MiniCssExtractPlugin.loader
      //     , options: {
      //       insertAt: 'top'
      //     }
      //   }, 'css-loader', 'postcss-loader', 'less-loader']
      // }

      // ,{
      //   test: /\.[jt]sx?$/
      //
      //   , use: [{
      //     loader: 'ts-loader'
      //     ,options:{
      //       // 默认为false
      //       transpileOnly: true // 只做语言转化而不做类型检查(意思是说即使写的代码不符合ts规范,也能编译成功) 类型检查交给 ForkTsCheckerWebpackPlugin 插件来做 这样做是为了提高构建速度(build的速度)
      //     }
      //   }]
      //
      //   , include: path.resolve(__dirname,'../src')
      //
      //   , exclude: /node_modules/
      // }
    ]
  }

  , plugins: [
    new HtmlWebpackPlugin({
      //模板位置
      template: './public/index_pro.html'
      //打包后的名字
      // , filename: 'index.html'
      //压缩
      , minify: {
        //删除属性的双引号
        removeAttributeQuotes: true
        //折叠空白区域 也就是压缩代码
        ,collapseWhitespace:true
      }
    })

    , new MiniCssExtractPlugin({
      //[name]是entry的名字，默认为main
      filename: 'css/[name]-[contenthash:7].css'
    })

    //在打包的文件顶部插入文字
    , new webpack.BannerPlugin('code&design by Fancier')

    /*
      By default, this plugin will remove all files inside webpack's output.path directory
      , as well as all unused webpack assets after every successful rebuild.
    */
    , new CleanWebpackPlugin()

    // , new ForkTsCheckerWebpackPlugin() // 和默认的ts类型检查相比较 是放在独立的进程中进行的 另外需要注意的是 即使类型校验有错 并且报错到控制台了 但也不会阻止打包
  ]
};
